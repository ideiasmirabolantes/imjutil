package com.im.imjutil.session;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.im.imjutil.exception.SessionException;
import com.im.imjutil.exception.ValidationException;

public class SessionManagerTest {

	// Classe de implementacao da sessao
	public static final Class<? extends Session> clazz = SimpleSession.class;
	
	private static SessionManager sessionManager;
	private static Session session;
	private static String sid;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		sessionManager = new SessionManager(clazz);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		sessionManager = null;
	}
	
	@Test(expected = IllegalStateException.class)
	public void testStart() {
		sessionManager.start();
		sessionManager.start();
	}

	@Test(expected = SessionException.class)
	public void testCreateInt() {
		Session s = sessionManager.create(10);
		assertNotNull(s);
		s = sessionManager.create(0);
	}

	@Test
	public void testCreate() {
		session = sessionManager.create();
		assertNotNull(session);
		sid = session.getId();
	}

	@Test(expected = ValidationException.class)
	public void testGet() {
		assertNotNull(sessionManager.get(sid));
		assertNull(sessionManager.get("null"));
		sessionManager.get(null);
	}
	
	@Test
	public void testGetSessions() {
		assertNotNull(sessionManager.getSessions());
	}

	@Test 
	public void testIsValid() {
		assertTrue(sessionManager.isValid(sid));
		assertTrue(sessionManager.isValid(session));
	}

	@Test
	public void testInvalidate() {
		sessionManager.invalidate(session);
		assertFalse(session.isValid());
	}
	
	@Test
	public void testIsNotValid() {
		assertFalse(sessionManager.isValid(sid));
		assertFalse(sessionManager.isValid(session));
	}

	@Test
	public void testStop() {
		sessionManager.stop();
	}

}
