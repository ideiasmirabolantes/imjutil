package com.im.imjutil.session;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Date;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.util.Util;

public class SessionTest {

	// Classe de implementacao da sessao
	public static final Class<? extends Session> clazz = SimpleSession.class;
	
	public static Session session;
	public static Date expiration;
	public static String id;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		session = clazz.newInstance();
		
		Calendar c = Calendar.getInstance();
		c.add(Calendar.MINUTE, 30);
		expiration = c.getTime();
		session.setExpirationDate(expiration);
		
		id = Util.generatePassword(32).toUpperCase();
		session.setId(id);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		session = null;
	}

	@Test(expected = ValidationException.class)
	public void testAddNullData() {
		session.addData("Teste", null);
	}
	
	@Test(expected = ValidationException.class)
	public void testAddNullKey() {
		session.addData(null, "Teste");
	}

	@Test
	public void testGetData() {
		Date d = new Date();
		session.addData("date", d);
		assertEquals(d, session.getData("date"));
	}
	
	@Test
	public void testGetNullData() {
		assertNull(session.getData("null"));
	}

	@Test
	public void testGetExpirationDate() {
		assertNotNull(session.getExpirationDate());
		assertEquals(expiration, session.getExpirationDate());
	}

	@Test
	public void testGetId() {
		assertEquals(id, session.getId());
	}

	@Test
	public void testIsValid() {
		assertTrue(session.isValid());
	}
	
	@Test
	public void testRemoveData() {
		session.addData("test", new Object());
		session.removeData("test");
		assertNull(session.getData("test"));
	}

	@Test(expected = ValidationException.class)
	public void testSetExpirationNullDate() {
		session.setExpirationDate(null);
	}
	
	@Test(expected = ValidationException.class)
	public void testSetExpirationInvalidDate() {
		session.setExpirationDate(new Date(0));
	}

	@Test(expected = ValidationException.class)
	public void testSetEmptId() {
		session.setId("");
	}
	
	@Test(expected = ValidationException.class)
	public void testSetNullId() {
		session.setId(null);
	}

	@Test
	public void testInvalidate() {
		session.invalidate();
		assertEquals(false, session.isValid());
	}
	
	@Test
	public void testIsNotValid() {
		assertFalse(session.isValid());
	}
}
