package com.im.imjutil.email;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;

public class MailPop {

//http://www.javaworld.com/javaworld/jw-10-2001/jw-1026-javamail.html?page=4

public static void main(String[] args) {

    Properties props = new Properties();

    String host = "pop.mail.yahoo.com.br";
    String username = "felipezappala";
    String password = "d22z09";
    String provider = "pop3";
    
//    String host = "pop.gmail.com";
//    String username = "felipe.zappala";
//    String password = "S3vEod2>";
//    String provider = "pop3";

//    String host = "utopia.poly.edu";
//    String username = "eharold";
//    String password = "mypassword";
//    String provider = "pop3";

    
    
    try {

      // Connect to the POP3 server
//      Session session = Session.getDefaultInstance(props, new Authenticator() {
//    	  @Override
//    	public PasswordAuthentication getPasswordAuthentication() {
//    		return new PasswordAuthentication("felipe.zappala", "S3vEod2>");
//    	}
//      });
    	Session session = Session.getDefaultInstance(props, null);
    	
      Store store = session.getStore(provider);
      //store.connect(host, 995, username, password);
      store.connect(host, username, password);

      // Open the folder
      Folder inbox = store.getFolder("INBOX");
      if (inbox == null) {
        System.out.println("No INBOX");
        System.exit(1);
      }
      inbox.open(Folder.READ_ONLY);
      
      // Get the messages from the server
      Message[] messages = inbox.getMessages();
      for (int i = 0; i < messages.length; i++) {
        //System.out.println("------------ Message " + (i+1) + " ------------");
        //messages[i].writeTo(System.out);
    	  printMessage(messages[i]);
      }

      // Close the connection
      // but don't remove the messages from the server
      inbox.close(false);
      store.close();

    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
  }
  
  /**
   * "printMessage()" method to print a message.
   */
 public static void printMessage(Message message)
 {
   try
   {
     // Get the header information
     String from=((InternetAddress)message.getFrom()[0]).getPersonal();
     if (from==null) from=((InternetAddress)message.getFrom()[0])
      .getAddress();
     System.out.println("FROM: "+from);
     String subject=message.getSubject();
     System.out.println("SUBJECT: "+subject);
     // -- Get the message part (i.e. the message itself) --
     Part messagePart=message;
     Object content=messagePart.getContent();
     // -- or its first body part if it is a multipart message --
     if (content instanceof Multipart)
     {
       messagePart=((Multipart)content).getBodyPart(0);
       System.out.println("[ Multipart Message ]");
     }
     // -- Get the content type --
     String contentType=messagePart.getContentType();
     // -- If the content is plain text, we can print it --
     System.out.println("CONTENT:"+contentType);
     if (contentType.startsWith("text/plain")
      || contentType.startsWith("text/html"))
     {
       InputStream is = messagePart.getInputStream();
       BufferedReader reader
        =new BufferedReader(new InputStreamReader(is));
       String thisLine=reader.readLine();
       while (thisLine!=null)
       {
         System.out.println(thisLine);
         thisLine=reader.readLine();
       }
     }
     System.out.println("-----------------------------");
   }
   catch (Exception ex)
   {
     ex.printStackTrace();
   }
 }
}


