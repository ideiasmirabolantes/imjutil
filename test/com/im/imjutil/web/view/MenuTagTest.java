package com.im.imjutil.web.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.im.imjutil.util.Albums;

public class MenuTagTest extends MenuTag {

	private static final long serialVersionUID = 1L;


	public static void main(String[] args) {
		MenuItem mi1 = new Menu();
		mi1.setName("menu 1");
		mi1.setText("menu 1 menu 1 menu 1");
		mi1.setURL("menu1.html");
		
		MenuItem mi2 = new Menu();
		mi2.setName("menu 2");
		mi2.setText("menu 2 menu 2 menu 2");
		mi2.setURL("menu2.html");
		
		MenuItem mi3 = new Menu();
		mi3.setName("menu 3");
		mi3.setText("menu 3 menu 3 menu 3");
		mi3.setURL("menu3.html");
		mi2.add(mi3);
		
		MenuItem mi4 = new Menu();
		mi4.setName("menu 4");
		mi4.setText("menu 4 menu 4 menu 4");
		mi4.setURL("menu4.html");
		mi3.add(mi4);
		
		MenuTag mt = new MenuTag();
		mt.setCssClassMenu("cssmenu");
		mt.setCssClassSubMenu("csssubmenu");
		String menu = mt.print(Albums.asList(mi1, mi2));
		
		System.out.println(menu);
	}
	
	

	static class Menu implements MenuItem {

		private List<MenuItem> childs = new ArrayList<MenuItem>();
		private String name = "";
		private String URL = "";
		private String text = "";
		
		@Override
		public void add(MenuItem composite) {
			childs.add(composite);
		}

		@Override
		public void remove(MenuItem composite) {
			childs.remove(composite);
		}

		@Override
		public boolean isComposite() {
			return !childs.isEmpty();
		}

		@Override
		public Iterator<MenuItem> iterator() {
			return childs.iterator();
		}

		@Override
		public String getName() {
			return name;
		}

		@Override
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String getURL() {
			return URL;
		}

		@Override
		public void setURL(String url) {
			this.URL = url;
		}

		@Override
		public String getText() {
			return text;
		}

		@Override
		public void setText(String text) {
			this.text = text;
		}

		@Override
		public boolean isHidden() {
			return false;
		}
		
	}

}
