package com.im.imjutil.dao.jpa;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.dao.FactoryDAO;
import com.im.imjutil.dao.TypeDAO;
import com.im.imjutil.logging.Logger;
import com.im.imjutil.util.Filter;

public abstract class DAOTest<T> {
	
	protected Class<T> clazz;
	protected DAO<T> dao;
	protected TypeDAO typeDAO;
	protected Filter filter;
	protected Object id;
	protected T type;
	
	public DAOTest(TypeDAO dao, Class<T> clazz, T type, 
			Filter filter, Object id) {
		
		Logger.debug("[DAOTest] UNIT TEST CASE for Class: ", clazz.getName());

		this.typeDAO = dao;
		this.clazz = clazz;
		this.type = type;
		this.filter = filter;
		this.id = id;
	}
	
	@Before
	public void setUp() throws Exception {
		Logger.debug("[DAOTest] Starting...");
		dao = FactoryDAO.createDAO(typeDAO, clazz);
	}

	@After
	public void tearDown() throws Exception {
		Logger.debug("[DAOTest] Ending...");
		dao = null;
	}

	@Test
	public void testAdd() {
		Logger.debug("[DAOTest] add: ", type);
		dao.add(type);
	}

	@Test  
	public void testGet() {
		Logger.debug("[DAOTest] get: ", id);
		T t = dao.get(id);
		Logger.debug("[DAOTest] get result: ", t);
		assertNotNull(t);
	}

	@Test
	public void testFindFilter() {
		Logger.debug("[DAOTest] find: ", filter);
		T t = dao.find(filter);
		Logger.debug("[DAOTest] find result: ", t);
		assertNotNull(t);
	}

	@Test
	public void testRemove() {
		T t = dao.find(filter);
		Logger.debug("[DAOTest] remove: ", t);
		dao.remove(t);
	}

	@Test
	public void testAddAll() {
		List<T> t = dao.findAll(filter);
		Logger.debug("[DAOTest] addAll: ", t);
		dao.addAll(t);
	}
	
	@Test
	public void testGetAll() {
		List<T> t = dao.getAll();
		Logger.debug("[DAOTest] getAll result: ", t);
		assertNotNull(t);
	}
	
	@Test
	public void testFindAllFilter() {
		Logger.debug("[DAOTest] findAll: ", filter);
		List<T> t = dao.findAll(filter);
		Logger.debug("[DAOTest] find result: ", t);
		assertNotNull(t);
	}
	
	@Test
	public void testRemoveAll() {
		List<T> t = dao.findAll(filter);
		Logger.debug("[DAOTest] removeAll: ", t);
		dao.removeAll(t);
	}

}
