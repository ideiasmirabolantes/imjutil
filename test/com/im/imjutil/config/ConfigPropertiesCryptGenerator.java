package com.im.imjutil.config;

import java.io.File;
import java.io.FileWriter;

import com.im.imjutil.util.Util;
import com.im.imjutil.validation.Convert;

public class ConfigPropertiesCryptGenerator {

	public static void main(String[] args) throws Exception {
		String file = args[0];
		byte[] arq = Convert.toBytes(new File(file));
		
		String cfile = Util.encryptPassword(new String(arq));
		FileWriter fw = new FileWriter(new File(file + ".crypt"));
		fw.append(cfile);
		fw.close();
	}
	
}
