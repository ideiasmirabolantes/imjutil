package com.im.imjutil.factory;

import java.io.InputStream;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.validation.Convert;


/**
 * Classe de teste para o classLoader implementado na jlibutil.
 * 
 * @author Bruno Alcantara
 */
public class ClassLoaderTest {

	public static void main(String[] args) {
		try {
			ClassLoader plugin = new ClassLoader();

			String pluginPath = "/Users/brunomirabolante/Desktop/test/";
			
			plugin.addFilesFromDirectory(pluginPath);

			Class<?> instance = plugin
				.loadClass("com.hint.photodream.model.profile.SocialNetwork");
			
			Object mySN = instance.newInstance();
			InputStream resource = mySN.getClass().getResourceAsStream(
					"/META-INF/persistence.xml");

			Logger.debug("Resource URL: ", resource);
			Logger.debug("Resource Content: ",
					new String(Convert.toBytes(resource)));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
