package com.im.imjutil.validation;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

import org.junit.Test;

public class ConvertTest extends TestCase {

	@Test
	public void testToInteger() {
		assertEquals(55, Convert.toInteger("55"));
		assertEquals(0, Convert.toInteger("null"));
	}

	@Test
	public void testToLong() {
		assertEquals(55L, Convert.toLong("55"));
		assertEquals(0L, Convert.toLong("null"));
	}

	@Test
	public void testToShort() {
		assertEquals(((short)55), Convert.toShort("55"));
		assertEquals(((short)0), Convert.toShort("null"));
	}

	@Test
	public void testToByte() {
		assertEquals(((byte)55), Convert.toByte("55"));
		assertEquals(((byte)0), Convert.toByte("null"));
	}

	@Test
	public void testToFloat() {
		assertEquals(55.5F, Convert.toFloat("55.5F"));
		assertEquals(0.0F, Convert.toFloat("null"));
	}

	@Test
	public void testToDouble() {
		assertEquals(55.5D, Convert.toDouble("55.5D"));
		assertEquals(0.0D, Convert.toDouble("null"));
	}
	
	@Test
	public void testToBigInteger() {
		assertEquals(new BigInteger("55"), 
				Convert.toBigInteger("55"));
		assertEquals(new BigInteger("0"), 
				Convert.toBigInteger("null"));
		assertEquals(new BigInteger("33"), 
				Convert.toBigInteger(new Integer("33")));
		assertEquals(new BigInteger("33"), 
				Convert.toBigInteger(new Double("33.333")));
	}
	
	@Test
	public void testToBigDecimal() {
		assertEquals(new BigDecimal("555.55"), 
				Convert.toBigDecimal("555.55"));
		assertEquals(new BigDecimal("0"), 
				Convert.toBigDecimal("null"));
		assertEquals(new BigDecimal("33.55"), 
				Convert.toBigDecimal(new Double("33.55")));
		assertEquals(new BigDecimal("33"), 
				Convert.toBigDecimal(new Integer("33")));
	}

	@Test
	public void testToTimestampStringString() {
		Date date = null;
		try {
			date = new SimpleDateFormat("dd/MM hh:mm")
					.parse("31/12 11:59");
		} catch (ParseException e) {
			fail("Fala ao criar a data");
		}
		assertEquals(date, Convert.toTimestamp("dd/MM hh:mm", "31/12 11:59"));
		assertNull(Convert.toTimestamp("null"));
	}
	
	@Test
	public void testToTimestampString() {
		Date date = null;
		try {
			date = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
					.parse("31/12/2009 11:59:00");
		} catch (ParseException e) {
			fail("Fala ao criar a data");
		}
		assertEquals(date, Convert.toTimestamp("31/12/2009 11:59:00"));
		assertNull(Convert.toTimestamp("null"));
	}

	@Test
	public void testToDate() {
		Date date = null;
		try {
			date = new SimpleDateFormat("dd/MM/yyyy")
					.parse("31/12/2009");
		} catch (ParseException e) {
			fail("Fala ao criar a data");
		}
		assertEquals(date, Convert.toDate("31/12/2009"));
		assertNull(Convert.toDate("null"));
	}

	@Test
	public void testToTime() {
		Date date = null;
		try {
			date = new SimpleDateFormat("hh:mm:ss")
					.parse("11:59:00");
		} catch (ParseException e) {
			fail("Fala ao criar a data");
		}
		assertEquals(date, Convert.toTime("11:59:00"));
		assertNull(Convert.toTime("null"));
	}

}
