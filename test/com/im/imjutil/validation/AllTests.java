package com.im.imjutil.validation;

import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for com.im.imjutil.validation");
		//$JUnit-BEGIN$
		suite.addTestSuite(ConvertTest.class);
		suite.addTestSuite(ValidationTest.class);
		//$JUnit-END$
		return suite;
	}

}
