package com.im.imjutil.validation;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.Test;

public class ValidationTest extends TestCase {

	@Test
	public void testIsValid() {
		String s = new String("A");
		assertTrue(Validator.isValid(s));
		s = null;
		assertFalse(Validator.isValid(s));
		Object o = new String("");
		assertFalse(Validator.isValid(o));
		o = null;
		assertFalse(Validator.isValid(o));
	}

	@Test
	public void testIsEmptyArray() {
		String[] s = {"A", "B", "C"};
		assertFalse(Validator.isEmpty(s));
		s = null;
		assertTrue(Validator.isEmpty(s));
	}
	
	@Test
	public void testIsEmptyCollection() {
		List<String> l = Arrays.asList(new String[]{"A", "B", "C"});
		assertFalse(Validator.isEmpty(l));
		l = null;
		assertTrue(Validator.isEmpty(l));
	}
	
	@Test
	public void testIsEmptyMap() {
		Map<String, String> m = new HashMap<String, String>();
		m.put("A", "a"); m.put("B", "b");
		assertFalse(Validator.isEmpty(m));
		m = null;
		assertTrue(Validator.isEmpty(m));
	}

	@Test
	public void testValidDate() {
		assertTrue(Validator.isDate("31/12/2009"));
		assertFalse(Validator.isDate("AA/BB/CC"));
		assertFalse(Validator.isDate((String) null));
	}

	@Test
	public void testValidTime() {
		assertTrue(Validator.isTime("23:59:59"));
		assertFalse(Validator.isTime("AA:BB:CC"));
		assertFalse(Validator.isTime((String) null));
	}

	@Test
	public void testIsTimestamp() {
		assertTrue(Validator.isTimestamp("31/12/2009 23:59:59"));
		assertFalse(Validator.isTimestamp("AA/BB/CC AA:BB:CC"));
		assertFalse(Validator.isTimestamp((String) null));
	}

	@Test
	public void testIsNumber() {
		assertTrue(Validator.isNumber("123456789"));
		assertTrue(Validator.isNumber("123456789.987654321"));
		assertFalse(Validator.isNumber((String) null));
	}

	@Test
	public void testValidRegExp() {
		assertTrue(Validator.validRegExp("^casa$", "casa"));
		assertFalse(Validator.validRegExp("^casa$", (String) null));
	}

	@Test
	public void testIsEmail() {
		assertTrue(Validator.isEmail("aa@bb.cc"));
		assertFalse(Validator.isEmail("  AA@BB.CC  "));
		assertFalse(Validator.isEmail("AA@@BB.CC"));
		assertFalse(Validator.isEmail("AA@BB.CC."));
		assertFalse(Validator.isEmail("AA@BB.&.CC"));
		assertFalse(Validator.isEmail((String) null));
	}

	@Test
	public void testIsCPF() {
		assertTrue(Validator.isCPF("477.889.124-41"));
		assertTrue(Validator.isCPF("47788912441"));
		assertFalse(Validator.isCPF("12345678901"));
		assertFalse(Validator.isCPF("123"));
		assertFalse(Validator.isCPF("abc"));
		assertFalse(Validator.isCPF((String) null));
	}
	
	@Test
	public void testIsCEP() {
		assertTrue(Validator.isCEP("25.635-190"));
		assertTrue(Validator.isCEP("25635-190"));
		assertTrue(Validator.isCEP("25635190"));
		assertFalse(Validator.isCEP("2563519"));
		assertFalse(Validator.isCEP("abcdefgh"));
		assertFalse(Validator.isCEP((String) null));
	}
	
	@Test
	public void testIsCNPJ() {
		assertTrue(Validator.isCNPJ("58.871.375/0001-07"));
		assertTrue(Validator.isCNPJ("58871375/0001-07"));
		assertTrue(Validator.isCNPJ("58871375000107"));
		assertFalse(Validator.isCNPJ("1234567890123a"));
		assertFalse(Validator.isCNPJ((String) null));
	}
	
	@Test
	public void testIsURL() {
		assertTrue(Validator.isURL("http://www.iw.com/"));
		assertTrue(Validator.isURL("ftp://www.iw.com"));
		assertTrue(Validator.isURL("https://www.iw.com"));
		assertTrue(Validator.isURL("ftps://www.iw.com"));
		assertFalse(Validator.isURL("www.iw.com"));
		assertFalse(Validator.isURL("com.iw"));
		assertFalse(Validator.isURL((String) null));
	}
	
}
