package com.im.imjutil.logging;

import com.im.imjutil.config.Configurator;
import com.im.imjutil.validation.Convert;

/**
 * Classe responsavel por registar atividades do software em arquivo log.
 * usa como log padrao o CommonLog da Apache. <br>
 * A configuracao do log se da pelo arquivo <code>log4j.properties</code> 
 * situado no <code>CLASSPATH</code> da aplicacao.<br>
 * Para ativar ou desativar o log, use as propriedades do arquivo
 * <code>/META-inf/config.properties</code>
 * 
 * @author Felipe Zappala
 */
public final class Logger {
	
	private static final String LOG_INFO;
	private static final String LOG_ERRO;
	private static final String LOG_DEBUG;
	private static final String LOG_ALL;
	static {
		LOG_INFO  = "logger.active.info";
		LOG_ERRO  = "logger.active.error";
		LOG_DEBUG = "logger.active.debug";
		LOG_ALL   = "logger.active.all";
	}
	
	private static boolean activeLogInfo;
	private static boolean activeLogError;
	private static boolean activeLogDebug;
	
	/** Configura as ativacoes dos logs. */
	static {
		activeLogInfo = true;
		activeLogError = true;
		activeLogDebug = true;
		configureActivation();
	}
	
	/** Objeto logger */
	private static final org.apache.log4j.Logger logger;
	static {
		logger = org.apache.log4j.Logger.getRootLogger();
	}
	
	/** Proibi a instanciacao. */
	private Logger() {
		super();
	}

	/**
	 * Logger de debug.
	 * 
	 * @param o Objeto a ser logado.
	 */
	public static final void debug(Object... o) {
		if (activeLogDebug) {
			logger.debug(Convert.toString(o));
		}
	}
	
	/**
	 * Logger de informacoes.
	 * 
	 * @param o Objeto a ser logado.
	 */
	public static final void info(Object... o) {
		if (activeLogInfo) {
			logger.info(Convert.toString(o));
		}
	}
	
	/**
	 * Logger de erro.
	 * 
	 * @param o Objeto a ser logado.
	 */
	public static final void error(Object... o) {
		if (activeLogError) {
			logger.error(Convert.toString(o));
		}
	}
	
	/**
	 * Logger de erro.
	 * 
	 * @param o Objeto a ser logado.
	 * @param t A excessao causadora do erro.
	 */
	public static final void error(Object o, Throwable t) {
		if (activeLogError) {
			logger.error(o, t);
		}
	}
	
	/**
	 * Logger de erro.
	 * 
	 * @param t A excessao causadora do erro.
	 * @param o Objeto a ser logado.
	 */
	public static final void error(Throwable t, Object... o) {
		if (activeLogError) {
			logger.error(Convert.toString(o), t);
		}
	}
	
	/**
	 * Configura a ativacao ou desativacao dos logs com base no arquivo
	 * <code>/META-INF/config.properties<code>.<br>
	 * Por padrao todos os logs sao ativos.
	 */
	private static void configureActivation() {
		String active = Configurator.getProperty(LOG_ALL);
		
		if (active != null) {
			boolean activeAll = Boolean.valueOf(active);
			activeLogInfo = activeAll;
			activeLogError = activeAll;
			activeLogDebug = activeAll;
			
		} else {
			active = Configurator.getProperty(LOG_INFO);
			if (active != null) {
				activeLogInfo = Boolean.valueOf(active);
			}
			active = Configurator.getProperty(LOG_ERRO);
			if (active != null) {
				activeLogError = Boolean.valueOf(active);
			}
			active = Configurator.getProperty(LOG_DEBUG);
			if (active != null) {
				activeLogDebug = Boolean.valueOf(active);
			}
		}
	}

}
