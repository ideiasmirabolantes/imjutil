package com.im.imjutil.logging;

import java.io.IOException;
import java.util.Date;

import com.im.imjutil.file.File;
import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Format;

/**
 * Classe utilitaria de log para gravacao em arquivo.
 * 
 * @author Bruno Alcantara.
 *
 */
public class LoggerFile {
	
	/** Separador para a impressao do log de erro (exececao). */
	private static String separator;
	
	/** Arquivo de log da aplicacao. */
	private static java.io.File file;

	/** Verifica se o arquivo de log foi inicializado com sucesso. */
	private static boolean active;

	/** Mensagem do erro do arquivo de log inativo. */
	private static String activeLogMessage;
	
	static {
		separator = Convert.toString(
				"\n==========================================",
		"======================================\n");
		
		activeLogMessage = Convert.toString(
				"O arquivo de log nao foi inicialiado com sucesso. ",
				"Verifique se foi utilizada a chamada do metodo ",
				"[initializeLogger] em seu codigo para inicializa-lo ",
				"antes de utilizar os metodos de log da classe LoggerFile.");
	}
	
	public static java.io.File getFile() {
		return file;
	}

	public static void setFile(java.io.File file) {
		LoggerFile.file = file;
	}

	/**
	 * Inicializa o logger da aplicacao.
	 * 
	 * @param file Objeto do arquivo de log.
	 */
	public static void initializeLogger(java.io.File fileObject) {
		file = fileObject;
		
		if (!file.exists()) {
			try {
				file.createNewFile();
				active = true;
			} catch (IOException e) {
				String msg = "Erro ao criar o arquivo de log: ";
				Logger.error(msg, e);
				throw new RuntimeException(msg, e);
			}
		}
	}

	/**
	 * Grava o log de na saida do console e no arquivo de log da aplicacao.
	 * 
	 * @param <T> Tipo da classe passado como parametro.
	 * @param clazz Classe utilizada no log.
	 * @param message Mensagem do log.
	 * @param e Exececao do log de erro. Para um og de informacao seu valor
	 *          pode ser como nulo.
	 * @param error Para o valor seja VERDADEIRO o log de erro e utlizado, caso
	 *            contrario o log de informacao e utilizado.
	 */
	public static <T> void log(Class<T> clazz, String message, 
			Exception e, boolean error) {

		/*
		 * Verifica se o log de arquivo foi inicializado (arquivo de log foi
		 * criado com sucesso).
		 */
		if (!active) {
			throw new RuntimeException(activeLogMessage);
		}
		
		// Nome da classe usado no log.
		String className = Convert.toString("[", clazz.getSimpleName(), "] ");
		
		// Data corrente usada no log.
		String fomatDate = Convert.toString(
				"[", Format.toTimestamp(new Date()), "] ");
		
		// Log de erro.
		if (error) {
			// Log de erro sem excecao.
			if (e == null) {
				Logger.error(Convert.toString(className, message));
				File.WriterLn(file, Convert.toString(
						className, fomatDate, message), true);
			} else {
				// Log de erro com excecao.
				Logger.error(Convert.toString(className, message), e);
				File.WriterLn(file, Convert.toString(className, fomatDate, 
						message, separator, e, separator), true);
			}
		} else {
			// Log de informacao.
			Logger.info(Convert.toString(className, message));
			File.WriterLn(file, Convert.toString(
					className, fomatDate, message), true);
		}
		
	}
	
	/**
	 * Grava o log de erro na saida do console e no arquivo de log da aplicacao.
	 * 
	 * @param <T> Tipo da classe passado como parametro.
	 * @param clazz Classe utilizada no log.
	 * @param message Mensagem do log de erro.
	 */
	public static <T> void info(
			Class<T> clazz, String message) {
		log(clazz, message, null, false);
	}
	
	/**
	 * Grava o log de erro na saida do console e no arquivo de log da aplicacao.
	 * 
	 * @param <T> Tipo da classe passado como parametro.
	 * @param clazz Classe utilizada no log.
	 * @param message Mensagem do log de erro.
	 * @param e Exececao do log de erro.
	 */
	public static <T> void error(
			Class<T> clazz, String message, Exception e) {
		log(clazz, message, e, true);
	}
	
	/**
	 * Grava o log de erro na saida do console e no arquivo de log da aplicacao.
	 * 
	 * @param <T> Tipo da classe passado como parametro.
	 * @param clazz Classe utilizada no log.
	 * @param message Mensagem do log de erro.
	 * @param e Exececao do log de erro.
	 */
	public static <T> void error(
			Class<T> clazz, String message) {
		log(clazz, message, null, true);
	}
	
	/**
	 * Grava o log de erro na saida do console e no arquivo de log da aplicacao
	 * e lanca uma excecao de tempo de execucao com a mensagem do log de erro.
	 * 
	 * @param <T> Tipo da classe passado como parametro.
	 * @param clazz Classe utilizada no log.
	 * @param message Mensagem do log de erro.
	 * @param e Exececao do log de erro.
	 */
	public static <T> void errorWithThrow(
			Class<T> clazz, String message, Exception e) {
		error(clazz, message, e);
		throw new RuntimeException(message);
	}
	
}
