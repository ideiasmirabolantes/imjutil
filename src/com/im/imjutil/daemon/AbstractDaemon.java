package com.im.imjutil.daemon;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

public abstract class AbstractDaemon extends QuartzJobBean implements Runnable {

	/**
	 * Contexto de execucao do trabalho passado no metodo de chamada do daemon.
	 */
	protected JobExecutionContext context;
	
	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		this.context = arg0;
		this.run();
	}

}
