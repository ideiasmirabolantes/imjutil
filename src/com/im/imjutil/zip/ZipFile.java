package com.im.imjutil.zip;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;

import com.im.imjutil.validation.Convert;

/**
 * Classe utilitaria pra criacao e extracao de arquivos zip. 
 * 
 * @author Felipe Zappala
 */
public final class ZipFile {

	/** Buffer de leitura */
	private static final int BUFFER = 1024; 

	/** Proibir a inicializaco. */
	private ZipFile() {
		super();
	}
	
	/**
	 * Cria um arquivo zip com o nome de hierarquia.
	 * {@link ZipFile#zip(File, boolean, File...)}
	 * 
	 * @param zip O arquivo zip a ser criado
	 * @param files Os arquivos a serem adicionados no zip
	 * @return retorna um array de bytes do arquivo zipado
	 * @throws ZipException se algum erro ocorre
	 */
	public static byte[] zip(File zip, File... files) throws ZipException {
		return Convert.toBytes(zip(true, files));
	}
	
	/**
	 * Cria um arquivo zip com o nome de hierarquia.
	 * {@link ZipFile#zip(File, boolean, File...)}
	 * 
	 * @author Bruno Alcantara
	 * @param files Os arquivos a serem adicionados no zip
	 * @return retorna um array de bytes do arquivo zipado
	 * @throws ZipException se algum erro ocorrer
	 */
	public static byte[] zip(File... files) throws ZipException {
		return zipBytes(true, files);
	}
	
	/**
	 * Cria um arquivo zip com o nome de hierarquia.
	 * {@link ZipFile#zip(File, boolean, File...)}
	 * 
	 * @author Bruno Alcantara
	 * @param files Os arquivos a serem adicionados no zip
	 * @param hierarchical Se verdade, faz o zip do caminho completo do
	 * 		arquivo, senao, usa somente o nome na raiz do zip. 
	 * @return retorna um array de bytes do arquivo zipado
	 * @throws ZipException se algum erro ocorrer
	 */
	public static byte[] zipBytes(boolean hierarchical, File... files) throws ZipException {
		return Convert.toBytes(zip(hierarchical, files));
	}
	
	/**
	 * Cria um arquivo zip.
	 * 
	 * @author Bruno Alcantara
	 * @param files Os arquivos a serem adicionados no zip.
	 * @param hierarchical Se verdade, faz o zip do caminho completo do
	 * 		arquivo, senao, usa somente o nome na raiz do zip. 
	 * @throws ZipException se algum erro ocorrer.
	 */
	public static File zip(boolean hierarchical, File... files) throws ZipException {
		try {
			File file = files[0];
			String [] filename = file.getName().split("\\.");
			File zip = new File(Convert.toString(file.getParent(), 
					File.separator, filename[0], ".zip"));
			
			zip(zip, hierarchical, files);
			return zip;
		} catch (Exception e) {
			String message = Convert.toString("Error zipping file. Caused by: ",
					e.getClass().getSimpleName(), " - ", e.getMessage());
			
        	throw new ZipException(message);
		}
	}
	
	/**
	 * Cria um arquivo zip.
	 * 
	 * @param zip O arquivo zip a ser criado.
	 * @param hierarchical Se verdade, faz o zip do caminho completo do
	 * 		arquivo, senao, usa somente o nome na raiz do zip. 
	 * @param files Os arquivos a serem adicionados no zip.
	 * @throws ZipException se algum erro ocorrer.
	 */
	public static void zip(File zip, boolean hierarchical, File... files) 
			throws ZipException {
		try {
            byte[] buf = new byte[BUFFER];
             
            ZipOutputStream zipOut = new ZipOutputStream(
            		new BufferedOutputStream(new FileOutputStream(zip)));
            zipOut.setLevel(Deflater.BEST_COMPRESSION);
            
            ZipEntry z;
            BufferedInputStream bufIn;
            
            for (File f : files) {
            	bufIn = new BufferedInputStream(new FileInputStream(f));
            	
            	if (hierarchical) { 
            		z = new ZipEntry(f.toString()); //Full path of file in zip
            	} else {
        			z = new ZipEntry(f.getName());  //So name of file in zip
            	}
                zipOut.putNextEntry(z);
                
                int length;
                while ((length = bufIn.read(buf)) > 0) {
                	zipOut.write(buf, 0, length);
                }
                zipOut.flush();
                bufIn.close();
			}
            zipOut.finish();
            zipOut.close();
            
        } catch (Exception e) {
        	StringBuilder sb = new StringBuilder();
        	sb.append("Error zipping file. Caused by: ");
        	sb.append(e.getClass().getSimpleName());
        	sb.append(" - ");
        	sb.append(e.getMessage());
        	throw new ZipException(sb.toString());
        }
	}
	
	/**
	 * Extrai um arquivo zip.
	 * 
	 * @param zip O arquivo zip a ser criado.
	 * @param files Os arquivos a serem adicionados no zip.
	 * @return Uma lista com todos os arquivos do zip.
	 * @throws ZipException se algum erro ocorrer.
	 */
	public static List<File> unzip(File zip, File dir) throws ZipException {
		List<File> files = new ArrayList<File>();
		try {
			byte data[] = new byte[BUFFER];
			
			java.util.zip.ZipFile zipfile = new java.util.zip.ZipFile(zip);
			BufferedOutputStream dest;
			BufferedInputStream is;
			ZipEntry entry;

			String fileName;
			Enumeration<?> entries = zipfile.entries();
			while (entries.hasMoreElements()) {
				entry = (ZipEntry) entries.nextElement();
				fileName = dir.toString() + File.separatorChar + entry.getName();
				files.add(new File(fileName));
				
				is = new BufferedInputStream(zipfile.getInputStream(entry));
				dest = new BufferedOutputStream(new FileOutputStream(fileName));
				int length;
				while ((length = is.read(data)) > 0) {
					dest.write(data, 0, length);
				}
				dest.flush();
				dest.close();
				is.close();
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			sb.append("Error unzipping file. Caused by: ");
			sb.append(e.getClass().getSimpleName());
			sb.append(" - ");
			sb.append(e.getMessage());
			throw new ZipException(sb.toString());
		}
		return files;
	}
	
}
