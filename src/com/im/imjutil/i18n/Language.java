package com.im.imjutil.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

import com.im.imjutil.config.Configurator;
import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Validator;

/**
 * Classe responsavel pela internacionalizacao do sistema. 
 * Esta obtem as mensagens traduzidas em diversas linguagens.
 * Podendo ser instanciada ou utilizada estaticamente. 
 * 
 * @author Felipe Zappala
 */
public class Language {

	/**
	 * Define o caminho padrao dos arquivos de mensagem.
	 * Por padrao, estao localizados em {@code /META-INF/}
	 * sob o nome base {@code messages}
	 */
	protected static final String MESSAGES;
	
	/**
	 * Define a lingua padrao usada no sistema, podendo ser configurada atraves
	 * da propriedade {@code} do arquivo {@code /META-INF/config.properties}.
	 */
	protected static final String DEFAULT_LANGUAGE;
	
	static {
		MESSAGES = "META-INF.messages";
		
		String lang = Configurator.getProperty("system.locale");
		DEFAULT_LANGUAGE = (lang != null) ? lang : "pt,BR"; 
	}

	/** Pacote de recurso da linguagem. */
	private ResourceBundle resourceBundle;
	
	/** Localidade das linguagem. */
	private Locale locale;

	/**
	 * Construtor das mensagens em diversas linguagens.
	 * 
	 * @param baseName O nome base do arquivo de linguagens.
	 * @param locale O objeto de localidade {@link Locale} da lingua.
	 */
	public Language(String baseName, Locale locale) {
		if (!Validator.isValid(locale)) {
			throw new ValidationException("Parametro locale e nulo");
		}
		if (!Validator.isValid(baseName)) {
			throw new ValidationException("Parametro baseName e nulo ou vazio");
		}
		this.locale = locale;
		this.resourceBundle = ResourceBundle.getBundle(baseName, locale);
	}

	/**
	 * Construtor das mensagens em diversas linguagens.
	 */
	public Language() {
		this(MESSAGES, new Locale(DEFAULT_LANGUAGE));
	}
	
	/**
	 * Construtor das mensagens em diversas linguagens.
	 * 
	 * @param locale A string que representa a localidade. 
	 */
	public Language(String locale) {
		this(MESSAGES, new Locale(locale));
	}

	/**
	 * Construtor das mensagens em diversas linguagens.
	 * 
	 * @param locale O objeto {@link Locale} da localidade.
	 */
	public Language(Locale locale) {
		this(MESSAGES, locale);
	}
	
	/**
	 * Obtem a localidade configurada para as traducoes.
	 * 
	 * @return A localidade utilizada.
	 */
	public Locale getLocale() {
		return locale;
	}
	
	/**
	 * Obtem uma mensagem traduzida para o idioma da localidade configurada.
	 * 
	 * @param token A chave da mensagem desejada.
	 * @return O valor da chave passada.
	 */
	public String getMessage(String token) {
		String msg;
		try {
			msg = resourceBundle.getString(token.trim());
		} catch (Exception e) {
			msg = Convert.toString("???", token, "???");
		}
		return msg;
	}

	/**
	 * Obtem uma mensagem traduzida para o idioma da localidade configurada.
	 * Este concatena as varias traducoes das tokens passadas usando 
	 * o separador entre elas, criando uma unica frase.
	 * 
	 * @param separator O separador das palavras.
	 * @param tokens As chaves das mensagens desejadas.
	 * @return Os valores das chaves passadas concatenados e separados.
	 */
	public String getMessages(String separator, String... tokens) {
		StringBuilder sb = new StringBuilder();
		if (!Validator.isEmpty(tokens) && Validator.isValid(separator)) {
			for (String token : tokens) {
				sb.append(getMessage(token)).append(separator);
			}
			sb.setLength(sb.length() - separator.length());
		}
		return sb.toString();
	}
	
	/**
	 * Obtem uma mensagem traduzida pada o idioma da localidade passada.
	 * 
	 * @param token A chave da mensagem desejada.
	 * @return O valor da chave passada.
	 */
	public static String getMsg(String token) {
		Language language = new Language(DEFAULT_LANGUAGE);
		return language.getMessage(token);
	}
	
	/**
	 * Obtem uma mensagem traduzida pada o idioma da localidade passada.
	 * 
	 * @param locale A localidade do idioma.
	 * @param token A chave da mensagem desejada.
	 * @return O valor da chave passada.
	 */
	public static String getMsg(Locale locale, String token) {
		Language language = new Language(locale);
		return language.getMessage(token);
	}
	
	/**
	 * Obtem uma mensagem traduzida pada o idioma da localidade passada.
	 * 
	 * @param locale A localidade do idioma.
	 * @param token A chave da mensagem desejada.
	 * @return O valor da chave passada.
	 */
	public static String getMsg(String locale, String token) {
		Language language = new Language(locale);
		return language.getMessage(token);
	}
	
	/**
	 * Obtem uma mensagem traduzida para o idioma da localidade configurada.
	 * Este concatena as varias traducoes das tokens passadas usando 
	 * o separador entre elas, criando uma unica frase.
	 * 
	 * @param separator O separador das palavras.
	 * @param tokens As chaves das mensagens desejadas.
	 * @return Os valores das chaves passadas concatenados e separados.
	 */
	public static String getMsgs(String separator, String... tokens) {
		Language language = new Language(DEFAULT_LANGUAGE);
		return language.getMessages(separator, tokens);
	}
	
	/**
	 * Obtem uma mensagem traduzida para o idioma da localidade configurada.
	 * Este concatena as varias traducoes das tokens passadas usando 
	 * o separador entre elas, criando uma unica frase.
	 * 
	 * @param locale A localidade do idioma.
	 * @param separator O separador das palavras.
	 * @param tokens As chaves das mensagens desejadas.
	 * @return Os valores das chaves passadas concatenados e separados.
	 */
	public static String getMsgs(Locale locale, String separator, 
			String... tokens) {
		Language language = new Language(locale);
		return language.getMessages(separator, tokens);
	}

	/**
	 * Obtem uma mensagem traduzida para o idioma da localidade configurada.
	 * Este concatena as varias traducoes das tokens passadas usando 
	 * o separador entre elas, criando uma unica frase.
	 * 
	 * @param locale A localidade do idioma.
	 * @param separator O separador das palavras.
	 * @param tokens As chaves das mensagens desejadas.
	 * @return Os valores das chaves passadas concatenados e separados.
	 */
	public static String getMsgs(String locale, String separator, 
			String... tokens) {
		Language language = new Language(locale);
		return language.getMessages(separator, tokens);
	}
	
}
