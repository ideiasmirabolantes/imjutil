package com.im.imjutil.enums;

/**
 * Enum que define os dias da semana.
 * 
 * @author Ygor Fonseca
 */
public enum DaysOfWeekEnum {

	/**Define o dia da semana como domingo */
	SUNDAY(1),
	
	/**Define o dia da semana como segunda-feira */
	MONDAY(2),
	
	/**Define o dia da semana como terca-feira */
	TUESDAY(3),
	
	/**Define o dia da semana como quarta-feira */
	WEDNESDAY(4),
	
	/**Define o dia da semana como quinta-feira */
	THURSDAY(5),
	
	/**Define o dia da semana como sexta-feira */
	FRIDAY(6),
	
	/**Define o dia da semana como sabado */
	SATURDAY(7),

	/** Define o dia como feriado */
	HOLIDAY(8),
	
	/** Define um final de semana */
	WEEKEND(9),
	
	/** Valor neutro. */
	NONE(10);

	/** O valor do dia da semana em inteiro. */
	private final int daysOfWeek;
	
	private DaysOfWeekEnum(int daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}
	
	/**
	 * Obtem dia da semana como um inteiro primitivo. 
	 * 
	 * @return O nome como um inteiro primitivo.
	 */
	public int getDaysOfWeek() {
		return daysOfWeek;
	}
	
	/**
	 * Retorna o dia enum do dia da semana correspondente ao inteiro passado.
	 * Por exemplo:
	 * 		1 - SUNDAY
	 * 		2 - MONDAY
	 * 			...
	 * 
	 * @param daysOfWeek Dia da semana representado por um inteiro.
	 * 
	 * @return Retorna o enum do dia da semana do tipo {@link DaysOfWeekEnum}.
	 */
	public static DaysOfWeekEnum getDaysOfWeek(int daysOfWeek) {
		for (DaysOfWeekEnum daysOfWeekEnum : DaysOfWeekEnum.values()) {
			if (daysOfWeekEnum.daysOfWeek == daysOfWeek) {
				return daysOfWeekEnum;
			}
		}
		return NONE;
	}

	/**
	 * Converte um caracter que representa o pagamento para um enumerador.
	 *  
	 * @param name O caracter que representa o tipo do pagamento.
	 * @return O objeto enumerador do tipo de pagamento.
	 */
	public static DaysOfWeekEnum convert(String name) {
		for (DaysOfWeekEnum d : DaysOfWeekEnum.values()) {
			if (d.toString().equals(name)) {
				return d;
			}
		}
		return NONE;
	}

}
