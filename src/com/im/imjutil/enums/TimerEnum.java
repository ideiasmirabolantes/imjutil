package com.im.imjutil.enums;

/**
 * Enum define o tipo do tempo com seu valor em milisegundos.
 * 
 * @author Bruno Alcantara.
 */
public enum TimerEnum {
	
	/** Tempo de um minuto em milisegundos. */
	MINUTE_IN_MILLISECONDS(60000L),
	
	/** Tempo de uma hora em milisegundos. */
	HOUR_IN_MILLISECONDS(3600000L),

	/** Tempo de um dia em milisegundos. */
	DAY_IN_MILLISECONDS(86400000L),
	
	/** Tempo de uma semana em milisegundos. */
	WEEK_IN_MILLISECONDS(604800000L),
	
	/** Tempo de um mes em milisegundos. */
	MONTH_IN_MILLISECONDS(2592000000L),
	
	/** Tempo de um ano em milisegundos. */
	YEAR_IN_MILLISECONDS(31104000000L),
	
	/** Tempo zero nulo. */
	NONE(0);
	
	/** Tempo em milisegundos. */
	private final long time;
	
	/** Construtor padrao. */
	private TimerEnum(long time) {
		this.time = time;
	}
	
	/**
	 * Este metodo retorna o valor em milisegundos do tempo.
	 * 
	 * @return Long com o valor em milisegundos do tempo.
	 */
	public long getTime() {
		return time;
	}
	
}
