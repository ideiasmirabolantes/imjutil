package com.im.imjutil.enums;

public enum DateGeneratorEnum {

	/** Define a constante de dia*/
	DAY(0),
	
	/** Define a constante de semana*/
	WEEK(1),
	
	/** Define a constante de mes*/
	MONTH(2),
	
	/** Define a constante de ano*/
	YEAR(3),
	
	/** Tempo zero nulo. */
	NONE(-1);
	
	private final int code;
	
	private DateGeneratorEnum(int code){
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	
	public static DateGeneratorEnum convert(String name) {
		for (DateGeneratorEnum d : DateGeneratorEnum.values()) {
			if (d.toString().equalsIgnoreCase(name)) {
				return d;
			}
		}
		return NONE;
	}
	
	public static DateGeneratorEnum getDate(int code) {
		for (DateGeneratorEnum d: DateGeneratorEnum.values()) {
			if (d.code == code) {
				return d;
			}
		}
		return NONE;
	}
}
