package com.im.imjutil.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.validation.Convert;

/**
 * Classe implementa metodos para escrever e ler em arquivo com extensao txt.
 * 
 * @author Bruno Alcantara.
 */
public class File {
	
	/**
	 * Escreve em um arquivo txt a String da linha passada como parametro.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * @param line String para ser gravada no arquivo.
	 */
	public static void Writer(String pathFileName, String line) {
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileWriter writer = new FileWriter(file);
			BufferedWriter buffer = new BufferedWriter(writer);
			buffer.write(line);
			buffer.flush();
			buffer.close();
			writer.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a String da linha passada como parametro.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * @param line String para ser gravada no arquivo.
	 * @param append Caso verdadeiro anexa no final do arquivo a linha 
	 * 		         passada como primeiro parametro do metodo Writer.
	 */
	public static void Writer(String pathFileName, String line,
			boolean append) {
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileWriter writer = new FileWriter(file, append);
			BufferedWriter buffer = new BufferedWriter(writer);
			buffer.write(line);
			buffer.flush();
			buffer.close();
			writer.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a Lista de String passada como parametro.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * @param lines Lista de String para ser escrita no arquivo txt.
	 */
	public static void Writer(String pathFileName, List<String> lines) {
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileWriter writer = new FileWriter(file);
			BufferedWriter buffer = new BufferedWriter(writer);
			for (String line : lines) {
				buffer.write(line);
			}
			buffer.flush();
			buffer.close();
			writer.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a Lista de String passada como parametro.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * @param lines Lista de String para ser escrita no arquivo txt.
	 * @param append Caso verdadeiro anexa no final do arquivo a lista de 
	 * 				 linhas passada como primeiro parametro do metodo Writer.
	 */
	public static void Writer(String pathFileName, List<String> lines,
			boolean append) {
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileWriter writer = new FileWriter(file, append);
			BufferedWriter buffer = new BufferedWriter(writer);
			for (String line : lines) {
				buffer.write(line);
			}
			buffer.flush();
			buffer.close();
			writer.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a linha de String passada como parametro.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * @param line String para ser gravada no arquivo.
	 */
	public static void Writer(java.io.File file, String line) {
		try {
			if (file.exists()) {
				Writer(file.getCanonicalPath(), line);
			}
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a linha de String passada como parametro.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * @param line String para ser gravada no arquivo.
	 * @param append Caso verdadeiro anexa no final do arquivo a lista de 
	 * 				 linhas passada como primeiro parametro do metodo Writer.
	 */
	public static void Writer(java.io.File file, String line, boolean append) {
		try {
			if (file.exists()) {
				Writer(file.getCanonicalPath(), line, append);
			}
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Escreve em um arquivo txt a linha de String passada como parametro
	 * acrescentando uma quebra de linha a cada append.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * @param line String para ser gravada no arquivo.
	 * @param append Caso verdadeiro anexa no final do arquivo a lista de linhas
	 *            passada como primeiro parametro do metodo Writer.
	 */
	public static void WriterLn(
			java.io.File file, String line, boolean append) {
		try {
			if (file.exists()) {
				Writer(file.getCanonicalPath(), 
						Convert.toString(line, "\n"), append);
			}
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a Lista de String passada como parametro.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * @param lines Lista de String para ser escrita no arquivo txt.
	 */
	public static void Writer(java.io.File file, List<String> lines) {
		try {
			if (file.exists()) {
				Writer(file.getCanonicalPath(), lines);
			}
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Escreve em um arquivo txt a Lista de String passada como parametro.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * @param lines Lista de String para ser escrita no arquivo txt.
	 * @param append Caso verdadeiro anexa no final do arquivo a lista de 
	 * 				 linhas passada como primeiro parametro do metodo Writer.
	 */
	public static void Writer(java.io.File file, List<String> lines,
			boolean append) {
		try {
			if (file.exists()) {
				Writer(file.getCanonicalPath(), lines, append);
			}
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * Este metodo faz a leitura de todo o arquivo txt e devolve 
	 * uma lista contendo as linhas do arquivo.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * 
	 * @return Lista com as linhas do arquivo txt.
	 */
	public static List<String> read(String pathFileName) {
		List<String> list = new ArrayList<String>();
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileReader reader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(reader);
			
			while (buffer.ready()) {
				list.add(buffer.readLine());
			}
			buffer.close();
			reader.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Este metodo faz a leitura de todo o arquivo txt e devolve 
	 * uma lista contendo as linhas do arquivo.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * 
	 * @return Lista com as linhas do arquivo txt.
	 */
	public static List<String> read(java.io.File file) {
		List<String> list = new ArrayList<String>();
		try {
			if(file.exists()) {
				list = read(file.getCanonicalPath());
			}
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * Este metodo faz a leitura de todo o arquivo txt e devolve 
	 * uma lista contendo a(s) linha(s) do(s) registro(s) de detalhe do arquivo.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * 
	 * @return Lista com a(s) linha(s) do(s) registro(s) 
	 * 		   de detalhe do arquivo txt.
	 */
	public static List<String> readContent(String pathFileName) {
		List<String> list = new ArrayList<String>(200000);
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileReader reader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(reader);
			while (buffer.ready()) {
				list.add(buffer.readLine());
			}
			buffer.close();
			reader.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
		// Remove o cabecalho e o Trailer do sistema da SULACAP.
		list.remove(0);
		list.remove(list.size() - 1);
		
		return list;
	}
	
	/**
	 * Este metodo retorna o conetudo da primeira linha do arquivo txt.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * 
	 * @return String da primeira linha do arquivo txt.
	 */
	public static String readFisrtLine(String pathFileName) {
		java.io.File file = new java.io.File(pathFileName.trim());
		String line = null;
		try {
			FileReader reader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(reader);
			line = buffer.readLine();
			buffer.close();
			reader.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
		return line;
	}
	
	/**
	 * Este metodo retorna o conetudo da ultima linha do arquivo txt.
	 * 
	 * @param pathFileName Caminho completo do arquivo.
	 * 
	 * @return String da ultima linha do arquivo txt.
	 */
	public static String readLastLine(String pathFileName) {
		List<String> list = new ArrayList<String>();
		java.io.File file = new java.io.File(pathFileName.trim());
		try {
			FileReader reader = new FileReader(file);
			BufferedReader buffer = new BufferedReader(reader);
			
			while (buffer.ready()) {
				list.add(buffer.readLine());
			}
			buffer.close();
			reader.close();
		} catch (IOException e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			Logger.error(e.getMessage());
			e.printStackTrace();
		}
		return list.get(list.size() - 1);	
	}

	/**
     * Obtem a extensao do arquivo.
     * 
     * @param  file - O arquivo a se obter a extensao.
     * @return  A {@link String} da extensao do arquivo com ponto (Ex.: .html)
     */
    public static String getFileExtension(java.io.File file) {
        String extension = "";
        String filename = file.getName();
        int dotPos = filename.lastIndexOf(".");
        if (dotPos >= 0) {
            extension = filename.substring(dotPos);
        }
        return extension.toLowerCase();
    }

	/**
	 * Deleta um arquivo recebendo o caminho completo do mesmo como parametro.
	 * 
	 * @param absolutFilePath Caminho completo do arquivo a ser deletado.
	 * 
	 * @return Retorna VERDADEIRO se o arquivo foi deletado com sucesso, caso
	 *         contrario retorna FALSO.
	 */
    public static boolean deleteFile(String absolutFilePath) {
    	boolean result = false;
    	java.io.File file = new java.io.File(absolutFilePath);
		if (file.exists()) {
			result = file.delete();
		}
		return result;
    }
    
    /**
	 * Deleta um arquivo recebendo o caminho completo do mesmo como parametro.
	 * 
	 * @param file Objeto do tipo {@link java.io.File}.
	 * 
	 * @return Retorna VERDADEIRO se o arquivo foi deletado com sucesso, caso
	 *         contrario retorna FALSO.
	 */
    public static boolean deleteFile(java.io.File file) {
    	boolean result = false;
		if (file.exists()) {
			result = file.delete();
		}
		return result;
    }
    
}
