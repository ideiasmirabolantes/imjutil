package com.im.imjutil.file;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.im.imjutil.validation.Format;

/**
 * Classe que representa um arquivo enviado por HTTP. Ela armazena internamente
 * os bytes do arquivo, podendo ser acessado via stream ou array.
 * 
 * @author Felipe Zappala 
 */
public class HTTPFile {

	/** Nome do campo do formulario de upload */
	private String field;
	/** Nome do arquivo carregado */
	private String name;
	/** Tamanho em bytes do arquivo carregado */
	private long size;
	/** Bytes do arquivo carregado */
	private byte[] bytes;
	
	/**
	 * Construtor padrao
	 */
	public HTTPFile() {
		this.field = "";
		this.name = "";
		this.size = -1;
		this.bytes = new byte[0];
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public InputStream getStream() {
		return new ByteArrayInputStream(bytes);
	}

	public byte[] getBytes() {
		return bytes;
	}
	
	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public boolean isFile() {
		return this.bytes != null && this.bytes.length > 0;
	}
	
	@Override
	public String toString() {
		return Format.toString(this, "bytes");
	}
	
}
