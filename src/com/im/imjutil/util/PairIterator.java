package com.im.imjutil.util;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

/**
 * Classe que define um iterador para um objeto de pares.
 * 
 * @param <F> Primeiro valor do par.
 * @param <L> Ultimo valor do par.
 * 
 * @author Felipe Zappala
 */
class PairIterator<F, L> implements Serializable, Iterator<Pair<F, L>> {

	/** serialVersionUID */
	private static final long serialVersionUID = -7780221563945942565L;
	
	/** O mapa de pares */
	private Map<F, L> pairs;
	
	/** O iterador das chaves decorado */
	private Iterator<F> iterator;
	
	/**
	 * Cria um novo iterador para o mapa passado.
	 * 
	 * @param pairs O mapa a ser iterado
	 */
	public PairIterator(Map<F, L> pairs) {
		this.pairs = pairs;
		this.iterator = this.pairs.keySet().iterator();
	}
	
	@Override
	public boolean hasNext() {
		return iterator.hasNext();
	}

	@Override
	public Pair<F, L> next() {
		F next = iterator.next(); 
		return Pair.make(next, pairs.get(next));
	}

	@Override
	public void remove() {
		iterator.remove();
	}

}
