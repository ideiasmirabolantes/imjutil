package com.im.imjutil.util;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.im.imjutil.validation.Validator;

/**
 * Classe responsavel por fazer um pool de threads. Esta utiliza os pools 
 * do Java internamente. Sua principal funcao e a sincronia da execucao
 * da thread corrente e das threads criadas no pool. Ideal para paralelizar 
 * um processo e unificar o resultado em uma unica saida.  
 * 
 * @author Felipe Zappala
 */
public final class ThreadPool {
	
	/** O real pool de thread */
	private ExecutorService pool;
	/** Lista de resultados futuros das submicoes de tarefas */
	private List<Future<?>> futures;
	
	/**
	 * Construtor padao do {@link ThreadPool}. 
	 * Este cria um pool fixo de tamanho 100. 
	 */
	public ThreadPool() {
		this(100);
	}
	
	/**
	 * Construtor do {@link ThreadPool}. 
	 * Este cria um pool fixo de tamanho configuravel. 
	 * 
	 * @param size O tamanho do pool
	 */
	public ThreadPool(int size) {
		this(Executors.newFixedThreadPool(size));
	}
	
	/**
	 * Construtor do {@link ThreadPool}. 
	 * Este cria um pool personalizado pelo usuario.
	 * 
	 * @param pool
	 */
	public ThreadPool(ExecutorService pool) {
		this.futures = new LinkedList<Future<?>>();
		this.pool = pool;
	}
	
	/**
	 * Executa um comando no pool de threads.
	 * 
	 * @param command Uma instancia de {@link Runnable}.
	 */
	public void execute(Runnable command) {
		if (command != null) {
			this.pool.execute(command);			
		}
	}
	
	/**
	 * Submete uma tarefa no pool de threads.
	 * 
	 * @param task Uma instancia de {@link Runnable}.
	 * @return Uma instancia de {@link Future}.
	 */
	public Future<?> submit(Runnable task) {
		if (task != null) {
			Future<?> future = this.pool.submit(task);
			this.futures.add(future);
			return future;
		}
		return new NullFuture();
	}
	
	/**
	 * Submete uma tarefa no pool de threads.
	 * 
	 * @param task Uma instancia de {@link Callable}.
	 * @return Uma instancia de {@link Future}.
	 */
	public Future<?> submit(Callable<?> task) {
		if (task != null) {
			Future<?> future = this.pool.submit(task);
			this.futures.add(future);
			return future;
		}
		return new NullFuture();
	}
	
	/**
	 * Retorna a lista de resultados completados das tarefas submetidas.
	 * 
	 * @return Um lista imutavel de resultados do tipo {@link Future}. 
	 */
	public List<Future<?>> futureList() {
		return Collections.unmodifiableList(this.futures);
	}
	
	/**
	 * Executa todos os comandos no pool de threads.
	 * 
	 * @param commands Um array de comandos do tipo {@link Runnable}. 
	 */
	public void executeAll(Runnable... commands) {
		if (!Validator.isEmpty(commands)) {
			for (Runnable command : commands) {
				this.execute(command);
			}
		}
	}
	
	/**
	 * Executa todos os comandos no pool de threads.
	 * 
	 * @param commands Uma colecao de comandos do tipo {@link Runnable}.
	 */
	public void executeAll(Collection<? extends Runnable> commands) {
		if (!Validator.isEmpty(commands)) {
			for (Runnable command : commands) {
				this.execute(command);
			}
		}
	}
	
	/**
	 * Submete todas as tarefas no pool de threads.
	 * 
	 * @param tasks Um array de tarefas do tipo {@link Callable}.
	 * @return Retorna a lista de resultados futuros das tarefas.
	 */
	public List<Future<?>> submitAll(Callable<?>... tasks) {
		if (!Validator.isEmpty(tasks)) {
			for (Callable<?> task : tasks) {
				this.submit(task);
			}
		}
		return this.futureList();
	}
	
	/**
	 * Submete todas as tarefas no pool de threads.
	 * 
	 * @param tasks Uma colecao de tarefas do tipo {@link Callable}.
	 * @return Retorna a lista de resultados futuros das tarefas.
	 */
	public List<Future<?>> submitAll(Collection<? extends Callable<?>> tasks) {
		if (!Validator.isEmpty(tasks)) {
			for (Callable<?> task : tasks) {
				this.submit(task);
			}
		}
		return this.futureList();
	}
	
	/**
	 * Desliga o pool, nao permitindo a inclusao de novas tarefas ou comandos.
	 * A thread principal ficara aguardando a finalizacao de todas as threads
	 * criadas no pool.
	 */
	public void shutdownAwait() {
		try {
			this.pool.shutdown();
			this.pool.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * Desliga o pool, nao permitindo a inclusao de novas tarefas ou comandos.
	 * A thread principal continuara sua execucao independente da execucao 
	 * das threads do pool.
	 */
	public void shutdownProceed() {
		this.pool.shutdown();
	}
	
	/**
	 * Verifica se o pool se encontra desligado.
	 * 
	 * @return Retorna verdadeiro caso este estiver desligado.
	 */
	public boolean isShutdown() {
		return this.pool.isShutdown();
	}
	
	/**
	 * Verifica se o pool ja executou todas as suas threads.
	 * 
	 * @return Retorna verdadeiro caso este estiver finalizado todas as threads.
	 */
	public boolean isTerminated() {
		return this.pool.isTerminated();
	}
	
	/**
	 * Faz a thread corrente dormir por um tempo determinado.
	 * 
	 * @param millis O tempo de sono da thread em milisegundos.
	 */
	public static void sleepCurrent(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * Faz a thread corrente dormir por um tempo determinado.
	 * 
	 * @param time O tempo de sono da thread.
	 * @param unit A unidade de tempo a ser utilizada.
	 */
	public static void sleepCurrent(long time, TimeUnit unit) {
		try {
			Thread.sleep(TimeUnit.MILLISECONDS.convert(time, unit));
		} catch (InterruptedException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * Executa o comando em uma nova thread e a thread corrente 
	 * continua a sua execucao em paralelo.
	 * 
	 * @param command Uma instancia de {@link Runnable}
	 */
	public static void executeProceed(Runnable command) {
		execute(command, false);
	}
	
	/**
	 * Executa o comando em uma nova thread e a thread corrente 
	 * ficara aguardando a nova finalizar para continuar.
	 * 
	 * @param command Uma instancia de {@link Runnable}
	 */
	public static void executeAwait(Runnable command) {
		execute(command, true);
	}
	
	/**
	 * Executa um comando em uma thread.
	 */
	private static void execute(Runnable command, boolean await) {
		try {
			if (command == null) {
				return;
			}
			Thread thread = new Thread(command);
			thread.start();
			if (await) {
				thread.join();
			}
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * Classe interna que representa uma instancia nula de {@link Future} 
	 */
	private class NullFuture implements Future<Object> {

		public NullFuture() {
			// Nada faz...
		}

		@Override
		public boolean cancel(boolean mayInterruptIfRunning) {
			return false;
		}

		@Override
		public boolean isCancelled() {
			return false;
		}

		@Override
		public boolean isDone() {
			return true;
		}

		@Override
		public Object get() throws InterruptedException, ExecutionException {
			return null;
		}

		@Override
		public Object get(long timeout, TimeUnit unit)
				throws InterruptedException, ExecutionException,
				TimeoutException {
			return null;
		}
	}
	
}
