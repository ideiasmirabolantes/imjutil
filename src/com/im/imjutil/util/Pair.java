package com.im.imjutil.util;

import java.io.Serializable;

import com.im.imjutil.validation.Format;

/**
 * Classe que define um par de objetos.
 * 
 * @param <F> Primeiro valor do par.
 * @param <L> Ultimo valor do par.
 * 
 * @author Felipe Zappala
 */
public class Pair<F,L> implements Serializable, Comparable<Pair<F, L>> {

	/** serialVersionUID */
	private static final long serialVersionUID = 6623044632581002045L;
	
	/** Primeiro valor */
	private F first;
	
	/** Ultimo valor */
	private L last;

	/**
	 * Cria um novo par construido com os valores passados. 
	 * 
	 * @param <F> Primeiro valor
	 * @param <L> Ultimo valor
	 * @param first Primeiro valor do par
	 * @param last Ultimo valor do par
	 * @return Um novo par construido com os valores passados.
	 */
	public static <F, L> Pair<F, L> make(F first, L last) {
		return new Pair<F, L>(first, last);
	}
	
	/**
	 * Construtor padrao para um {@link Pair}.
	 */
	public Pair() {
		super();
	}
	
	/**
	 * Construtor para um {@link Pair}.
	 * 
	 * @param first Primeiro valor do par.
	 * @param last Ultimo valor do par
	 */
	public Pair(F first, L last) {
		this.first = first;
		this.last = last;
	}
	
	/**
	 * Obtem o primeiro valor.
	 * 
	 * @return O primeiro valor.
	 */
	public F getFirst() {
		return first;
	}

	/**
	 * Configura o primeiro valor.
	 * 
	 * @param first O primeiro valor.
	 */
	public void setFirst(F first) {
		this.first = first;
	}

	/**
	 * Obtem o ultimo valor.
	 * 
	 * @return O ultimo valor.
	 */
	public L getLast() {
		return last;
	}

	/**
	 * Configura o ultimo valor.
	 * 
	 * @param last O ultimo valor.
	 */
	public void setLast(L last) {
		this.last = last;
	}

	/**
	 * Verifica se o par esta vazio.
	 * 
	 * @return Retorna verdade quando estiver com seus atributos nulos.
	 */
	public boolean isEmpty() {
		return this.first == null && this.last == null;
	}
	
	/**
	 * Retorna o codigo de hash das propriedades do par.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((first == null) ? 0 : first.hashCode());
		result = prime * result + ((last == null) ? 0 : last.hashCode());
		return result;
	}

	/**
	 * Testa a equivalencia entre o objeto passado e o par,
	 * levando em consideracao a equivalencia entre suas propriedades.
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair<?, ?> other = (Pair<?, ?>) obj;
		if (first == null) {
			if (other.first != null)
				return false;
		} else if (!first.equals(other.first))
			return false;
		if (last == null) {
			if (other.last != null)
				return false;
		} else if (!last.equals(other.last))
			return false;
		return true;
	}

	/**
	 * Compara um par a outro levando em consideracao as propriedades
	 * coincidentes em ambos.
	 */
	@Override
	public int compareTo(Pair<F, L> other) {
		if (other != null) {
			if (this.equals(other)) {
				return 0;
				
			} else if ((this.first != null && this.last != null) 
					&& (other.first == null || other.last == null)) {
				return 1;
				
			} else if ((other.first != null && other.last != null) 
					&& (this.first == null || this.last == null)) {
				return -1;
				
			} else {
				return 0;
			}
		} 
		return 1;
	}
	
	/**
	 * Retorna o objeto no formato:<br>
	 * <code>
	 *		NomeDaClasse{atributo1:valor1,...,atributoN:valorN}
	 * </code>
	 */
	@Override
	public String toString() {
		return Format.toString(this);
	}
}
