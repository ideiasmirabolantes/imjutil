package com.im.imjutil.util;

/**
 * Classe que implementa {@link java.util.Map.Entry<K, V>} 
 * decorando um {@link Pair}.
 *   
 * @author Felipe Zappala
 */
class PairEntry<K, V> implements java.util.Map.Entry<K, V> {

	private final Pair<K, V> pair;
	
	public PairEntry(Pair<K, V> pair) {
		this.pair = pair;
	}
	
	@Override
	public K getKey() {
		return pair.getFirst();
	}

	@Override
	public V getValue() {
		return pair.getLast();
	}

	@Override
	public V setValue(V value) {
		pair.setLast(value);
		return pair.getLast();
	}
	
}
