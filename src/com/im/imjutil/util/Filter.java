package com.im.imjutil.util;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.im.imjutil.query.Query;
import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;

/**
 * Classe que representa um filtro para os relatorios.<br>
 * A chave do filtro e o nome da propriedado do objeto alvo.
 * O valor do filtro e o objeto referente a propriedade desejada.
 * 
 * @author Felipe Zappala
 * @version 2.0
 */
public class Filter implements Serializable, Comparable<Filter>, 
		Iterable<Pair<String, Object>> {

	/** serialVersionUID */
	private static final long serialVersionUID = 1058622258401689018L;

	/** Mapa que armazena os objetos do filtro. */
	private Map<String, Pair<String, Object>> filters;

	/**
	 * Determina se a operacao logica usada e 
	 * um AND (exclusivo) ou um OR (inclusivo).
	 */
	private boolean exclusive;
	
	/** Define a consulta a ser usado com o filtro atual. */
	private Query query;
	
	/** Define os limites de resultados na consulta. */
	private Pair<Integer, Integer> limits;
	
	/** Define os campos de ordenacao do resultado do filtro */
	private Map<String, Pair<String, Boolean>> orders;
	
	/**
	 * Constroi um filtro baseado em outro filtro ja existente.
	 * 
	 * @param another Filtro a ser anexado ao atual.
	 */
	public Filter(Filter another) {
		this();

		if (another != null) {
			this.filters.putAll(another.filters);
			this.exclusive = another.exclusive;
			this.limits = another.limits;
			this.query = another.query;
		}
	}
	
	/**
	 * Constroi um filtro padrao para relatorios.
	 */
	public Filter() {
		this.filters = new LinkedHashMap<String, Pair<String, Object>>();
		this.orders = new LinkedHashMap<String, Pair<String, Boolean>>();
		this.exclusive = true;
		this.query = null;
		this.limits = Pair.make(-1, -1);
	}
	
	/**
	 * Adiciona um novo valor ao filtro.
	 * 
	 * @param name Nome da propriedade. 
	 * @param object Valor a ser filtrado.
	 * @return O filtro atual.
	 */
	public Filter add(String name, Object object) {
		return this.addFilter(name, object);
	}
	
	/**
	 * Adiciona um novo valor ao filtro.
	 * 
	 * @param name Nome da propriedade. 
	 * @param object Valor a ser filtrado.
	 * @return O filtro atual.
	 */
	public Filter addFilter(String name, Object object) {
		if (Validator.isValid(name, object)) {
			this.filters.put(name, Pair.make(name, object));
		}
		return this;
	}
	
	/**
	 * Constroi um filtro consultavel.
	 */
	public Filter(Query query) {
		this();
		this.setQuery(query);
	}
	
	/**
	 * Adiciona todos os valores de um filtro ao filtro atual.
	 * 
	 * @param anotherFilters os filtros a serem agregados.
	 * @return O filtro atual.
	 */
	public Filter addFilters(Filter... anotherFilters) {
		if (anotherFilters != null) {
			for (Filter another : anotherFilters) {
				this.filters.putAll(another.filters);
				this.exclusive = another.exclusive;
				this.limits = another.limits;
				this.query = another.query;
			}
		}
		return this;
	}
	
	/**
	 * Remove um valor no filtro.
	 * 
	 * @param name Nome da propriedade. 
	 * @return O filtro atual.
	 */
	public Filter remove(String name) {
		return this.removeFilter(name);
	}
	
	/**
	 * Remove um valor no filtro.
	 * 
	 * @param name Nome da propriedade. 
	 * @return O filtro atual.
	 */
	public Filter removeFilter(String name) {
		if (Validator.isValid(name)) {
			this.filters.remove(name);
		}
		return this;
	}
	
	/**
	 * Remove todos os valores de um filtro no filtro atual.
	 * 
	 * @param anotherFilter o filtro a ser agregado. 
	 * @return O filtro atual.
	 */
	public Filter removes(Filter anotherFilter) {
		return this.removeFilters(anotherFilter);
	}
	
	/**
	 * Remove todos os valores de um filtro no filtro atual.
	 * 
	 * @param anotherFilter o filtro a ser agregado. 
	 * @return O filtro atual.
	 */
	public Filter removeFilters(Filter anotherFilter) {
		if (Validator.isValid(anotherFilter) && !anotherFilter.isEmpty()) {
			for (Pair<String, Object> pair : anotherFilter) {
				this.filters.remove(pair.getFirst());
			}
		}
		return this;
	}
	
	/**
	 * Obtem um valor filtrado pelo nome da propriedade. 
	 * 
	 * @param <F> Tipo do retorno do valor do filtro objeto.
	 * @param name Nome da propriedade.
	 * @return O valor a ser filtrado.
	 */
	public <F> F get(String name) {
		return this.getFilter(name);
	}
	
	/**
	 * Obtem um valor filtrado pelo nome da propriedade. 
	 * 
	 * @param <F> Tipo do retorno do valor do filtro objeto.
	 * @param name Nome da propriedade.
	 * @return O valor a ser filtrado.
	 */
	@SuppressWarnings("unchecked")
	public <F> F getFilter(String name) {
		Pair<String, Object> pair = this.filters.get(name);

		if (pair != null) {
			return (F) pair.getLast();
		}
		return null;
	}
	
	/**
	 * Verifica se o filtro e inclusivo, se usa a operacao logica OR.
	 * @return o tipo de operacao logica
	 */
	public boolean isInclusive() {
		return !exclusive;
	}

	/**
	 * Verifica se o filtro e exclusivo, se usa a operacao logica AND.
	 * @return o tipo de operacao logica
	 */
	public boolean isExclusive() {
		return exclusive;
	}
	
	/**
	 * Configura o filtro para inclusivo, usar a operacao logica OR.
	 * 
	 * @return O filtro atual.
	 */
	public Filter setInclusive() {
		this.exclusive = false;
		return this;
	}
	
	/**
	 * Configura o filtro para exclusivo, usar a operacao logica AND.
	 * 
	 * @return O filtro atual.
	 */
	public Filter setExclusive() {
		this.exclusive = true;
		return this;
	}
	
	/**
	 * Obtem a consulta a ser executada com o filtro atual.
	 * 
	 * @return A consulta a ser executada.
	 */
	public Query getQuery() {
		return query;
	}

	/**
	 * Configura a consulta a ser executada com o filtro atual.
	 * 
	 * @param query A consulta a ser executada.
	 * @return O filtro atual.
	 */
	public Filter setQuery(Query query) {
		if (query != null) {
			this.query = query;
		}
		return this;
	}
	
	/**
	 * Define se o filtro esta utilizando consulta personalizada.
	 * 
	 * @return Verdadeiro caso o filtro for consultavel.
	 */
	public boolean isQueriable() {
		return (query != null);
	}
	
	/**
	 * Obtem os limites de resultados nas consultas.
	 * O primeiro valor representa o valor inicial e o segundo o valor 
	 * final dos resultados a serem retornados.
	 * Retorna <code>-1</code> em seus valores casos nao for configurado. 
	 * 
	 * @return Os limites de resultados.
	 */
	public Pair<Integer, Integer> getLimits() {
		return limits;
	}

	/**
	 * Configura os limites de resultados nas consultas.
	 * O primeiro valor representa o valor inicial e o segundo o valor 
	 * final dos resultados a serem retornados.
	 * 
	 * @param limits Os limites de resultados.
	 * @return O filtro atual. 
	 */
	public Filter setLimits(Pair<Integer, Integer> limits) {
		if (limits != null) {
			this.limits = limits;
		}
		return this;
	}
	
	/**
	 * Configura os limites de resultados nas consultas.
	 * O primeiro valor representa o valor inicial e o segundo o valor 
	 * final dos resultados a serem retornados.
	 * 
	 * @param first Define o indice do primeiro resultado. 
	 * @param max Define o numero maximo de resultados.
	 * @return O filtro atual. 
	 */
	public Filter setLimits(int first, int max) {
		if ((first + max) > 0) {
			this.setLimits(Pair.make(first, max));			
		}
		return this;
	}

	/**
	 * Define se o filtro esta utilizando restricao de limites.
	 * 
	 * @return Verdadeiro caso o filtro for limitado.
	 */
	public boolean isLimited() {
		return (limits.getFirst() > -1) && (getLimits().getLast() > -1); 
	}
	
	/**
	 * Define se o filtro esta utilizando ordenacao de resultados.
	 * 
	 * @return Verdadeiro caso o filtro for ordenado.
	 */
	public boolean isOrdened() {
		return !this.orders.isEmpty(); 
	}	
	
	/**
	 * Adiciona um campo de ordenacao de resultados do filtro.
	 * 
	 * @param field Campo de ordenacao do filtro.
	 * @param asc Define ordenacao ascendente, ou seja, crescente.
	 * @return O filtro atual.
	 */
	public Filter addOrder(String field, boolean asc) {
		if (Validator.isValid(field)) {
			this.orders.put(field, Pair.make(field, asc));	
		}
		return this;
	}
	
	/**
	 * Adiciona um campo de ordenacao ascendente de resultados do filtro.
	 * 
	 * @param field Campo de ordenacao do filtro.
	 * @return O filtro atual.
	 */
	public Filter addOrder(String field) {
		return addOrder(field, true);
	}

	/**
	 * Remove um campo de ordenacao de resultados do filtro.
	 * 
	 * @param field O nome campo de ordenacao a ser removido.
	 * @return O filtro atual.
	 */
	public Filter removeOrder(String field) {
		if (Validator.isValid(field)) {
			this.orders.remove(field);
		}
		return this;
	}
	
	/**
	 * Retorna o conjunto de campos de ordenacao do filtro.
	 * 
	 * @return Os pares de ordenacao [campo, ascendencia]. 
	 */
	public Collection<Pair<String, Boolean>> getOrders() {
		return this.orders.values();
	}
	
	/**
	 * Retorna o conjunto de valores do filtro.
	 * 
	 * @return Os pares de filtragem [filtro, valor].
	 */
	public Collection<Pair<String, Object>> getFilters() {
		return this.filters.values();
	}
	
	/**
	 * Verifica a existencia do valor no filtro.
	 *  
	 * @param name Nome da propriedade.
	 * @return Retorna verdade se o filtro contiver a propriedade.
	 */
	public boolean contains(String name) {
		return this.filters.containsKey(name);
	}
	
	/**
	 * Verifica se o filtro esta vazio. 
	 * 
	 * @return Retorna verdade se o filtro estiver vazio.
	 */
	public boolean isEmpty() {
		return this.filters.isEmpty();
	}
	
	/**
	 * Retorna o codigo de hash das propriedades do filtro.
	 */
	@Override
	public int hashCode() {
		return this.filters.hashCode();
	}
	
	/**
	 * Testa a equivalencia entre o objeto passado e o filtro,
	 * levando em consideracao a equivalencia entre suas propriedades.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Filter) {
			Filter another = (Filter) obj;
			return this.filters.equals(another);
		}
		return false;
	}
	
	/**
	 * Retorna o objeto no formato:<br>
	 * <code>
	 *		NomeDaClasse{atributo1:valor1,...,atributoN:valorN}
	 * </code>
	 */
	@Override
	public String toString() {
		return Format.toString(this);
	}

	/**
	 * Compara um filtro a outro levando em consideracao as propriedades
	 * coincidentes em ambos.
	 */
	@Override
	public int compareTo(Filter o) {
		if (this.equals(o)) {
			return 0;
			
		} else if (this.filters.keySet().containsAll(o.filters.keySet())) {
			return 1;
			
		} else {
			return -1;
		}
	}

	/**
	 * Itera sobre os valores das propriedades existentes no filtro.
	 * Segue a ordem que foram adicionadas no mesmo.
	 */
	@Override
	public Iterator<Pair<String, Object>> iterator() {
		return this.filters.values().iterator();
	}

}
