package com.im.imjutil.util;

import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Classe responsavel por implementar um cronometro simples.Possui recursos 
 * como marcacao de voltas e cronometragem estatica por thread.
 * <br>
 * <pre>
 * 
 * 	// Exemplo estatico:
 * 	Chronometer.startCount();
 *	Thread.sleep(65345);
 *	Chronometer.stopCount();
 *	System.out.println(Chronometer.format(Chronometer.countTime()));
 *
 *	// Exemplo instancia:
 *	Chronometer chronometer = new Chronometer();
 *	chronometer.start();
 *	Thread.sleep(1000);
 *	System.out.println(chronometer.time());
 *	Thread.sleep(2000);
 *	System.out.println(chronometer.lap());
 *	Thread.sleep(3000);
 *	System.out.println(Chronometer.toSeconds(chronometer.lap()));
 *	Thread.sleep(4000);		
 *	System.out.println(Chronometer.format(chronometer.lap()));
 *	chronometer.stop();
 *	System.out.println(chronometer.laps());
 *
 * </pre>
 * @author Felipe Zappala
 */
public class Chronometer {

	private long startTime;
	private long stopTime;
	private boolean running;
	private List<Long> laps;
	
	private static ThreadLocal<Chronometer> staticChronometer; 
	static {
		staticChronometer = new ThreadLocal<Chronometer>();
	}
	
	public Chronometer() {
		this.startTime = 0;
		this.stopTime = 0;
		this.running = false;
		this.laps = new LinkedList<Long>();
	}
	
	/**
	 * Obtem a instancia estatico do cronometro para a thread corrente. 
	 */
	private static Chronometer threadChronometer() {
		Chronometer chron = staticChronometer.get(); 
		if (chron == null) {
			chron = new Chronometer();
			staticChronometer.set(chron);			
		}
		return chron; 
	}

	/**
	 * Inicia o contador do cronometro estatico para a thread corrente. 
	 */
	public static void startCount() {
		threadChronometer().start();
	}
	
	/**
	 * Para o contador do cronometro estatico para a thread corrente. 
	 */
	public static void stopCount() {
		threadChronometer().stop();
	}
	
	/**
	 * Retorna o tempo do contador do cronometro estatico para a thread corrente. 
	 */
	public static long countTime() {
		return threadChronometer().time();
	}

	/**
	 * Inicia ou reinicia o cronometro.
	 */
	public void start() {
		if (!running) {
			startTime = current() - (stopTime - startTime);
			running = true;
		}
	}

	/**
	 * Para ou pausa o cronometro.
	 */
	public void stop() {
		if (running) {
			stopTime = current();
			running = false;
		}
	}

	/**
	 * Obtem o tempo do cronometro.
	 */
	public long time() {
		if (startTime != 0) {
			if (!running) {
				return stopTime - startTime;
			}
			return current() - startTime;
		}
		return 0;
	}

	/**
	 * Zera o cronometro.
	 */
	public void reset() {
		startTime = 0;
		stopTime = 0;
	}

	/**
	 * Obtem o tempo corrente
	 */
	private long current() {
		return System.currentTimeMillis();
	}

	/**
	 * Marca uma volta no cronometro e retorna o tempo desta.
	 */
	public long lap() {
		long now = time();
		laps.add(now);
		return now;
	}

	/**
	 * Retorna a lista imutavel de voltas do cronometro. 
	 */
	public List<Long> laps() {
		return Collections.unmodifiableList(laps);
	}

	/**
	 * Formata o milessegundo passado em hora, minuto, segundo, no padrao:
	 * {@code 99h:99m99.99s}
	 */
	public static String format(long time) {
		StringBuilder sb = new StringBuilder();
		long h, m, s, t;
		
		t = time;
		h = t / 1000 / 60 / 60;
		sb.append(h).append("h:");
		
		t = t - h * 1000 * 60 * 60;
		m = t / 1000 / 60;
		sb.append(m).append("m:");
		
		t = t - m * 1000 * 60;
		s = t / 1000;
		sb.append(s).append('.');
		
		t = t - s * 1000;
		sb.append(t).append('s');
		
		return sb.toString();
	}
	
	/**
	 * Converte o milessegundo passado em segundo. 
	 */
	public static long toSeconds(long time) {
		return time / 1000;
	}
	
	/**
	 * Converte o milessegundo passado em minuto. 
	 */
	public static long toMinutes(long time) {
		return time / 1000 / 60;
	}
	
	/**
	 * Converte o milessegundo passado em hora. 
	 */
	public static long toHours(long time) {
		return time / 1000 / 60 / 60;
	}
	
	/**
	 * Retorna a data {@link Date} de inicio da cronometragem. 
	 */
	public Date getStartDate() {
		return new Date(startTime);
	}
	
	/**
	 * Retorna a data {@link Date} de fim da cronometragem. 
	 */
	public Date getStopDate() {
		return new Date(stopTime);
	}
	
}