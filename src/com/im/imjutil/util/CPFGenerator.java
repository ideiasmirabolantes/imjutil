package com.im.imjutil.util;

import java.util.ArrayList;
import java.util.List;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.validation.Convert;

/**
 * Classe gera cpf imaginario utilizando o calculo do primeiro e segundo digitos
 * verifcadores do cpf. O algoritmo aqui empregado pode ser encontrado em.
 * 
 * @see http://imasters.uol.com.br/artigo/2410/javascript/algoritmo_do_cpf/
 * Autor do artigo: Thiago Prado, contato: pradogeracao@hotmail.com
 *  
 * @author Bruno Alcantara.
 */
public class CPFGenerator {
	
	/**
	 * Calcula o primeiro digito verificador do cpf.
	 * 
	 * @param numbers Lista dos primeiros 9 numeros aleatorios do cpf.
	 * 
	 * @return Retorna um inteiro do primeiro digito verificador do cpf.
	 */
	private static int firstVerificationCode(List<Integer> numbers) {
		int multi = 10;
		int total = 0;
		int currentMulti = 0;
		List<Integer> totals = new ArrayList<Integer>();
		
		// Multiplica os numeros com a distribuicao dos digitos
		for (Integer number : numbers) {
			currentMulti = number * multi;
			totals.add(currentMulti);
			total += currentMulti;
			--multi;
		}
		
		int rest = total % 11;
		
		if (rest < 2) {
			return 0;
		} 
		
		return (11 - rest);
	}
	
	/**
	 * Calcula o segundo digito verificador do cpf.
	 * 
	 * @param numbers Lista dos primeiros 9 numeros aleatorios do cpf.
	 * 
	 * @return Retorna um inteiro do segundo digito verificador do cpf.
	 */
	private static int secondVerificationCode(List<Integer> numbers, int first) {
		int multi = 11;
		int total = 0;
		int currentMulti = 0;
		List<Integer> totals = new ArrayList<Integer>();
		
		// Multiplica os numeros com a distribuicao dos digitos
		for (Integer number : numbers) {
			currentMulti = number * multi;
			totals.add(currentMulti);
			total += currentMulti;
			--multi;
		}
		
		currentMulti = first * multi;
		totals.add(currentMulti);
		total += currentMulti;

		int rest = total % 11;
		
		if (rest < 2) {
			return 0;
		} 
		
		return (11 - rest);
	}
	
	/**
	 * Gera o cpf imaginario.
	 * 
	 * @return Retorna um long contendo o cpf imaginario.
	 */
	public static long generates() {
		List<Integer> numbers = new ArrayList<Integer>();
		
		for (int i = 0; i < 9; i++) {
			numbers.add(0 + (int)(9 * Math.random()));
		}
		
		int first = firstVerificationCode(numbers);
		int second = secondVerificationCode(numbers, first);
		
		numbers.add(first);
		numbers.add(second);
		
		StringBuilder number = new StringBuilder();
		
		
		for (Integer n : numbers) {
			number.append(n);
		}
			
		return Convert.toLong(number.toString());
	}

	/**
	 * Gera o cpf imaginario.
	 * 
	 * @param numbers Lista de numeros imaginarios para gerar o cpf. OBS.: Esta
	 *            lista deve conter apenas os 9 primeiros digitos do cpf, os
	 *            outros 2 (digitos verificadores) sao gerados pelo algoritmo
	 *            interno da classe {@link CPFGenerator}.
	 *            
	 * @return Retorna um long contendo o cpf imaginario.
	 */
	public static long generates(List<Integer> numbers) {
		int first = firstVerificationCode(numbers);
		int second = secondVerificationCode(numbers, first);
		
		numbers.add(first);
		numbers.add(second);
		
		StringBuilder number = new StringBuilder();
		
		for (Integer n : numbers) {
			number.append(n);
		}
			
		return Convert.toLong(number.toString());
	}
	
	public static void main(String[] args) {
		Logger.debug("result: ", CPFGenerator.generates());
	}
	
}
