package com.im.imjutil.util;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;

import com.im.imjutil.sequence.Sequence;
import com.im.imjutil.validation.Validator;

/**
 * Classe utilitaria responsavel pela manipulacao de albuns, colecoes do java,
 * facilitando a construcao de algumas destas e conversoes entre elas. 
 *  
 * @author Felipe Zappala
 */
public final class Albums {

	/** Proibido a instanciacao */
	private Albums() {
		super();
	}
	
	/**
	 * Retorna a colecao passada como um array.
	 * 
	 * @param <E> Tido da colecao
	 * @param collection A colecao {@link Collection}
	 * @return O array da colecao.
	 */
	@SuppressWarnings("unchecked")
	public static <E> E[] asArray(Collection<E> collection) {
		if (!Validator.isEmpty(collection)) {
			return (E[]) collection.toArray(new Object[0]);	
		}
		return (E[]) new Object[0];
	}
	
	/**
	 * Retorna um array dos elementos passados. 
	 * 
	 * @param <E> Tido da colecao
	 * @param elements Os elementos do vetor {@link E[]}
	 * @return O array da colecao.
	 */
	@SuppressWarnings("unchecked")
	public static <E> E[] asArray(E... elements) {
		if (!Validator.isEmpty(elements)) {
			return elements;	
		}
		return (E[]) new Object[0];
	}
	
	/**
	 * Retorna um vetor dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos do vetor {@link Vector}
	 * @return O vetor dos elementos
	 */
	public static <E> Vector<E> asVector(E... elements) {
		Vector<E> vector = new Vector<E>();

		if (!Validator.isEmpty(elements)) {
			vector.addAll(Arrays.asList(elements));
		}
		return vector; 
	}
	
	/**
	 * Retorna uma pilha dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos da pilha {@link Stack}
	 * @return A pilha dos elementos
	 */
	public static <E> Stack<E> asStack(E... elements) {
		Stack<E> stack = new Stack<E>();

		if (!Validator.isEmpty(elements)) {
			stack.addAll(Arrays.asList(elements));
		}
		return stack;
	}
	
	/**
	 * Retorna uma fila dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos da fila {@link Queue}
	 * @return A fila dos elementos
	 */
	public static <E> Queue<E> asQueue(E... elements) {
		Queue<E> queue = new LinkedList<E>();

		if (!Validator.isEmpty(elements)) {
			queue.addAll(Arrays.asList(elements));
		}
		return queue;
	}
 	
	/**
	 * Retorna uma fila dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos da fila {@link Queue}
	 * @return A fila dos elementos
	 */
	public static <E> Deque<E> asDeque(E... elements) {
		Deque<E> deque = new ArrayDeque<E>();

		if (!Validator.isEmpty(elements)) {
			deque.addAll(Arrays.asList(elements));
		}
		return deque;
	}
	
	/**
	 * Retorna uma fila de prioridade dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos da fila {@link PriorityQueue}
	 * @return A fila dos elementos
	 */
	public static <E> Queue<E> asSortedQueue(E... elements) {
		Queue<E> queue = new PriorityQueue<E>();

		if (!Validator.isEmpty(elements)) {
			queue.addAll(Arrays.asList(elements));
		}
		return queue;
	}
	
	/**
	 * Retorna uma lista dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos da lista {@link List}
	 * @return A fila dos elementos
	 */
	public static <E> List<E> asList(E... elements) {
		List<E> list = new ArrayList<E>();

		if (!Validator.isEmpty(elements)) {
			list = Arrays.asList(elements);
		}
		return list; 
	}
	
	/**
	 * Retorna uma lista ordenada dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos da lista {@link LinkedList}
	 * @return A fila dos elementos
	 */
	public static <E> List<E> asSortedList(E... elements) {
		List<E> list = new LinkedList<E>();

		if (!Validator.isEmpty(elements)) {
			list = Arrays.asList(elements);
		}
		return list; 
	}
	
	/**
	 * Retorna um conjunto dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos do conjunto {@link Set}
	 * @return O conjunto dos elementos
	 */
	public static <E> Set<E> asSet(E... elements) {
		Set<E> set = new LinkedHashSet<E>();

		if (!Validator.isEmpty(elements)) {
			set.addAll(Arrays.asList(elements));
		}
		return set;
	}
	
	/**
	 * Retorna um conjunto ordenado dos elementos passados. 
	 * 
	 * @param <E> O tipo do elemento
	 * @param elements Os elementos do conjunto {@link SortedSet}
	 * @return O conjunto dos elementos
	 */
	public static <E> Set<E> asSortedSet(E... elements) {
		Set<E> set = new TreeSet<E>();

		if (!Validator.isEmpty(elements)) {
			set.addAll(Arrays.asList(elements));
		}
		return set;
	}
	
	@SuppressWarnings("unchecked")
	private static <K,V> Map<K, V> asMap(Map<K, V> map, Object... keyValue) {
		if (!Validator.isEmpty(keyValue) && keyValue.length % 2 == 0) {
			for (int i = 0; i < keyValue.length; i += 2) {
				map.put((K)keyValue[i], (V)keyValue[i+1]);
			}
		}
		return map;
	}
	
	/**
	 * Retorna um mapa dos pares de elementos passados. 
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param elements Os elementos do conjunto {@link Map}
	 * @return O mapa dos elementos
	 */
	public static <K,V> Map<K, V> asMap(Object... keyValue) {
		return asMap(new HashMap<K, V>(), keyValue);
	}
	
	/**
	 * Retorna um mapa dos pares de elementos passados. 
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param elements Os elementos do conjunto {@link Map}
	 * @return O mapa dos elementos
	 */
	public static <K,V> Map<K, V> asHashTable(Object... keyValue) {
		return asMap(new Hashtable<K, V>(), keyValue);
	}
	
	/**
	 * Retorna um mapa ordenado dos pares de elementos passados. 
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param elements Os elementos do conjunto {@link SortedMap}
	 * @return O mapa dos elementos
	 */
	public static <K,V> Map<K, V> asSortedMap(Object... keyValue) {
		return asMap(new TreeMap<K, V>(), keyValue);
	}
	
	private static <K,V> Map<K, V> asMap(Map<K, V> map, Pair<K,V>... pairs) {
		if (!Validator.isEmpty(pairs)) {
			for (Pair<K, V> pair : pairs) {
				map.put(pair.getFirst(), pair.getLast());
			}
		}
		return map;
	}
	
	/**
	 * Retorna um mapa dos pares de elementos passados. 
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param elements Os elementos do conjunto {@link Map}
	 * @return O mapa dos elementos
	 */
	public static <K,V> Map<K, V> asMap(Pair<K,V>... pairs) {
		return asMap(new HashMap<K, V>(), pairs);
	}
	
	/**
	 * Retorna um mapa dos pares de elementos passados. 
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param elements Os elementos do conjunto {@link Map}
	 * @return O mapa dos elementos
	 */
	public static <K,V> Map<K, V> asHashTable(Pair<K,V>... pairs) {
		return asMap(new Hashtable<K, V>(), pairs);
	}
	
	/**
	 * Retorna um mapa ordenado dos pares de elementos passados. 
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param elements Os elementos do conjunto {@link SortedMap}
	 * @return O mapa dos elementos
	 */
	public static <K,V> Map<K, V> asSortedMap(Pair<K,V>... pairs) {
		return asMap(new TreeMap<K, V>(), pairs);
	}
	
	/**
	 * Retorna um mapa que nao pode ser modificado.
	 * 
	 * @param <K> O tipo da chave do par
	 * @param <V> O tipo do valor do par
	 * @param map O mapa a ser imutavel.
	 * @return O mapa imutavel.
	 */
	public static <K,V> Map<K, V> unmodifiable(Map<K, V> map) {
		return Collections.unmodifiableMap(map);
	}
	
	/**
	 * Retorna uma lista que nao pode ser modificado.
	 * 
	 * @param <E> O tipo do elemento
	 * @param list A lista a ser imutavel.
	 * @return A lista imutavel.
	 */
	public static <E> List<E> unmodifiable(List<E> list) {
		return Collections.unmodifiableList(list);
	}
	
	/**
	 * Retorna um conjunto que nao pode ser modificado.
	 * 
	 * @param <E> O tipo do elemento
	 * @param set O conjunto a ser imutavel.
	 * @return O conjunto imutavel.
	 */
	public static <E> Set<E> unmodifiable(Set<E> set) {
		return Collections.unmodifiableSet(set);
	}
	
	/**
	 * Retorna uma colecao que nao pode ser modificado.
	 * 
	 * @param <E> O tipo do elemento
	 * @param collection A colecao a ser imutavel.
	 * @return A colecao imutavel.
	 */
	public static <E> Collection<E> unmodifiable(Collection<E> collection) {
		return Collections.unmodifiableCollection(collection);
	}

	/**
	 * Retorna uma colecao vazia que nao pode ter elementos.
	 * 
	 * @param <E> O tipo do elemento
	 * @return Uma colecao vazia.
	 */
	public static <E> Collection<E> emptyCollection() {
		return Collections.emptySet();
	}
	
	/**
	 * Retorna uma lista vazia que nao pode ter elementos.
	 * 
	 * @param <E> O tipo do elemento
	 * @return Uma lista vazia.
	 */
	public static <E> List<E> emptyList() {
		return Collections.emptyList();
	}
	
	/**
	 * Retorna um conjunto vazio que nao pode ter elementos.
	 * 
	 * @param <E> O tipo do elemento
	 * @return Um conjunto vazio.
	 */
	public static <E> Set<E> emptySet() {
		return Collections.emptySet();
	}
	
	/**
	 * Retorna um mapa vazio que nao pode ter elementos.
	 * 
	 * @param <E> O tipo do elemento
	 * @return Um mapa vazio.
	 */
	public static <K, V> Map<K, V> emptyMap() {
		return Collections.emptyMap();
	}
	
	/**
	 * Converte a colecao passada para uma lista
	 * 
	 * @param <E> O tipo do elemento
	 * @param collection a colecao a ser convertida
	 * @return A lista dos elementos da colecao
	 */
	public static <E> List<E> toList(Collection<E> collection) {
		if (!Validator.isEmpty(collection)) {
			List<E> list = new ArrayList<E>(collection.size());
			for (E e : collection) {
				list.add(e);
			}
			return list;
		}
		return new ArrayList<E>();
	}
	
	/**
	 * Converte a colecao passada para um conjunto
	 * 
	 * @param <E> O tipo do elemento
	 * @param collection a colecao a ser convertida
	 * @return O conjunto dos elementos da colecao
	 */
	public static <E> Set<E> toSet(Collection<E> collection) {
		if (!Validator.isEmpty(collection)) {
			Set<E> set = new LinkedHashSet<E>(collection.size());
			for (E e : collection) {
				set.add(e);
			}
			return set;
		}
		return new HashSet<E>();
	}
	
	/**
	 * Converte a colecao passada para um mapa
	 * 
	 * @param <E> O tipo do elemento
	 * @param collection a colecao a ser convertida
	 * @return O mapa dos elementos da colecao
	 */
	public static <K, V> Map<K, V> toMap(Collection<Pair<K, V>> collection) {
		Map<K, V> map = new HashMap<K, V>();
		if (!Validator.isEmpty(collection)) {
			for (Pair<K, V> pair : collection) {
				map.put(pair.getFirst(), pair.getLast());
			}
		}
		return map;
	}
	
	/**
	 * Converte a colecao passada para um mapa
	 * 
	 * @param <E> O tipo do elemento
	 * @param collection a colecao a ser convertida
	 * @return O mapa dos elementos da colecao
	 */
	public static <K, V> Map<K, V> toEntryMap(Collection<Entry<K, V>> collection) {
		Map<K, V> map = new HashMap<K, V>();
		if (!Validator.isEmpty(collection)) {
			for (Map.Entry<K, V> entry : collection) {
				map.put(entry.getKey(), entry.getValue());
			}
		}
		return map;
	}
	
	/**
	 * Retorna a representacao em {@link String} da colecao passada.
	 * 
	 * @param collection A colecao a ser representada.
	 * @return A representacao da colecao.
	 */
	public static String toString(Collection<?> collection) {
		if (collection != null) {
			return collection.toString();
		}
		return "";
	}
	
	/**
	 * Retorna a representacao em {@link String} do mapa passada.
	 * 
	 * @param map O mapa a ser representado.
	 * @return A representacao do mapa.
	 */
	public static String toString(Map<?, ?> map) {
		if (map != null) {
			return map.toString();
		}
		return "";
	}
	
	/**
	 * Retorna a representacao em {@link String} do array passado.
	 * 
	 * @param array O array a ser representado.
	 * @return A representacao do array.
	 */
	public static <A> String toString(A[] array) {
		if (array != null) {
			return Arrays.toString(array);
		}
		return "";
	}

	/**
	 * Retorna uma lista de numeros na faixa entre o valor zero inclusivo
	 * e o ultimo exclusivo, incrementando no passo de um.
	 */
	public static List<Long> range(long last) {
		return range(0L, last, 1L);
	}
	
	/**
	 * Retorna uma lista de numeros na faixa entre o primeiro valor inclusivo
	 * e o ultimo exclusivo, incrementando no passo um.
	 */
	public static List<Long> range(long first, long last) {
		return range(first, last, 1L);
	}
	
	/**
	 * Retorna uma lista de numeros na faixa entre o primeiro valor inclusivo
	 * e o ultimo exclusivo, incrementando no passo passado.
	 */
	public static List<Long> range(long first, long last, long step) {
		List<Long> range = new ArrayList<Long>(Math.abs((int)last));
		long i = first;
		
		if (last > 0) {
			while (i < last) {
				range.add(i);
				i += Math.abs(step);
			}
		} else {
			while (i > last) {
				range.add(i);
				i -= Math.abs(step);
			}
		}
		return range;
	}
	
	/**
	 * Retorna a lista gerada pelo intervalo da sequencia passada. 
	 */
	public static  <T> List<T> range(Sequence sequence) {
		List<T> range = new LinkedList<T>();
		
		T step;
		while ((step = sequence.next()) != null) {
			range.add(step);
		}
		return range;
	}
	
	/**
	 * Retorna um iterador {@link Iterator<E>} de pares para o mapa passado. 
	 */
	public static <K, V> Iterator<Pair<K, V>> pairIterator(Map<K, V> pairs) {
		return new PairIterator<K, V>(pairs);
	}
	
	/**
	 * Retorna uma entrada de mapa {@link Map.Entry<K, V>} para o par passado. 
	 */
	public static <K, V> Map.Entry<K, V> pairEntry(Pair<K, V> pair) {
		return new PairEntry<K, V>(pair);
	}

	/**
	 * Retorna um mapa classificado pela ordem natural dos elementos.
	 */
	public static <K, V> SortedMap<K, V> sort(Map<K, V> map) {
		return sort(map, new TreeSet<K>().comparator());
	}
	
	/**
	 * Retorna um mapa classificado pela ordem definida pelo comparador passado. 
	 */
	public static <K, V> SortedMap<K, V> sort(Map<K, V> map, 
			Comparator<? super K> comparator) {
		SortedMap<K, V> sortedMap = new TreeMap<K, V>(comparator);
		for (Map.Entry<K, V> entry : map.entrySet()) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
}
