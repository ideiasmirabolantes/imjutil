package com.im.imjutil.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.NetworkInterface;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.springframework.web.util.HtmlUtils;

import com.im.imjutil.cipher.Base64Coder;
import com.im.imjutil.cipher.Cipher;
import com.im.imjutil.config.Configurator;
import com.im.imjutil.email.Email;
import com.im.imjutil.email.EmailSender;
import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.logging.Logger;
import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;
import com.sun.org.apache.bcel.internal.classfile.Code;

/**
 * Classe de utilidades gerais.
 * 
 * @author Bruno Alcantara.
 * @author Ygor Fonseca.
 * @author Felipe Zappala.
 */
public final class Util {
	
	private Util() {
		// Proibida a instanciacao
	}
	
	/**
	 * Este metodo substitui os espacos em branco pelo caractere '_' no nome 
	 * passado para o metodo, e altera todo nome para minusculo, removendo
	 * tambem os seus acentos.
	 * 
	 * @param name Nome para ser modficado.
	 * @return Retorna o nome modificado.
	 */
	public static String getUnifiedName(String name) {
		return removeAccents(name).trim().replaceAll("\\s", "_").toLowerCase();
	}
	
	/**
	 * Este metodo substitui os espacos em branco pelo caractere '-' no nome 
	 * passado para o metodo, e altera todo nome para minusculo, removendo
	 * tambem os seus acentos e as palavras 'stop keywords', 
	 * tipo 'de', 'em', 'o', 'as', etc.
	 */
	public static String getCleanName(String name) {
		if (Validator.isValid(name)) {
			List<String> list = parseCSV(Convert.toString(
					Util.class.getResourceAsStream("stopwords.csv")));
			
			String result = removeAccents(name).trim()
						.replaceAll("\\s+", "-").toLowerCase();
			
			for (String string : list) {
				result = result.replaceAll(Convert.toString("-",string,"-"),"-");
			}
			return result;
		}
		return "";
	}
	
	/**
	 * Remove os caracteres especias da string passada subistituindo por vazio.
	 * Caracteres: {@code
	 *     - % $ # @ ! & * {} () [] ? = + § £ ¢ ¬ _ | \ / , . ; : ' " ~ ^ ` 
	 *  }
	 */
	public static String removeChars(String str) {
		return removeChars(str, "");
	}
	
	/**
	 * Remove os caracteres especias da string passada subistituindo pela
	 * string replace passada.
	 * 
	 * Caracteres: {@code
	 *     - % $ # @ ! & * {} () [] ? = + § £ ¢ ¬ _ | \ / , . ; : ' " ~ ^ ` 
	 *  }
	 */
	public static String removeChars(String str, String replace) {
		String removed = "";
		if(Validator.isValid(str)){
			removed = str;
			char[] chars = {
					'-', '%', '$', '#', '@', '!', '&', '*', 
					'{', '}', '(', ')', '[', ']', '?', '=', 
					'+', '_', '|', '/', ',', '\\', '<', '>',
					'.', ';', ':', '\'', '"', '~', '^', '`',
					'\u00A2', '\u00AC', '\u00B0', '\u00BA', 
					'\u00AA', '\u00B9', '\u00B2', '\u00B3',
					'\u00A7', '\u00A3',
			};
			for (char c : chars) {
				removed = removed.replaceAll("\\" + c, replace);	
			}
		}
		return removed;
	}
	
	/**
	 * Remove os acentos da string passada.
	 */
	public static String removeAccents(String s) {
		return RemoveAccents.remove(s);
	}
	
	private static class RemoveAccents {
		// Codificado com: http://people.w3.org/rishida/tools/conversion/
	    static String accented 
	    		= "\u00E7\u00C7\u00E1\u00E9\u00ED\u00F3\u00FA\u00FD\u00C1"
    			+ "\u00C9\u00CD\u00D3\u00DA\u00DD\u00E0\u00E8\u00EC\u00F2"
				+ "\u00F9\u00C0\u00C8\u00CC\u00D2\u00D9\u00E3\u00F5\u00F1"
				+ "\u00E4\u00EB\u00EF\u00F6\u00FC\u00FF\u00C4\u00CB\u00CF"
				+ "\u00D6\u00DC\u00C3\u00D5\u00D1\u00E2\u00EA\u00EE\u00F4"
				+ "\u00FB\u00C2\u00CA\u00CE\u00D4\u00DB";  
	    static String notAccented 
	    		= "cCaeiouyAEIOUYaeiouAEIOUaonaeiouyAEIOUAONaeiouAEIOU";
	    
	    static char[] table;  
	    static { 
	        table = new char[256];  
	        for (int i = 0; i < table.length; ++i) {  
	        	table [i] = (char) i;  
	        }  
	        for (int i = 0; i < accented.length(); ++i) {  
	            table [accented.charAt(i)] = notAccented.charAt(i);  
	        }  
	    }  
	    public static String remove(String s) {
	        StringBuffer sb = new StringBuffer();
	        if(Validator.isValid(s)){
		        for (int i = 0; i < s.length(); ++i) {  
		            char ch = s.charAt (i);  
		            if (ch < 256) {  
		                sb.append (table [ch]);  
		            } else {  
		                sb.append (ch);  
		            }  
		        } 
	        }
	        return sb.toString();
	    	
	    }  
	} 
	
	/**
	 * Gera um codigo de erro para ser utilizado nas mensagens de falha 
	 * do sistema. O codigo e baseado no tempo corrente e gerado
	 * sincronizado com a thread corrente.
	 *   
	 * @return Um codigo de erro unico gerado. 
	 */
	public static long getErrorCode() {
		return getCode();
	}
	
	/**
	 * Envia por email uma notificacao do erro ocorrido.
	 * O disparo do e-mail e feito em uma nova thread.
	 * 
	 * @param code Codigo do erro no log do sistema.
	 * @param message Mensagem referente ao causa/local do erro.
	 * @param error O erro capturado.
	 */
	public static void sendError(final long code, final String message, 
			final Throwable error) {
		// Nao envia o email cado estiver desativado
		String active = Configurator.getProperty("logger.util.sendError");
		active = Validator.isValid(active) ? active : "true";  
		if (!Boolean.valueOf(active)) {
			return;
		}
		
		Thread thread = new Thread() {
			@Override
			public void run() {
				try {
					Email email = new Email();
					// Cria a mensagem
					email.setSubject(Convert.toString(
							"[ATENCAO] Ocorreu um erro no sistema: ", 
							Configurator.getProperty("system.name")
					));
					
					String machine;
					try {
					   NetworkInterface ni = NetworkInterface.getByName("eth0");
					   machine = ni.toString().replaceAll("\\n", " ");
					} catch (Exception e) {
					   machine = "DESCONHECIDA";
					}
					
					email.setMessage(Convert.toString(
							"<h2>ATENCAO: Ocorreu um erro no sistema!</h2>",
							"* Maquina: ", machine, "<br>",
							"* Produto: ", 
							Configurator.getProperty("system.name"), "<br>",
							"* Versao: ",
							Configurator.getProperty("system.version"), "<br>",
							"* Mensagem: ", message, "<br>",
							"* Codigo de erro: ", code, "<br>",
							"* Erro: ", error.getClass().getSimpleName(), 
							" -- ", error.getMessage(), 
							(
								error.getCause() != null ? Convert.toString(
								" -- | -- Caused by: ", 
								error.getCause().getClass().getSimpleName(),
								" -- ", error.getCause().getMessage()) : ""
					    	),
							"<br>",
							"* Stacktrace: em anexo"
					));

					// Configura os envolvidos a receber a notificao.
					String to = Configurator.getProperty("system.email"); 
					email.setTo(Arrays.asList(to.split("\\s*[,;]\\s*")));

					// Cria o arquivo com o stacktrace do erro e anexa ao email
					File fileError = File.createTempFile(Convert.toString(
							"errocode_", code, "---"), ".txt");
					PrintWriter print = new PrintWriter(fileError);
					error.printStackTrace(print);
					print.close();
					email.addAttachment(fileError);
					
					// Envia o email para os destinatarios configurados.
					EmailSender.send(email);
				
				} catch (Exception e) {
					Logger.error(Convert.toString("[", code, "] [Util] ",
							"Erro ao notificar email de excecao: ",
							message, " - ", error.getClass().getSimpleName(), 
							": ", error.getMessage()
					), e);
				}				
			}
		};
		thread.setPriority(Thread.NORM_PRIORITY);
		thread.start();
	}
	
	/**
	 * Envia emails usando uma configuracao especifica de send.
	 * 
	 * @param config A configuracao do sender do email 
	 * @param emails Um ou mais emails a serem enviados
	 */
	public static void sendEmail(Properties config, String subject, 
			String message, String... to) {
		Email email = new Email(to);
		email.setSubject(subject);
		email.setMessage(message);
		sendEmail(config, email);
	}
	
	/**
	 * Envia emails usando uma configuracao especifica de send.
	 * 
	 * @param config A configuracao do sender do email 
	 * @param emails Um ou mais emails a serem enviados
	 */
	public static void sendEmail(Properties config, Email... emails) {
		if (!Validator.isEmpty(emails)) {
			EmailSender sender;
			if (config != null && !config.isEmpty()) {
				sender = new EmailSender(config);
			} else {
				sender = new EmailSender();
			}
			for (Email email : emails) {
				sender.add(email);
			}
			sender.send();
		}
	}
	
	/**
	 * Gera um codigo unico no sistema. O codigo e baseado no tempo 
	 * corrente e gerado sincronizado com a thread corrente.
	 * 
	 * @param size Tamanho maximo do codigo gerado.
	 * @return Um codigo unico no sistema.
	 */
	public static long getCode(int size) {
		String code = Convert.toString(getCode());
		
		if (size > code.length()) {
			throw new ValidationException(Convert.toString(
					"Tamanho maximo para o codigo e de [", 
					code.length(), "] digitos"));
		}
		if (size <= 0) {
			throw new ValidationException(
					"Tamanho minimo para o codigo e de [1] digitos");
		}
		return Convert.toLong(code.substring(code.length()-size,code.length()));
	}
	
	/**
	 * Gera um codigo unico no sistema. O codigo e baseado no tempo 
	 * corrente e gerado sincronizado com a thread corrente.
	 * 
	 * @return Um codigo unico no sistema.
	 */
	public synchronized static long getCode() {
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			Logger.error("[Util][getCode] erro ao gerar codigos.", e);
		}
		return System.currentTimeMillis();
	}
	
	/**
	 * Gera um codigo unico no sistema. O codigo e baseado no tempo 
	 * corrente e gerado sincronizado com a thread corrente.
	 * 
	 * @return Um codigo unico no sistema.
	 */
	public synchronized static String getCodeAsString() {
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			Logger.error("[Util][getCode] erro ao gerar codigos.", e);
		}
		return Convert.toString(System.currentTimeMillis());
	}
	
	/**
	 * Gera um cpf valido com numeros imaginarios.
	 * 
	 * @return Retorna um long com o cpf.
	 */
	public static long generateCPF() {
		return CPFGenerator.generates();
	}
	
	/**
	 * Gera um cpf valido com numeros imaginarios do tipo {@link String}
	 * 
	 * @return Retorna uma String com o cpf.
	 */
	public static String generateCPFAsString() {
		return Convert.toString(CPFGenerator.generates());
	}
	
	/**
	 * Gera um cpf com mascara valido com numeros imaginarios.
	 * 
	 * @return Retorna uma {@link String} com o cpf e sua mascara, exemplo: 
	 * 		   666.666.666-66
	 */
	public static String generateCPFWithMask() {
		return Format.toCPF(CPFGenerator.generates());
	}
	
	/**
	 * Remove caracteres especiais da string passada, tais como:
	 * <code>
	 *    .   ,   -   /   :   |  () [] {} _
	 * </code>
	 * @param s A string a ser desmascarada.
	 * @return A string desmascarada ou vazio caso nulo.
	 */
	public static String unmask(String s) {
		String umasked = "";
		if (Validator.isValid(s)) {
			umasked = s.replaceAll("\\s|\\W|_", "");
		}
		return umasked;
	}
	
	/**
	 * Preenche um numero com zeros a esquerda ou a direita, indicando
	 * o tamanho maximo de inclusoes.
	 * 
	 * @param number O numero a ser preenchido com zeros. 
	 * @param size O tamanho maximo que o numero deve alcancar.
	 * @param inLeft Verdadeiro para inclusao a esquerda, falso a direita.
	 * @return A String do numero com zeros preenchidos.
	 */
	public static String fillZeros(Number number, int size, boolean inLeft) {
		return fillChar(Convert.toString(number), '0', size, inLeft);
	}
	
	/**
	 * Preenche uma palavra com espaco em branco, a esquerda ou a direita, 
	 * indicando o tamanho maximo de inclusoes.
	 * 
	 * @param word A palavra a ser preenchida com os espacos.
	 * @param size O tamanho maximo que a palavra deve alcancar.
	 * @param inLeft Verdadeiro para inclusao a esquerda, falso a direita.
	 * @return A String da palavra com os espacos em branco preenchidos.
	 */
	public static String fillSpace(String word, int size, boolean inLeft) {
		return fillChar(word, ' ', size, inLeft);
	}
	
	/**
	 * Preenche uma palavra com o caracter passado, a esquerda ou a direita, 
	 * indicando o tamanho maximo de inclusoes.
	 * 
	 * @param word A palavra a ser preenchida com caracteres.
	 * @param character O caracter a preencher.
	 * @param size O tamanho maximo que a palavra deve alcancar.
	 * @param inLeft Verdadeiro para inclusao a esquerda, falso a direita.
	 * @return A String da palavra com os caracteres preenchidos.
	 */
	public static String fillChar(String word, char character, 
			int size, boolean inLeft) {
		StringBuilder sb = new StringBuilder();
		int max = Math.abs(size) - Convert.toString(word).length();
		
		if (!inLeft) {
			sb.append(word);
		}
		for (int i = 0; i < max; ++i) {
			sb.append(character);
		}
		if (inLeft) {
			sb.append(word);
		}
		
		String result = sb.toString();
		if (result.length() > size) {
			if (inLeft) {
				result = result.substring(Math.abs(max));			
			} else {
				result = result.substring(0, size);
			}
		}
		
		return result;
	}

	/**
	 * Preenche com zeros a esquerda ate que o numero 
	 * do CPF alcance seus 11 digitos.  
	 * 
	 * @param cpf O numero do CPF.
	 * @return O numero com zeros a esquerda.
	 */
	public static String toCPF(long cpf) {
		return fillZeros(cpf, 11, true);
	}
	
	/**
	 * Preenche com zeros a esquerda ate que o numero 
	 * do CNPJ alcance seus 14 digitos.  
	 * 
	 * @param cnpj O numero do CNPJ.
	 * @return O numero com zeros a esquerda.
	 */
	public static String toCNPJ(long cnpj) {
		return fillZeros(cnpj, 14, true);
	}
	
	
	/**
	 * Aplica a decodificacao Base64 na senha e descifra a string resultante 
	 * com o algoritmo AES.
	 * 
	 * @param password A senha a se decodificada.
	 * @return A senha decodificada.
	 */
	public static String decryptPassword(String password) {
		String pass = password;
		try {
			if (Validator.isValid(pass)) {
				Cipher cipher = Cipher.getInstance();
				byte[] coded = Base64Coder.decrypt(pass);
				pass = new String(cipher.decrypt(coded));
			}
		} catch (Exception e) {
			String error = "Erro ao decifrar a senha.";
			Logger.error(error, e);
			throw new ValidationException(error);
		}
		return pass;
	}
	
	/**
	 * Cifra a senha com o algoritmo AES e codifica o array de bytes resultante 
	 * usando o Base64 para torna-la string 
	 * 
	 * @param password senha a ser encriptada
	 * 
	 * @return Retorna a senha cifrada no formato {@link String}
	 */
	public static String encryptPassword(String password) {
		String pass = password;
		try {
			if (Validator.isValid(pass)) {
				Cipher cipher = Cipher.getInstance();
				byte[] coded = cipher.encrypt(pass.getBytes());
				pass = Base64Coder.encrypt(coded);
			}
		} catch (Exception e) {
			String error = "Erro ao cifrar a senha.";
			Logger.error(error, e);
			throw new ValidationException(error);
		}
		return pass;
	}
	
	/**
	 * Gerador de senhas aleatorias com tamanho determinado.
	 * Gera somente caracteres validos na codificacao 
	 * Base64 [a-zA-Z0-9+/=].
	 * 
	 * @param size O tamanho da senha a ser gerada.
	 * @return A senha gerada.
	 */
	public static String generatePassword(int size) {
		StringBuilder password = new StringBuilder();
		
		if (size > 0) {
			byte[] b = new byte[size];
			Random r = new Random();
			
			for (int i = 0; i < size; ++i) {
				b[i] = (byte) r.nextInt(127);
			}
			password.append(Base64Coder.encrypt(b));
			password.setLength(size);
		}
		return password.toString();
	}

	/**
	 * Gerador de conjunto de numeros aleatorios aleatorios. 
	 * Gera numeros entre (0) e (size -1) na quantidade definida por (total).
	 * Observacao: O tamanho do numero nao pode ser menor que a quantidade 
	 * total de numeros, ja que sao gerados numeros unicos ate preencher o total.
	 * 
	 * @param total Quantidade total de numeros.
	 * @param size Tamanho maximo do numero.
	 * @return O conjunto de titulos gerados.
	 */
	public static Set<Long> generateNumbers(int total, int size) {
		if (total > size) {
			throw new ValidationException(
					"O parametro total e maior que o parametro size");
		}
		Set<Long> numbers = new LinkedHashSet<Long>(total);
		Random random = new Random();
		
		while (numbers.size() < total) {
			numbers.add(Long.valueOf(random.nextInt(size)));
		}
		return numbers;
	}

	/**
	 * Converte uma lista de strings em uma string separada por virgula (,).
	 * 
	 * @param csv Uma lista com as partes separadas.
	 * @return A string no formato CSV. 
	 */
	public static String toCSV(List<?> csv) {
		StringBuilder sb = new StringBuilder();
		
		if (!Validator.isEmpty(csv)) {
			for (Object o : csv) {
				sb.append(o).append(",");
			}
			sb.setLength(sb.length() -1);
		}
		return sb.toString();
	}
	
	/**
	 * Converte um mapa de strings/objeto em uma string no formato json ({"":""})
	 * 
	 * @param list Uma lista com as partes separadas.
	 * @return A string no formato JSON. 
	 */
	@SuppressWarnings("unchecked")
	public static String toJSON(Map<String, ?> map) {
		String jsonString = "";
		if (!Validator.isEmpty(map)) {
			JSONObject json = new JSONObject();
			for (Map.Entry<String, ?> entry : map.entrySet()) {
				json.put(entry.getKey(), entry.getValue());
			}
			jsonString = json.toJSONString();
		}
		return jsonString;
	}
	
	/**
	 * Converte uma lista de strings em uma string no formato json (["",""])
	 * 
	 * @param list Uma lista com as partes separadas.
	 * @return A string no formato JSON. 
	 */
	public static String toJSON(List<String> list) {
		StringBuilder sb = new StringBuilder();
		
		if (!Validator.isEmpty(list)) {
			sb.append("[");
			for (String s : list) {
				sb.append("\"").append(s).append("\"").append(",");
			}
			sb.setLength(sb.length() - 1);
			sb.append("]");
			
		}
		return sb.toString();
	}
	
	/**
	 * Converte um mapa de chave valor uma string separada por 
	 * sinal de igual (=) e virgulas (,) entre os pares.
	 * 
	 * @param properties Um mapa das propriedades. 
	 * @return A string no formato properties.
	 */
	public static String toProperties(Map<?, ?> properties) {
		StringBuilder sb = new StringBuilder();
		
		if (!Validator.isEmpty(properties)) {
			for (Entry<?, ?> s : properties.entrySet()) {
				sb.append(s.getKey()).append("=");
				sb.append(s.getValue()).append(",");
			}
			sb.setLength(sb.length() -1);
		}
		return sb.toString();
	}
	
	/**
	 * Converte uma string separada por virgulas (,) ou ponto e virula(;)
	 * em uma lista de strings.
	 * 
	 * @param csv A string no formato CSV
	 * @return Uma lista com as partes separadas.
	 */
	public static List<String> parseCSV(String csv) {
		List<String> list = new ArrayList<String>();
		
		if (Validator.isValid(csv)) {
			String[] tokens = csv.split("\\s*[,;|]\\s*");
			if (!Validator.isEmpty(tokens)) {
				list.addAll(Arrays.asList(tokens));
			}
		}
		return list;
	}
	
	/**
	 * Converte um string separada por virgulas (,) ou ponto e virgula (;),
	 * contendo dados chave e valor separados por igual (=) ou dois pontos (:) 
	 * 
	 * @param properties A string no formato properties.
	 * @return Um mapa das propriedades.
	 */
	public static Map<String,String> parseProperties(String properties) {
		List<String> list = parseCSV(properties);
		Map<String,String> map = new LinkedHashMap<String, String>(list.size());
		
		if (!list.isEmpty()) {
			String[] tokens;
			for (String props : list) {
				tokens = props.split("\\s*[\\=\\:\n]\\s*");
				map.put(tokens[0], tokens[1]);
			}
		}
		return map;
	}

	/**
	 * Decodifica uma string codificada em URL.
	 * 
	 * @param str A string a ser decodificada.
	 * @param charset Charset utilizado na decodificacao.
	 * @return A string a decodificada ou a mesma se houver algum erro.
	 */
	public static String URLDecoder(String str, String charset) {
		try {
			return java.net.URLDecoder.decode(str, charset);
		} catch (Exception e) {
			return str;
		}
	}
	
	/**
	 * Decodifica uma string codificada em URL.
	 * 
	 * @param str A string a ser decodificada.
	 * @return A string a decodificada ou a mesma se houver algum erro.
	 */
	public static String URLDecoder(String str) {
		try {
			return java.net.URLDecoder.decode(str, "UTF-8");
		} catch (Exception e) {
			return str;
		}
	}
	
	/**
	 * Codifica uma string em URL.
	 * 
	 * @param str A string a ser codificada.
	 * @return A string codificada ou a mesma se houver algum erro.
	 */
	public static String URLEncoder(String str) {
		try {
			return java.net.URLEncoder.encode(str, "UTF-8");
		} catch (Exception e) {
			return str;
		}
	}

	/**
	 * Faz uma conexao HTTP/GET e obtem os seu retorno em UTF-8.
	 * 
	 * @param url A URL de destino
	 * @return A mensagem obtida, caso nao existirem, retorna null.
	 */
	public static String HTTPConnection(String url) {
		return HTTPConnection(url, null, "UTF-8");
	}
	
	/**
	 * Faz uma conexao HTTP/POST e obtem os seu retorno em UTF-8.
	 * 
	 * @param url A URL de destino
	 * @param params Os parametros, caso nao existirem, usar null.
	 * @return A mensagem obtida, caso nao existirem, retorna null.
	 */
	public static String HTTPConnection(String url, Map<?, ?> params) {
		return HTTPConnection(url, params, "UTF-8");
	}
	
	/**
	 * Faz uma conexao HTTP/GET e obtem os seu retorno no charset passado.
	 * 
	 * @param url A URL de destino
	 * @param charset Charset utilizado pelos parametros e retorno
	 * @return A mensagem obtida, caso nao existirem, retorna null.
	 */
	public static String HTTPConnection(String url, String charset) {
		return HTTPConnection(url, null, charset);
	}
	
	/**
	 * Faz uma conexao HTTP (GET ou POST) e obtem os seu retorno no
	 * charset passado.
	 * 
	 * @param url A URL de destino
	 * @param params Os parametros, caso nao existirem, usar null.
	 * @param charset Charset utilizado pelos parametros e retorno
	 * @return A mensagem obtida, caso nao existirem, retorna null.
	 */
	public static String HTTPConnection(String url, Map<?, ?> params, 
			String charset) {
		String response = null; 
		try { 
			// Montagem da conexao HTTP
			URL httpURL = new URL(url);
			URLConnection urlConn = httpURL.openConnection();
			urlConn.setDoOutput(true);
			urlConn.setDoInput(true);
			
			// Escrita do parametro
			if (params != null) {
				urlConn.setRequestProperty(
						"Content-Type", "application/x-www-form-urlencoded");
				PrintWriter pw = new PrintWriter(urlConn.getOutputStream());
				pw.print(urlEncode(params, charset));
				pw.close();
			}
		
			// Leitura do retorno
			byte[] bytes = Convert.toBytes(urlConn.getInputStream());
			if (bytes != null) {
				response = new String(bytes, charset);
			}
			
		} catch (Exception e) {
			Logger.error(e, "[Util][HTTPConnection] Erro ao executar.");
		}
		return response;
	}

	/**
	 * Codifica o post do HTTPConnect 
	 */
	private static String urlEncode(Map<?, ?> params, String encode) 
			throws Exception {
		StringBuilder sb = new StringBuilder();
		
		if (!Validator.isEmpty(params)) {
			for (Map.Entry<?, ?> param : params.entrySet()) {
				sb.append(param.getKey());
				sb.append("=");
				sb.append(URLEncoder.encode(Convert
						.toString(param.getValue()), encode));
				sb.append("&");
			}
			sb.setLength(sb.length() -1);
		}
		return sb.toString();
	}

	/**
	 * Mescla o objeto passado ao corrente, atualizando somente os 
	 * dados nao existentes no objeto atual.
	 */
	public static <T> T merge(T one, T another, T empty) {
		try {
			Class<?> c = one.getClass();
			Field t;
			for (Field f : c.getDeclaredFields()) {
				f.setAccessible(true);
				t = c.getDeclaredField(f.getName());
				t.setAccessible(true);
				
				if (f.get(one).equals(t.get(empty))) {
					if (!Modifier.isStatic(f.getModifiers()) 
							&& !Modifier.isFinal(f.getModifiers())) {
						f.set(one, t.get(another)); 
					}
				}
			}
			return one;
			
		} catch (Exception e) { 
			throw new ValidationException(Convert.toString(
					"Impossivel mesclar instancias, ",
					"causa: invalida ou inacessivel"), e); 
		}
	}
	
	/**
	 * Obtem o fuso horario da configuracao do sistema
	 */
	public static TimeZone getSystemTimeZone() {
		TimeZone tz = null;
		try {
			String system = Configurator.getProperty("system.timezone");
			tz = TimeZone.getTimeZone(system);
			
		} catch (Exception e) {
			tz = TimeZone.getTimeZone("America/Sao_Paulo");
		}
		return tz;
	}
	
	/**
	 * Obtem a localidade da configuracao do sistema 
	 */
	public static Locale getSystemLocale() {
		Locale locale = null;
		try {
			List<String> conf = Util.parseCSV(Configurator
					.getProperty("system.locale"));
			
			switch (conf.size()) {
			case 1:
				locale = new Locale(conf.get(0));
				break;
			case 2:
				locale = new Locale(conf.get(0), conf.get(1));
				break;
			case 3:
				locale = new Locale(conf.get(0), conf.get(1), conf.get(2));
				break;
			default: 
				throw new Exception("Localidade nao configurada");
			} 
		} catch (Exception e) {
			locale = new Locale("pt", "BR");
		}
		return locale;
	}
	
	/**
	 * Retorna a string passada capitalizada, ou seja, com a primeira letra
	 * da string em maiuscula. 
	 */
	public static String capitalize(String str) {
		if (str != null) {
			StringBuilder sb = new StringBuilder(str.trim().toLowerCase());
			sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
			return sb.toString();
		}
		return "";
	}
	
	/**
	 * Retorna a string passada capitalizada, ou seja, com a primeira letra
	 * de cada palavra da string em maiuscula.
	 */
	public static String capitalizeAll(String str) {
		if (str != null) {
			StringBuilder sb = new StringBuilder();
			String[] splited = str.trim().split("\\s+");
			if (!Validator.isEmpty(splited)) {
				for (String string : splited) {
					sb.append(capitalize(string)).append(" ");
				}
				sb.setLength(sb.length() -1);
			}
			return sb.toString();
		} 
		return "";
	}

	/**
	 * Um hash simples para codificar strings em um inteiro de 32bits
	 */
	public static String hash(String string) {
		int hash = 1;
		int prime = 31;
		char[] array = string.toCharArray();
		for (int i = 0; i < array.length; i++) {
			hash = prime * hash + array[i];
		}
		// Base 16
		return Integer.toHexString(prime + hash).toUpperCase();
	}

	/**
	 * Retorna a string passada com a metade de seus caracteres e na 
	 * posicao final desta ocultos pelo simbolo padrao '*'.
	 */
	public static String hide(String text) {
		if (Validator.isValid(text)) {
			return hide(text, text.length() / 2);			
		}
		return "";
	}
	
	/**
	 * Retorna a string passada com a quantidade de caracteres e na 
	 * posicao final desta ocultos pelo simbolo padrao '*'.
	 */
	public static String hide(String text, int quantity) {
		return hide(text, quantity, 1);
	}
	
	/**
	 * Retorna a string passada com a quantidade de caracteres e posicao nesta 
	 * ocultos pelo simbolo padrao '*'.
	 */
	public static String hide(String text, int quantity, int position) {
		return hide(text, quantity, position, '*');
	}
	
	/**
	 * Retorna a string passada com a quantidade de caracteres e posicao nesta 
	 * ocultos pelo simbolo informado.
	 */
	public static String hide(Object text, int quantity, int position, 
			char symbol) {
		StringBuilder sb = new StringBuilder();
		
		if (Validator.isValid(text)) {
			sb.append(text);
			int quant = Math.abs(quantity);
			quant = (quant > sb.length()) ? sb.length() : quant;
			String symb = fillChar("", symbol, quant, true);
			
			if (position < 0) {
				sb.replace(0, quant, symb);	
			} else if (position == 0) {
				int start = (sb.length() - quant) / 2;
				int end = start + quant;
				sb.replace(start, end, symb);				
			} else { // if (possition > 0) {
				sb.replace((sb.length() - quant), sb.length(), symb);
			}
		}
		return sb.toString();
	}
	
	private static String requestDomain(HttpServletRequest request, 
			boolean useProtocol) {
		StringBuilder domain = new StringBuilder();
		
		int port = request.getServerPort();
		if (useProtocol) {
			if (port == 443) {
				domain.append("https://");
			} else {
				domain.append("http://");
			}
		}
		
		domain.append(request.getServerName());
		
		if (!(port == 80 || port == 443)) {
			domain.append(':').append(port);
		}
		
		domain.append(request.getContextPath());
		if (request.getContextPath().length() > 1) {
			domain.append('/');
		}
		
		return domain.toString();
	}
	
	/**
	 * Retorna o dominio completo da requisicao no formato: 
	 * {@link Code http://dominio:porta/aplicacao}
	 */
	public static String getFullDomain(HttpServletRequest request) {
		return requestDomain(request, true);
	}
	
	/**
	 * Retorna o dominio completo da requisicao no formato: 
	 * {@link Code dominio:porta/aplicacao}
	 */
	public static String getDomain(HttpServletRequest request) {
		return requestDomain(request, false);
	}

	/**
	 * Metodo que retorna a latitude,longitude de uma localizacao recebendo 
	 * um cep de entrada através da api do GoogleMaps.
	 * 
	 * @param cep CEP a ser consultado
	 * 
	 * @return {@link Pair} onde first: latitude, last: longitude. 
	 * Caso nao encontre a localizacao, uma valor default será retornado.
	 */
	public static Pair<String, String> getGeoLocation(String cep) {
		if (Validator.isCEP(cep)) {
			
			JSONObject queryObj = getAddress(cep);
			
			StringBuilder query = new StringBuilder();
			
			String street = URLEncoder(Convert.toString(
					queryObj.get("tipo_logradouro")," ",
					queryObj.get("logradouro")));
			
			String city = URLEncoder(Convert.toString(
					queryObj.get("cidade"), "-"));
			String state = URLEncoder(Convert.toString(
					queryObj.get("uf"),", "));
			
			query.append(street);
			query.append(city);
			query.append(state);
			query.append(cep);
			
			String url = Convert.toString("http://maps.google.com/maps/",
					"api/geocode/json?address=" ,
					query.toString(),"&sensor=false");
			
			try {			
				String json = HTTPConnection(url);

				JSONObject object = (JSONObject) JSONValue.parse(json);
				if("OK".equals(object.get("status"))){
					JSONArray results = (JSONArray) object.get("results");
					JSONObject address = (JSONObject) results.get(0);
					JSONObject geometry = (JSONObject) address.get("geometry");
					JSONObject location = (JSONObject) geometry.get("location");
					Pair<String, String> pair = new Pair<String, String>();
					pair.setFirst(Convert.toString(location.get("lat")));
					pair.setLast(Convert.toString(location.get("lng")));
					return pair;
				}
			} catch (Exception e) {
				// Nada faz...
			}
		}
		return new Pair<String, String>();
	}
	
	/**
	 * Metodo que retorna a latitude,longitude de uma localizacao recebendo 
	 * um cep de entrada através da api do GoogleMaps.
	 * 
	 * @param street Rua do endereco
	 * @param city Cidade do endereco
	 * @param state Estado do endereco
	 * 
	 * @return {@link Pair} onde first: latitude, last: longitude. 
	 * Caso nao encontre a localizacao, uma valor default sera retornado.
	 * 
	 * Consegui atrav�s do Google Maps. Criei em minha aplica��o um programa que passo os dados de endere�o do cliente, chamo a URL do google maps e depois eu trato o retorno.

http://code.google.com/intl/pt-BR/apis/maps/documentation/javascript/v2/services.html

 

Como geocodificar via HTTP

 

Voc� tamb�m pode acessar diretamente o geocodificador da API do Google Maps usando o script do servidor. Esse m�todo n�o � recomendado em lugar de usar geocodificador do cliente, no entanto, � �til para prop�sitos de depura��o ou em casos em que um objeto GClientGeocoder JavaScript n�o est� dispon�vel.

Para acessar o Geocodificador da API do Google Maps, envie uma solicita��o para http://maps.google.com/maps/geo? com os seguintes par�metros no URI:

q (obrigat�rio) � O endere�o que deseja geocodificar.
key (obrigat�rio) � Sua chave de API.
sensor (obrigat�rio) � Indica se a solicita��o de geocodifica��o vem de um dispositivo com sensor de localiza��o. Esse valor deve ser true ou false.
output (obrigat�rio) � O formato em que o resultado deve ser gerado. As op��es s�o xml, kml, csv ou json (padr�o).
ll (opcional) � A {latitude,longitude} do centro da janela de visualiza��o expressa como uma string separada por v�rgulas (por exemplo, "ll=40.479581,-117.773438" ). Esse par�metro tem sentido apenas se o par�metro spn tamb�m for passado ao geocodificador.
spn (opcional) � O "span" (intervalo) da janela de visualiza��o expresso como uma string separada por v�rgulas de {latitude,longitude} (por exemplo, "spn=11.1873,22.5"). Esse par�metro tem sentido apenas se o par�metro ll tamb�m for passado ao geocodificador.
gl (opcional) � O c�digo do pa�s, especificado como um valor ccTLD ("dom�nio de n�vel superior") de dois caracteres.
Observa��o: Os par�metros da janela de visualiza��o gl e spn,ll apenas influenciar�o os resultados do geocodificador, sem restringi-los totalmente.

Nesse exemplo, solicitamos as coordenadas geogr�ficas da sede do Google:

http://maps.google.com/maps/geo?q=1600+Amphitheatre+Parkway,+Mountain+View,+CA&output=xml&sensor=true_or_false&key=abcdefg

	 */
	public static Pair<String, String> getGeoLocation(String street,
			String city, String state) {
		
		if (Validator.isValid(street, city, state)) {
			StringBuilder query = new StringBuilder();
			
			query.append(URLEncoder(street)).append("+");
			query.append(URLEncoder(city)).append("-");
			query.append(URLEncoder(state));
			
			String apiKey = Configurator.get("google.maps.apikey");
			
			String url = Convert.toString("http://maps.google.com/maps/geo?q=",
					query.toString(),"&sensor=false&key=", apiKey);
			
			try {			
				String json = HTTPConnection(url);

				JSONObject result = (JSONObject) JSONValue.parse(json);
				JSONObject status = (JSONObject) result.get("Status");
				long statusCode = (Long) status.get("code");
				
				if (statusCode == 200) {
					JSONArray placemark = (JSONArray) result.get("Placemark");
					JSONObject all = (JSONObject) placemark.get(0);
					JSONObject point = (JSONObject) all.get("Point");
					JSONArray coordinates = (JSONArray) point.get("coordinates");
					
					Pair<String, String> pair = new Pair<String, String>();
					pair.setFirst(Convert.toString(coordinates.get(1)));
					pair.setLast(Convert.toString(coordinates.get(0)));
					return pair;
				}
			} catch (Exception e) {
				// Nada faz...
			}
		}
		return new Pair<String, String>();
	}
	
	/**
	 * Consulta o endereco pelo cep usando a API do brunobarreto.<br>
	 * Exemplo, CEP 25651000, retorna um JSON no formato:<br> 
	 * <pre>
	 *	{
	 *		"tipo_logradouro":"Rua",
	 *		"logradouro":"Afrânio Melo Franco",
	 *		"bairro":"Quitandinha",
	 *		"cidade":"Petrópolis",
	 *		"uf":"RJ",
	 *		"erro":""
	 * 	}
	 * </pre> 
	 * OBS: O CEP pode estar nos formatos: 00.000-000, 00000-000, 00000000.
	 */
	public static JSONObject getAddress(String cep) {
		try {
			String url = "http://api.brunobarreto.net/cep/consulta.json?cep=";
			url = Convert.toString(url, cep);
			JSONObject res = (JSONObject) JSONValue.parse(HTTPConnection(url)); 
			return (JSONObject) res.get("resultado");
		} catch (Exception e) {
			String err = Convert.toString(
					"{\"bairro\":\"\",\"cidade\":\"\",\"uf\":\"\",",
					"\"logradouro\":\"\",\"tipo_logradouro\":\"\",",
					"\"erro\":\"Erro na consulta: ", e.getMessage(), "\"}");
			return (JSONObject) JSONValue.parse(err);
		}
	}
	
	/**
	 * Consulta o endereco pelo IP usando a API da ipinfodb.<br>
	 * Exemplo, IP 146.134.200.38, retorna um JSON no formato:<br>
	 * <pre>
	 * 	{
  	 *		"Status" : "OK",
  	 *		"CountryCode" : "BR",
  	 *		"CountryName" : "Brazil",
  	 *		"RegionCode" : "21",
  	 *		"RegionName" : "Rio de Janeiro",
  	 *		"City" : "Petrópolis",
  	 *		"ZipPostalCode" : "",
  	 *		"Latitude" : "-22.5108",
  	 *		"Longitude" : "-43.1844",
  	 *		"Gmtoffset" : "0",
  	 *		"Dstoffset" : "0",
  	 *		"TimezoneName" : "",
  	 *		"Isdst" : "",
  	 *		"Ip" : "146.134.200.38"
     *	}
	 * </pre>
	 * OBS: O IP pode estar nos formatos: 000.000.000.000
	 */
	public static JSONObject getIPLocation(String ip) {
		JSONObject json = new JSONObject();
		if (Validator.isIP(ip)) {
			try {
				String url = Convert.toString(
					"http://api.ipinfodb.com/v2/ip_query.php",
					"?output=json&timezone=false",
					"&key=5547757090bee0f3e6f611a9b3fcb2d0c7",
					"e6231e48d2e8f1c4e8bc3886e21f0a",
					"&ip=", ip);
				json = (JSONObject) JSONValue.parse(Util.HTTPConnection(url));
			} catch (Exception e) {
				String err = Convert.toString(
						"{\"Status\":\"", e.getMessage(), "\"}"
				);
				return (JSONObject) JSONValue.parse(err);
			}
		}
		return json;
	}

	/**
	 * Localiza o CEP pelo endereco usando a API do GoogleMaps.<br>
	 */
	public static String getCEP(String street, String city, String state) {
		return getCEP(Convert.toString(street, ",", city, ",", state));
	}

	/**
	 * Localiza o CEP pelo endereco usando a API do GoogleMaps.<br>
	 * Formato: {@code nome_da_rua,nome_da_cidade,nome_ou_sigla_do_estado}
	 */
	public static String getCEP(String address) {
		try {
			if (!Validator.isValid(address)) {
				return "";
			}
			String url = Convert.toString(
					"http://maps.google.com/maps/api/geocode/json?address=",
					URLEncoder(address), "&sensor=false");
		
				String json = HTTPConnection(url);
	
				JSONObject object = (JSONObject) JSONValue.parse(json);
				if ("OK".equals(object.get("status"))) {
					JSONArray results = (JSONArray) object.get("results");
					JSONObject addr = (JSONObject) results.get(0);
					JSONArray addrcomps = (JSONArray)
							addr.get("address_components");
					
					for (Object comp : addrcomps) {
						JSONObject jc = (JSONObject) comp;
						JSONArray types = (JSONArray) jc.get("types");
						
						if (types.contains("postal_code")) {
							return (String) jc.get("long_name");
						}
					}
				}
		} catch (Exception e) {
			// Nada faz...
		}
		return "";
	}

	/**
	 * Codifica os caracteres especiais do texto passado para o escape do html. 
	 */
	public static String HTMLEncoder(String html) {
		if (html != null) {
			return HtmlUtils.htmlEscape(html);
		} 
		return html;
	}
	
	/**
	 * Decodifica os caracteres especiais do texto passado em escape do html. 
	 */
	public static String HTMLDecoder(String html) {
		if (html != null) {
			return HtmlUtils.htmlUnescape(html);
		} 
		return html;
	}
	
	/**
	 * Clonador de qualquer objeto que implemente a interface {@link Serializable}
	 * 
	 * @param object Objeto a ser clonado
	 * @return Retorna um objeto clone
	 */
	@SuppressWarnings("unchecked")
	public static <T> T cloner(T object) {
		ObjectOutputStream oos = null;
		ObjectInputStream ois = null;
		T clone = null;
		try {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.flush();
			ByteArrayInputStream bin = new ByteArrayInputStream(bos.toByteArray());
			ois = new ObjectInputStream(bin);
			clone = (T) ois.readObject();
			oos.close();
			ois.close();
		} catch (Exception e) {
			Logger.error("[Util cloner] Error: ", e);
		}
		
		return clone;
	}
	
}
