package com.im.imjutil.validation;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import com.im.imjutil.config.Configurator;
import com.im.imjutil.util.Util;

/**
 * Classe que formata em <code>String</code> dados em seus tipos padroes.
 *   
 * @author Felipe Zappala
 */
public final class Format {
	
	private static final NumberFormat currencyFormatter;
	private static final SimpleDateFormat dateFormatter;
	private static final String PATTERN_TIMESTAMP;
	private static final String PATTERN_DATE;
	private static final String PATTERN_TIME;
	static {
		PATTERN_TIMESTAMP = "dd/MM/yyyy HH:mm:ss";
		PATTERN_DATE = "dd/MM/yyyy";
		PATTERN_TIME = "HH:mm:ss";
		
		Locale locale = getSystemLocale();
		TimeZone zone = getSystemTimeZone();
		currencyFormatter = NumberFormat.getCurrencyInstance(locale);
		dateFormatter = new SimpleDateFormat(PATTERN_TIMESTAMP, locale);
		dateFormatter.setCalendar(Calendar.getInstance(zone, locale));
	}
	
	/** Proibida a instanciacao*/
	private Format() {
		super();
	}

	/**
	 * Obtem o fuso horario da configuracao do sistema
	 */
	private static TimeZone getSystemTimeZone() {
		TimeZone tz = null;
		try {
			String userTZ = Configurator.getProperty("system.timezone");
			tz = TimeZone.getTimeZone(userTZ);
			
		} catch (Exception e) {
			tz = TimeZone.getTimeZone("America/Sao_Paulo");
		}
		return tz;
	}

	/**
	 * Obtem a localidade da configuracao do sistema 
	 */
	private static Locale getSystemLocale() {
		Locale locale = null;
		try {
			List<String> conf = Util.parseCSV(Configurator
					.getProperty("system.locale"));
			
			switch (conf.size()) {
			case 1:
				locale = new Locale(conf.get(0));
				break;
			case 2:
				locale = new Locale(conf.get(0), conf.get(1));
				break;
			case 3:
				locale = new Locale(conf.get(0), conf.get(1), conf.get(2));
				break;
			default: 
				throw new Exception("Localidade nao configurada");
			} 
		} catch (Exception e) {
			locale = new Locale("pt", "BR");
		}
		return locale;
	}
	
	/**
	 * Formata um CPF em: <code>###.###.###-##</code>
	 * 
	 * @param cpf O numero do CPF a ser formatado.
	 * @return O texto formatado.
	 */
	public static String toCPF(long cpf) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(Util.fillZeros(cpf, 11, true));
		prepare(sb, 11);		
		sb.insert(3, '.');
		sb.insert(7, '.');
		sb.insert(11, '-');
		
		return sb.toString();
	}
	
	/**
	 * Formata um CEP em: <code>##.###-###</code>
	 * 
	 * @param cep O numero do CEP a ser formatado.
	 * @return O texto formatado.
	 */
	public static String toCEP(int cep) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(Util.fillZeros(cep, 8, true));
		prepare(sb, 8);
		sb.insert(2, '.');
		sb.insert(6, '-');
		
		return sb.toString();
	}
	
	/**
	 * Formata um CNPJ em: <code>##.###.###/####-##</code>
	 * 
	 * @param cnpj O numero do CNPJ a ser formatado.
	 * @return O texto formatado.
	 */
	public static String toCNPJ(long cnpj) {
		StringBuilder sb = new StringBuilder();

		sb.append(Util.fillZeros(cnpj, 14, true));
		prepare(sb, 14);
		sb.insert(2, '.');
		sb.insert(6, '.');
		sb.insert(10, '/');
		sb.insert(15, '-');
		
		return sb.toString();
	}
	
	/**
	 * Prepara o <code>StringBuilder</code> para ter um comprimento fixo.
	 * Preenche com zero ('0') caso for menor que o comprimento esperado.
	 * 
	 * @param sb O <code>StringBuilder</code> a ser preparado.
	 * @param length O comprimento esperado.
	 */
	private static void prepare(StringBuilder sb, int length) {
		if (sb.length() < length) {
			sb.reverse();
			do {
				sb.append('0');
			} while(sb.length() < length);
			sb.reverse();
			
		} else if (sb.length() > length) {
			sb.setLength(length);
		}
	}
	
	/**
	 * Formata uma data/hora em: <code>dd/MM/yyyy hh:mm:ss</code>
	 * 
	 * @param timestamp A data/hora a ser formatada.
	 * @return O texto formatado.
 	 */
	public static String toTimestamp(Calendar timestamp) {
		String formatted = "";
		if (timestamp != null) {
			formatted = toTimestamp(timestamp.getTime());
		}
		return formatted;
	}
	
	/**
	 * Formata uma data/hora em: <code>dd/MM/yyyy hh:mm:ss</code>
	 * 
	 * @param timestamp A data/hora a ser formatada.
	 * @return O texto formatado.
 	 */
	public static String toTimestamp(Date timestamp) {
		String formatted = "";
		if (timestamp != null) {
			dateFormatter.applyPattern(PATTERN_TIMESTAMP);
			formatted = dateFormatter.format(timestamp);
		}
		return formatted;
	}
	
	/**
	 * Formata uma data/hora em: <code>dd/MM/yyyy hh:mm:ss</code>
	 * 
	 * @param pattern O formato da data.
	 * @param timestamp A data/hora a ser formatada.
	 * 
	 * @return Retorna uma {@link String} com o formato data/hora.
	 */
	public static String toTimestamp(String pattern, Calendar timestamp) {
		String formatted = "";
		if (!Validator.isValid(timestamp, pattern)) {
			formatted = toTimestamp(pattern, timestamp.getTime());
		}
		return formatted;
	}
	
	/**
	 * Formata uma data/hora em: <code>dd/MM/yyyy hh:mm:ss</code>
	 * 
	 * @param pattern O formato da data.
	 * @param timestamp A data/hora a ser formatada.
	 * 
	 * @return Retorna uma {@link String} com o formato data/hora.
	 */
	public static String toTimestamp(String pattern, Date timestamp) {
		String formatted = "";
		if (Validator.isValid(timestamp, pattern)) {
			dateFormatter.applyPattern(pattern);
			formatted = dateFormatter.format(timestamp);
		}
		return formatted;
	}
	
	/**
	 * Formata uma data em: <code>dd/MM/yyyy</code>
	 * 
	 * @param date A data a ser formatada.
	 * @return O texto formatado.
 	 */
	public static String toDate(Calendar date) {
		String formatted = "";
		if (date != null) {
			formatted = toDate(date.getTime());
		}
		return formatted;
	}
	
	/**
	 * Formata uma data em: <code>dd/MM/yyyy</code>
	 * 
	 * @param date A data a ser formatada.
	 * @return O texto formatado.
 	 */
	public static String toDate(Date date) {
		String formatted = "";
		if (date != null) {
			dateFormatter.applyPattern(PATTERN_DATE);
			formatted = dateFormatter.format(date);			
		}
		return formatted;
	}
	
	/**
	 * Formata uma hora em: <code>hh:mm:ss</code>
	 * 
	 * @param time A hora a ser formatada.
	 * @return O texto formatado.
 	 */
	public static String toTime(Calendar time) {
		String formatted = "";
		if (time != null) {
			formatted = toTime(time.getTime());
		}
		return formatted;
	}
	
	/**
	 * Formata uma hora em: <code>hh:mm:ss</code>
	 * 
	 * @param time A hora a ser formatada.
	 * @return O texto formatado.
 	 */
	public static String toTime(Date time) {
		String formatted = "";
		if (time != null) {
			dateFormatter.applyPattern(PATTERN_TIME);
			formatted = dateFormatter.format(time);			
		}
		return formatted;
	}
	
	/**
	 * Formata um telefone em: <code>####-####</code>
	 * 
	 * @param number O numero do telene.
	 * @return O texto formatado.
	 */
	public static String toPhone(int number) {
		return toPhone(0, number);
	}
	
	/**
	 * Formata um telefone em: <code>(##) ####-####</code>
	 * 
	 * @param code O codigo de area do telefone.
	 * @param number O numero do telene.
	 * @return O texto formatado.
	 */
	public static String toPhone(int code, int number) {
		StringBuilder sb = new StringBuilder();
		if (code > 0) {
			sb.append("(").append(code).append(") ");
		}
		sb.append(number);
		sb.reverse();
		sb.insert(4, '-');
		sb.reverse();
		
		return sb.toString();
	}
	
	/**
	 * Formata uma moeda em reais no formato: <code>R$ #.###,##</code>
	 * 
	 * @param value O valor a ser formatado.
	 * @return O texto formatado.
	 */
	public static String toCurrency(BigDecimal value) {
		String formatted = "";
		if (value != null) {
			formatted = currencyFormatter.format(value);			
		}
		return formatted;
	}
	
	/**
	 * Formata uma moeda em reais no formato: <code>R$ 0.000,00</code>
	 * 
	 * @param value O valor a ser formatado.
	 * @return O texto formatado.
	 */
	public static String toCurrency(double value) {
		return toCurrency(new BigDecimal(value));
	}
	
	private static Set<Field> getAvailableFields(Class<?> clazz) {
		Set<Field> availableFields = new HashSet<Field>();
		for (Field field : clazz.getDeclaredFields()) {
			availableFields.add(field);
		}
		
		Class<?> superClass = clazz.getSuperclass();
		while (!superClass.equals(Object.class)) {
			for (Field field : superClass.getDeclaredFields()) {
				availableFields.add(field);
			}	
			superClass = superClass.getSuperclass();
		}
		
		return availableFields;
	}
	
	/**
	 * Formata o objeto com a representacao String no formato:
	 * <code>
	 * 		ClassName{attribute1:value1, ..., attributeN:valueN}
	 * </code>
	 * 
	 * @param object O objeto a ser representado como String
	 * @return A representacao do objeto como string.
	 */
	public static String toString(Object object, String... ignoreFilds) {
		List<String> ignoreds = new ArrayList<String>();
		if (!Validator.isEmpty(ignoreFilds)) {
			ignoreds.addAll(Arrays.asList(ignoreFilds));
		}
		StringBuilder sb = new StringBuilder();
		sb.append(object.getClass().getSimpleName()).append("{");
		try {
			boolean has = false;
			for (Field f : getAvailableFields(object.getClass())) {
				f.setAccessible(true);
				if (!ignoreds.contains(f.getName())) {
					has = true;
					sb.append(f.getName());
					sb.append(":");
					sb.append(f.get(object));
					sb.append(",");
				}
			}
			if (has) {
				sb.setLength(sb.length() -1);
			}
		} catch (Exception e) { /* Do nothing! */ }
		sb.append("}");
		return sb.toString();
	}
	
	/**
	 * Formata uma colecao no padrao do CSV (valores separados por virgula) 
	 * 
	 * @param comma O separador da String.
	 * @param collection Colecao a ter seus valores separados por virgula. 
	 * @return A string resultante.
	 */
	public static String toCSV(String comma, Collection<?> collection) {
		StringBuilder sb = new StringBuilder();
		
		if (Validator.isValid(collection, comma)) {
			for (Object object : collection) {
				sb.append(Convert.toString(object)).append(comma);
			}
			sb.setLength(sb.length() - comma.length());
		}
		return sb.toString();
	}
	
	/**
	 * Formata uma colecao no padrao do CSV (valores separados por virgula) 
	 * 
	 * @param collection Colecao a ter seus valores separados por virgula. 
	 * @return A string resultante.
	 */
	public static String toCSV(Collection<?> collection) {
		return toCSV(",", collection);
	}

	/**
	 * Formata um array no padrao do CSV (valores separados por virgula) 
	 * 
	 * @param array Array a ter seus valores separados por virgula. 
	 * @return A string resultante.
	 */
	public static <T> String toCSV(T... array) {
		return toCSV(",", array);
	}
	
	/**
	 * Formata um array no padrao do CSV (valores separados por virgula) 
	 * 
	 * @param array Array a ter seus valores separados por virgula.
	 * @param comma O separador da String. 
	 * @return A string resultante.
	 */
	public static <T> String toCSV(String comma, T[] array) {
		StringBuilder sb = new StringBuilder();
		
		if (Validator.isValid(array, comma)) {
			for (Object object : array) {
				sb.append(Convert.toString(object)).append(comma);
			}
			sb.setLength(sb.length() - comma.length());
		}
		return sb.toString();
	}
   
}