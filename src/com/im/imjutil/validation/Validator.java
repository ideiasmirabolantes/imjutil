package com.im.imjutil.validation;

import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe responsavel por validacao de dados.
 * 
 * @author Felipe Zappala
 */
public final class Validator {

	//
	// Expressoes Regulares para as validacoes
	//
	private static final String REGEXP_EMAIL;
	private static final String REGEXP_NUMBER;
	private static final String REGEXP_DATE;
	private static final String REGEXP_TIME;
	private static final String REGEXP_TIMESTAMP;
	private static final String REGEXP_CPF;
	private static final String REGEXP_CEP;
	private static final String REGEXP_URL;
	private static final String REGEXP_CNPJ;
	private static final String REGEXP_PHONE;
	private static final String REGEXP_IP;
	static {
		REGEXP_EMAIL = "^([a-zA-Z0-9_\\-\\.]+)"
				+ "@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)"
				+ "|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		REGEXP_TIMESTAMP = "^\\d{2}\\/\\d{2}\\/\\d{4}"
				+ "\\s\\d{2}\\:\\d{2}\\:\\d{2}$";
		REGEXP_NUMBER = "^[-+]?\\d+([.,]?\\d+)?$";
		REGEXP_DATE = "^\\d{2}\\/\\d{2}\\/\\d{4}$";
		REGEXP_TIME = "^\\d{2}\\:\\d{2}\\:\\d{2}$";
		REGEXP_CPF = "^\\d{3}\\.?\\d{3}\\.?\\d{3}\\-?\\d{2}$";
		REGEXP_CNPJ = "^\\d{2}\\.?\\d{3}\\.?\\d{3}/?\\d{4}\\-?\\d{2}$";
		REGEXP_CEP = "^\\d{2}\\.?\\d{3}\\-?\\d{3}$";
		REGEXP_PHONE = "^\\(\\d{2}\\)\\s?\\d{4}\\-?\\d{4}$";
		REGEXP_IP = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$";
		REGEXP_URL = "^(file|ftp|ftps|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)" 
					+ "(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?$";
	}
	
	
	/** Proibir a inicializacao. */
	private Validator() {
		super();
	}
	
	/**
	 * Valida os parametros texto passados, verificando se são nulos ou vazios.
	 * 
	 * @param params Um array de parametros string.
	 * @return Retorna verdade quando os parametros nao forem nulos ou validos. 
	 */
	public static boolean isValid(String... params) {
		if (params != null && params.length > 0) {
			for (String p : params) {
				if (p == null || "".equals(p.trim())) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Valida os parametros objetos passados, verificando se são nulos ou vazios.
	 * 
	 * @param params Um array de parametros objetos.
	 * @return Retorna verdade quando os parametros nao forem nulos ou vazios. 
	 */
	public static boolean isValid(Object... params) {
		if (params != null && params.length > 0) {
			for (Object p : params) {
				if (p == null) {
					return false;
					
				} else if (p instanceof String) {
					String s = (String) p; 
					return isValid(s);
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Valida se um array qualquer esta nulo ou vazio.
	 * 
	 * @param <A> Define o tipo do array.
	 * @param array O array a ser validado.
	 * @return Retorna verdade quando o array for vazio.
	 */
	public static <A> boolean isEmpty(A[] array) {
		return (array == null || array.length == 0);
	}
	
	/**
	 * Valida se uma colecao qualquer esta nulo ou vazio.
	 * 
	 * @param <C> Define o tipo da colecao.
	 * @param collection A colecao a ser validada.
	 * @return Retorna verdade quando a colecao for vazio.
	 */
	public static <C> boolean isEmpty(Collection<C> collection) {
		return (collection == null || collection.isEmpty());
	}
	
	/**
	 * Valida se um mapa qualquer esta nulo ou vazio.
	 * 
	 * @param <K> Define o tipo da chave do mapa.
	 * @param <V> Define o tipo do valor do mapa.
	 * @param map O mapa a ser validado.
	 * @return Retorna verdade quando o mapa for vazio.
	 */
	public static <K,V> boolean isEmpty(Map<K,V> map) {
		return (map == null || map.isEmpty());
	}
	
	/**
	 * Valida a data no padrao: <code>dd/MM/yyyy</code>
	 * 
	 * @param date A data em texto.
	 * @return O objeto de data.
	 */
	public static boolean isDate(String date) {
		return validRegExp(REGEXP_DATE, date);
	}
	
	/**
	 * Valida da hora no formato: <code>dd/MM/yyyy hh:mm:ss</code> 
	 * @param time A hora em texto.
	 * @return O objeto da hora.
	 */
	public static boolean isTime(String time) {
		return validRegExp(REGEXP_TIME, time);
	}
	
	/**
	 * Valida a data e a hora no formato: <code>hh:mm:ss</code>
	 * 
	 * @param timestamp A data/hora em texto.
	 * @return Verdade se o texto passado e uma data/hora.
	 */
	public static boolean isTimestamp(String timestamp) {
		return validRegExp(REGEXP_TIMESTAMP, timestamp);
	}
	
	/**
	 * Valida se o texto passado e um numero. 
	 * Podendo ser inteiro ou flutuante, positivo ou negativo. 
	 * 
	 * @param number O numero em formato texto.
	 * @return Verdadeiro se for um numero valido.
	 */
	public static boolean isNumber(String number) {
		return validRegExp(REGEXP_NUMBER, number);
	}
	
	/**
	 * Valida um texto usando expressao regular.
	 * 
	 * @param regexp A expressao regular a ser usada.
	 * @param text O texto a ser validado.
	 * @return Verdadeiro caso o texto passe na checagem da expressao.
	 */
	public static boolean validRegExp(String regexp, String text) {
		boolean result = false;
		if (Validator.isValid(regexp, text)) {
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(text);
			result = matcher.matches();
		}
		return result;
	}
	
	/**
	 * Valida o e-mail passado usando expressao regular.
	 * Espara-se o formato: <code>aa@bb.cc</code>
	 * 
	 * @param email O e-mail a ser validado.
	 * @return Verdade caso o texto passode for um e-mail.
	 */
	public static boolean isEmail(String email) {
		return validRegExp(REGEXP_EMAIL, email);
	}

	/**
	 * Valida o CEP passado usando expressao regular.
	 * Espara-se o formato: <code>XX.YYY-ZZZ</code> ou <code>XXYYYZZZ</code> 
	 * 
	 * @param cep O CEP a ser validado.
	 * @return Verdade caso o texto passado for um CEP.
	 */
	public static boolean isCEP(String cep) {
		return validRegExp(REGEXP_CEP, cep);
	}
	
	/**
	 * Valida o telefone passado usando expressao regular.
	 * Espara-se o formato: <code>(00) 0000-0000</code> 
	 * ou <code>(00) 0000-0000</code> 
	 * 
	 * @param phone O telefone a ser validado.
	 * @return Verdade caso o texto passado for um telefone.
	 */
	public static boolean isPhone(String phone) {
		return validRegExp(REGEXP_PHONE, phone);
	}
	
	/**
	 * Valida o IP passado usando expressao regular.
	 * Espara-se o formato: <code>000.000.000.000</code> 
	 * 
	 * @param ip O IP a ser validado.
	 * @return Verdade caso o texto passado for um IP.
	 */
	public static boolean isIP(String ip) {
		return validRegExp(REGEXP_IP, ip);
	}
	
	/**
	 * Valida o CPF passado usando expressao regular 
	 * e faz o calculo dos digitos.
	 * Espara-se o formato: <code>xxx.yyy.zzz-kk</code> ou 
	 * <code>xxxyyyzzzkk</code> 
	 * 
	 * @param cpf O CPF a ser validado.
	 * @return Verdade caso o texto passado for um CPF.
	 */
	public static boolean isCPF(String cpf) {
		return validRegExp(REGEXP_CPF, cpf) && verifyCPF(cpf);
	}
	
	/**
	 * Valida o CNPJ passado usando expressao regular.
	 * Espara-se o formato: <code>xxx.yyy.zzz/kkkk-ww</code> ou 
	 * <code>xxxyyyzzzkkkkww</code> 
	 * 
	 * @param cnpj O CNPJ a ser validado.
	 * @return Verdade caso o texto passado for um CNPJ.
	 */
	public static boolean isCNPJ(String cnpj) {
		return validRegExp(REGEXP_CNPJ, cnpj);
	}
	
	/**
	 * Valida a URL passada usando expressao regular.
	 * Espara-se o formato: <code>http://aaa.bbb</code>
	 * 
	 * @param url A URL a ser validado.
	 * @return Verdade caso o texto passado for um URL.
	 */
	public static boolean isURL(String url) {
		return validRegExp(REGEXP_URL, url);
	}
	
	/**
	 * Calcula os digitos verificadores do CPF.
	 * @param cpf O CPF a ser verificado.
	 * @return Verdadeiro caso o calculo conferir.
	 */
	private static boolean verifyCPF(String cpf) {
		int d1, d2;
		int digito1, digito2, resto;
		int digitoCPF;
		String nDigResult;
		String cpfDigits = cpf;
		
		if (cpfDigits.contains("-") || cpfDigits.contains(".")) {
			cpfDigits = cpfDigits.replaceAll("\\-", "");
			cpfDigits = cpfDigits.replaceAll("\\.", "");
		}
		
		String reverseCpf = new StringBuilder(cpfDigits).reverse().toString();
		if (reverseCpf.equals(cpfDigits)) {
			return false;
		}
		reverseCpf = null;
		
		d1 = d2 = 0;
		digito1 = digito2 = resto = 0;

		for (int i = 1; i < cpfDigits.length() - 1; i++) {
			digitoCPF = Convert.toInteger(cpfDigits.substring(i - 1, i));
			d1 = d1 + (11 - i) * digitoCPF;
			d2 = d2 + (12 - i) * digitoCPF;
		}
		
		resto = (d1 % 11);

		if (resto < 2)
			digito1 = 0;
		else
			digito1 = 11 - resto;

		d2 += 2 * digito1;

		resto = (d2 % 11);

		if (resto < 2)
			digito2 = 0;
		else
			digito2 = 11 - resto;

		String nDigVerific = cpfDigits.substring(
				cpfDigits.length() - 2, cpfDigits.length());

		nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
		
		return nDigVerific.equals(nDigResult);
	}
	
	/**
	 * Valida o arquivo passado, verificando se ele existe e nao e nulo.
	 * 
	 * @param file O arquivo a ser validado.
	 * @return Verdade caso o arquivo passado existir.
	 */
	public static boolean isFile(File file) {
		return (file != null) && (file.exists());
	}
	
	/**
	 * Valida o arquivo passado, verificando se ele existe e nao e nulo.
	 * 
	 * @param file O caminho do arquivo a ser validado.
	 * @return Verdade caso o arquivo passado existir.
	 */
	public static boolean isFile(String file) {
		if (Validator.isValid(file)) {
			return isFile(new File(file));
		}
		return false;
	}
	
}
