package com.im.imjutil.sequence;


class NumberSequence extends Sequence {

	public NumberSequence() {
		this(0);
	}
	
	public NumberSequence(int seed) {
		this.setSeed(seed);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Integer next() {
		int number = this.getSeed();
		Integer stop = this.getStopCondition();
		
		if (stop != null && number == stop) {
			return null;
		}
		this.setSeed(number + 1);
		
		return number;
	}
	
}
