package com.im.imjutil.sequence;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.im.imjutil.util.Util;

/**
 * Gera uma sequencia unica de codigos de hash
 * 
 *  @author Felipe Zappala
 */
public class HashSequence extends Sequence {

	private int size;
	private Set<String> generated;
	{
		size = 32;
		generated = new HashSet<String>();
		generated.add("");
	}
	
	public HashSequence() {
		this.setSeed("");
	}
	
	public HashSequence(String seed) {
		this.setSeed(seed);
	}

	@Override
	@SuppressWarnings("unchecked")
	public String next() {
		String hash = getSeed();
		
		while (generated.contains(hash)) {
			hash = Util.generatePassword(size).toUpperCase();
			hash = hash.replaceAll("\\+|\\/", "Z");
		}
		generated.add(hash);
		setSeed(hash);
		
		return hash;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public Set<String> getGenereSet() {
		return Collections.unmodifiableSet(generated);
	}

}
