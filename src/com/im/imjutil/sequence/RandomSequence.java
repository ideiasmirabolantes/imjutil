package com.im.imjutil.sequence;

import java.util.Random;

class RandomSequence extends Sequence {

	public RandomSequence() {
		this(new Random());
	}
	
	public RandomSequence(Random seed) {
		this.setSeed(seed);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Long next() {
		Random random = getSeed();
		return random.nextLong();
	}

}
