package com.im.imjutil.sequence;

class TimeSequence extends Sequence {

	public TimeSequence() {
		this(System.currentTimeMillis());
	}
	
	public TimeSequence(long seed) {
		this.setSeed(seed);
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Long next() {
		long time = this.getSeed();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {/**/}
		
		this.setSeed(System.currentTimeMillis());
		
		return time;
	}

}
