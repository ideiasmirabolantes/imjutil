package com.im.imjutil.sequence;

/**
 * Determina os tipos de sequencia possiveis para a construcao 
 * de um objeto {@link Sequence}.
 * 
 * @author Felipe Zappala
 */
public enum SequenceType {

	/** 
	 * Gera os milesegundos a partir do tempo corrente, 
	 * incrementando segundo a segundo.
	 */
	TIME(TimeSequence.class), 
	
	/**
	 * Gera aleatoriamente de forma nao unica,
	 * incrementa gerando um novo na unico.
	 */
	RANDOM(RandomSequence.class), 
	
	/**
	 * Gera letras iniciando do 'a',
	 * incrementando o unicode do caracter em 1.
	 */
	LETTER(LetterSequence.class), 
	
	/**
	 * Gera numeros iniciando no '1',
	 * incrementando o numero em 1.
	 */
	NUMBER(NumberSequence.class),
	
	/**
	 * Gera um hash unico de 32 caracteres,
	 * incrementa usando um novo hash. 
	 */
	HASH(HashSequence.class);
	
	
	/** Define a classe de implementacao da {@link Sequence} */
	private Class<? extends Sequence> clazz;
	
	/**
	 * Construtor padrao.
	 * 
	 * @param clazz Classe de implementacao da {@link Sequence}
	 */
	private SequenceType(Class<? extends Sequence> clazz) {
		this.clazz = clazz;
	}
	
	/**
	 * Obtem a classe de implementacao da {@link Sequence} deste tipo.
	 *  
	 * @return A classe da sequencia.
	 */
	public Class<? extends Sequence>  getSequenceClass() {
		return this.clazz;
	}
	
}
