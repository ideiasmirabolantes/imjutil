package com.im.imjutil.sequence;

class LetterSequence extends Sequence {
	
	public LetterSequence() {
		this('a');
	}
	
	public LetterSequence(char seed) {
		this.setSeed(seed);
	}

	@Override
	@SuppressWarnings("unchecked")
	public Character next() {
		char next = this.getSeed();
		this.setSeed((char)(next + 1));
		return next;
	}

}
