package com.im.imjutil.query;

import java.util.Properties;

/**
 * Interface responsavel por definir dicas para a 
 * criacao e execucao de uma consulta.
 *
 * @see {@link Query}, {@link FilteredQuery}
 * 
 * @author Felipe Zappala
 */
public interface Hints {
	
	/**
	 * Configura uma dica de consulta {@link Query}.
	 * @param name O nome da dica.
	 * @param value O valor da dica.
	 */
	public void setHint(String name, Object value);
	
	/**
	 * Obtem o valor de uma dica de consulta {@link Query} configurada, 
	 * caso nao encontrado este retorna {@code null}.
	 * 
	 * @param name O nome da dica.
	 * @return O valor da dica ou {@code null}.
	 */
	public <H> H getHint(String name);
	
	/**
	 * Obtem um {@link Properties} com todas as dicas 
	 * configuradas na consulta {@link Query}.
	 * @return Um {@link Properties} contendo as dicas configuradas.
	 */
	public Properties getHints();
	
}
