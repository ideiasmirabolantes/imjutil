package com.im.imjutil.query;

/**
 * Classe que define uma consulta native no sistema. 
 * 
 * @author Felipe Zappala
 */
public class NativeQueryAdapter extends QueryAdapter implements NativeQuery {

	private Class<?> clazz;
	
	public NativeQueryAdapter(String query) {
		super(query);
	}
	
	public NativeQueryAdapter(String query, Class<?> clazz) {
		super(query);
		this.setTarget(clazz);
	}

	@Override
	public Class<?> getTarget() {
		return clazz;
	}

	@Override
	public void setTarget(Class<?> clazz) {
		this.clazz = clazz;
	}

}
