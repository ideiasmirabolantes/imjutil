package com.im.imjutil.query;

import com.im.imjutil.exception.QueryException;
import com.im.imjutil.util.Filter;


/**
 * Interface que defina uma consulta para sistemas de busca.
 * Essa consulta pode ser filtrada por uma instancia de {@link Filter}, 
 * podendo ser de qualquer tipo e deve conhecer como processar os filtros 
 * passados na consulta gerada.
 * 
 * @author Felipe Zappala
 */
public abstract interface Query {
	
	/**
	 * Cria a consulta baseada no filtro passado.
	 * Caso nao exista filtros, cria a consulta para selecao 
	 * de todos os resultados encontrados. 
	 * 
	 * @param filters Filtros para a consulta.
	 * @return A consulta criada baseada nos filtros ou nao.
	 * @throws QueryException caso ocorra algum erro na criacao da consulta.
	 */
	public abstract String createQuery(Filter... filters) throws QueryException;
	
}
