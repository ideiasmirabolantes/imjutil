package com.im.imjutil.query;

import com.im.imjutil.dao.DAO;

/**
 * Interface que define uma consulta nativa no {@link DAO}
 * 
 * @author Felipe Zappala
 */
public interface NativeQuery extends Query {
	
	/**
	 * Obtem a classe alvo da consulta nativa a ser executada.
	 *  
	 * @return A classe alvo da consulta.
	 */
	public Class<?> getTarget();
	
	/**
	 * Configura a classe alvo da consulta nativa a ser executada.
	 * 
	 * @param clazz A classe alvo da consulta.
	 */
	public void setTarget(Class<?> clazz);
	
}
