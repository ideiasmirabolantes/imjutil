package com.im.imjutil.query;

import java.util.Properties;

import com.im.imjutil.exception.QueryException;
import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.util.Filter;
import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;

/**
 * Classe adaptadora que transforma uma consulta qualquer no formato 
 * {@link String} em uma no formato {@link Query}. 
 * Esta nao implementa o processamento dos filtros passados no metodo 
 * {@link #createQuery(Filter...)}, fazendo um simples 
 * retorno da consulta passada no construtor {@link #QueryAdapter(String)}. 
 * Esta implementa a interface {@link FilteredQuery} porem sem processar 
 * seus filtros, utilizando o filtro passado no construtor.  
 * 
 * @see {@link Query}, {@link FilteredQuery}
 * 
 * @author Felipe Zappala
 */
public class QueryAdapter implements Query, FilteredQuery, Hints {

	/** Consulta no formato {@link String} */
	private final String query;
	
	/** Filtro que configura a consulta. */
	private final Filter filter;
	
	/** Dicas a serem aplicadas na execucao da consulta */
	private final Properties hints;
	
	/**
	 * Cria uma consulta personalizada com base na {@link String} passada.
	 * 
	 * @param query A consulta a ser criada.
	 */
	public QueryAdapter(String query) {
		this(query, null);
	}
	
	/**
	 * Cria uma consulta personalizada com base na {@link String} passada.
	 * 
	 * @param query A consulta a ser criada.
	 * @param filter O filtro que configura a consulta.
	 */
	public QueryAdapter(String query, Filter filter) {
		if (!Validator.isValid(query)) {
			throw new ValidationException("Parametro query e nulo ou vazio.");
		}
		this.query = query;
		
		if (Validator.isValid(filter)) {
			this.filter = filter;	
		} else {
			this.filter = new Filter();
		}
		this.hints = new Properties();
	}
	
	/**
	 * Retorna a consulta passada no construtor sem associar os filtros.
	 */
	@Override
	public String createQuery(Filter... filters) throws QueryException {
		return this.query;
	}
	
	/**
	 * Retorna o filtro passado no construtor.
	 */
	@Override
	public Filter createQueryFilter() throws QueryException {
		return this.filter;
	}

	@Override
	public void setHint(String name, Object value) {
		this.hints.put(name, value);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <H> H getHint(String name) {
		return (H) hints.get(name);
	}

	@Override
	public Properties getHints() {
		return hints;
	}
	
	@Override
	public String toString() {
		return Format.toString(this);
	}

}
