package com.im.imjutil.query;

import com.im.imjutil.dao.DAO;
import com.im.imjutil.exception.QueryException;
import com.im.imjutil.util.Filter;

/**
 * Interface responsavel por criar uma consulta {@link Query} que sabe 
 * montar o seu proprio filtro.
 * Esta e destinada a ser implementada por consultas que serao executados
 * atraves do metodo {@link DAO#find(Filter)} ou {@link DAO#findAll(Filter)}.  
 * 
 * @see {@link Filter}, {@link Query}, {@link DAO}
 * 
 * @author Felipe Zappala
 */
public abstract interface FilteredQuery extends Query {

	/**
	 * Cria o filtro para a consulta. 
	 * 
	 * @return O filtro {@link Filter} da consulta {@link Query}. 
	 * @throws QueryException caso ocorra algum erro na criacao do filtro.
	 */
	public abstract Filter createQueryFilter() throws QueryException;
	
}
