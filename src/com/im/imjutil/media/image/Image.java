package com.im.imjutil.media.image;

import java.util.Properties;


/**
 * Interface responsavel por representar uma imagem.
 * 
 * @author Felipe Zappala
 */
public interface Image {
	
	/**
	 * Enumera os formatos de imagem disponiveis.
	 */
	public static enum Format {
		BMP, GIF, JPEG, PNG, TIFF, FPX, PNM
	}
	
	/**
	 * Enumera as cores de imagem disponiveis. 
	 */
	public static enum Color {
		RGB, CMYK, CMY, LAB, BLACKWHITE, GREYSCALE, INDEXED
	}
	
	/**
	 * Enumera os ancoramentos de imagem disponiveis.
	 */
	public static enum Anchor {
		TOP_LEFT, TOP_CENTER, TOP_RIGHT,
		CENTER_LEFT, CENTER_CENTER, CENTER_RIGHT,
		BOTTOM_LEFT, BOTTOM_CENTER, BOTTOM_RIGHT
	}
	
	/**
	 * Obtem a cor da imagem. 
	 */
	public Color getColor();
	
	/**
	 * Obtem o formato da imagem.
	 */
	public Format getFormat();
	
	/**
	 * Obtem a altura em pixel da imagem.
	 */
	public int getHeight();

	/**
	 * Obtem a largura em pixel da imagem.
	 */
	public int getWidth();
	
	/**
	 * Obtem o tamanho em bytes da imagem.
	 */
	public int getSize();
	
	/**
	 * Obtem o objeto fonte da imagem. 
	 */
	public <I> I getSource();
	
	/**
	 * Obtem uma propriedade da imagem. 
	 */
	public <P> P getProperty(String name);
	
	/**
	 * Configura uma propriedade da imagem.
	 */
	public void setProperty(String name, Object value);
	
	/**
	 * Obtem todas as propriedades da imagem. 
	 */
	public Properties getProperties();
	
}
