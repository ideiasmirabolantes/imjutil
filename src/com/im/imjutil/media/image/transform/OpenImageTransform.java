package com.im.imjutil.media.image.transform;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.media.jai.JAI;
import javax.media.jai.OpImage;
import javax.media.jai.PlanarImage;
import javax.media.jai.RenderedOp;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.media.image.Image;
import com.im.imjutil.media.image.Image.Color;
import com.im.imjutil.media.image.Image.Format;
import com.im.imjutil.validation.Convert;
import com.sun.media.jai.codec.SeekableStream;

class OpenImageTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		String command = params.get(0);
		
		if (fullEquals("file", command)) {
			return openImage((File) params.get(1));
			
		} else if (fullEquals("stream", command)) {
			return openImage((InputStream) params.get(1));
			
		} else if (fullEquals("url", command)) {
			return openImage((URL) params.get(1));

		} else if (fullEquals("bytes", command)) {
			return openImage((byte[]) params.get(1));
			
		} else {
			throw new ValidationException(Convert.toString(
					"Comando de abertura invalido: ", command));
		}
	}
	
	private Image openImage(InputStream stream) {
		SeekableStream s = SeekableStream.wrapInputStream(stream, true);
		RenderedOp planarImage = JAI.create("stream", s);
		((OpImage) planarImage.getRendering()).setTileCache(null);
		Format format = ImageUtil.getFormat(stream);
		Color color = ImageUtil.getColor(planarImage);
		
		return ImageUtil.create(planarImage, format, color);
	}
	
	private Image openImage(File file) throws IOException {
		String path = file.getCanonicalPath();
		PlanarImage planarImage = JAI.create("fileload", path);
		Format format = ImageUtil.getFormat(file);
		Color color = ImageUtil.getColor(planarImage);
		
		return ImageUtil.create(planarImage, format, color);
	}
	
	private Image openImage(URL url) throws IOException {
		PlanarImage planarImage = JAI.create("url", url);
		Format format = ImageUtil.getFormat(url.openStream());
		Color color = ImageUtil.getColor(planarImage);
		
		return ImageUtil.create(planarImage, format, color);
	}
	
	private Image openImage(byte[] bytes) {
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		return openImage(bais);
	}

}
