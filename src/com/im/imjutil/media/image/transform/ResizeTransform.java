package com.im.imjutil.media.image.transform;

import java.awt.image.renderable.ParameterBlock;

import javax.media.jai.Interpolation;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;

import com.im.imjutil.media.image.Image;
import com.im.imjutil.validation.Convert;

class ResizeTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		PlanarImage pi = image.getSource();
		float newWidth = Convert.toFloat((Integer) params.get(0));
		float newHeight = Convert.toFloat((Integer) params.get(1));
		
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(pi);
		pb.add(newWidth / pi.getWidth());
		pb.add(newHeight / pi.getHeight());
		pb.add(0.0f);
		pb.add(0.0f);
		pb.add(Interpolation.getInstance(Interpolation.INTERP_NEAREST));
		pi = JAI.create("scale", pb);
		
		return ImageUtil.create(pi, image.getFormat(), image.getColor());
	}

}
