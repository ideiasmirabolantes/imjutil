package com.im.imjutil.media.image.transform;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.media.image.Image;
import com.im.imjutil.validation.Convert;

class SaveImageTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		String command = params.get(0);
		
		if (fullEquals("stream", command)) {
			saveStream(image, (OutputStream) params.get(1));
			
		} else if (fullEquals("file", command)) {
			saveFile(image, (File) params.get(1));
			
		} else {
			throw new ValidationException(Convert.toString(
					"Comando de abertura invalido: ", command));
		}
		return image;
	}
	
	private void saveStream(Image image, OutputStream stream) {
		JAI.create("encode", (PlanarImage)image.getSource(), 
				stream, image.getFormat().toString(), null);
	}
	
	private void saveFile(Image image, File file) throws IOException {
		JAI.create("filestore", (PlanarImage) image.getSource(),
				file.getCanonicalPath(), image.getFormat().toString());
	}

}
