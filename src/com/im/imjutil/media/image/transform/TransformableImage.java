package com.im.imjutil.media.image.transform;

import java.util.Properties;

import javax.media.jai.PlanarImage;

import com.im.imjutil.media.image.Image;
import com.im.imjutil.validation.Convert;

class TransformableImage implements Image {

	private PlanarImage source;
	private Format format;
	private Color color;
	
	public TransformableImage(PlanarImage source, Format format, Color color) {
		this.source = source;
		this.format = format;
		this.color = color;
	}
	
	public TransformableImage(PlanarImage source, Format format) {
		this(source, format, null);
	}
	
	public TransformableImage(PlanarImage source, Color color) {
		this(source, null, color);
	}
	
	public TransformableImage(PlanarImage source) {
		this(source, null, null);
	}
	
	@Override
	public Color getColor() {
		return color;
	}

	@Override
	public Format getFormat() {
		return format;
	}

	@Override
	public int getHeight() {
		return source.getHeight();
	}

	@Override
	public int getWidth() {
		return source.getWidth();
	}

	@Override
	public int getSize() {
		return getWidth() * getHeight();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <I> I getSource() {
		return (I) source;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <P> P getProperty(String name) {
		return (P) this.source.getProperty(name);
	}

	@Override
	public void setProperty(String name, Object value) {
		this.source.setProperty(name, value);
	}
	
	@Override
	public Properties getProperties() {
		Properties properties = new Properties();  
		for (String name : this.source.getPropertyNames()) {
			properties.put(name, this.source.getProperty(name));
		}
		return properties;
	}
	
	@Override
	public String toString() {
		return Convert.toString(this.getClass().getName(), "{",
				" format: ", getFormat(),
				" color: ", getColor(),
				" size: ", getSize(),
				" height: ", getHeight(),
				" width: ", getWidth(),
				" source: ", getSource(),
				"}");
	}

}
