package com.im.imjutil.media.image.transform;

import java.awt.image.renderable.ParameterBlock;

import javax.media.jai.Histogram;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;

import com.im.imjutil.media.image.Image;

class HistogramTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		int bins = 256;
		if (params.isValid(0)) {
			bins = params.get(0);	
		}
	    ParameterBlock pb = new ParameterBlock();
	    PlanarImage pi = image.getSource();
	    pb.addSource(pi);
	    pb.add(null);
	    pb.add(1); 
	    pb.add(1);
	    pb.add(new int[]{bins}); 
	    pb.add(new double[]{0});  
	    pb.add(new double[]{256});
	    PlanarImage dummy = JAI.create("histogram", pb);
	    Histogram histogram = (Histogram) dummy.getProperty("histogram");
	    dummy.setProperty("histogram", histogram.getBins());
	    
		return ImageUtil.create(dummy);
	}

}
