package com.im.imjutil.media.image.transform;

import java.awt.image.renderable.ParameterBlock;

import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;
import javax.media.jai.operator.TransposeDescriptor;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.media.image.Image;
import com.im.imjutil.validation.Convert;

class FlipTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		PlanarImage pi = image.getSource();
		String command = params.get(0);
		
        ParameterBlock pb = new ParameterBlock();
        pb.addSource(pi);
        
        if (fullEquals("horizontal", command)) {
        	pb.add(TransposeDescriptor.FLIP_HORIZONTAL);
        	
        } else if (fullEquals("vertical", command)) {
        	pb.add(TransposeDescriptor.FLIP_VERTICAL);
        	
        } else if (fullEquals("diagonal", command)) {
        	pb.add(TransposeDescriptor.FLIP_DIAGONAL);
        	
        } else if (fullEquals("antidiagonal", command)) {
        	pb.add(TransposeDescriptor.FLIP_ANTIDIAGONAL);
        	
        } else {
        	throw new ValidationException(Convert.toString(
					"Comando de abertura invalido: ", command));
        }
        pi = JAI.create("transpose", pb);
        
		return ImageUtil.create(pi, image.getFormat(), image.getColor());
	}

}
