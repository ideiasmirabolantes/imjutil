package com.im.imjutil.media.image.transform;

import java.awt.image.renderable.ParameterBlock;

import javax.media.jai.Interpolation;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;

import com.im.imjutil.media.image.Image;

class RotateTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		PlanarImage pi = image.getSource();
		int degrees = params.get(0);
		
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(pi);
		pb.add(0.0f);
		pb.add(0.0f);
		pb.add((float) Math.toRadians(degrees));
		pb.add(Interpolation.getInstance(Interpolation.INTERP_BICUBIC_2));
		pi = JAI.create("rotate", pb);
		
		return ImageUtil.create(pi, image.getFormat(), image.getColor());
	}

}
