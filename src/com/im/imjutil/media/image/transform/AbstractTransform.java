package com.im.imjutil.media.image.transform;

import com.im.imjutil.exception.ImageEditingException;
import com.im.imjutil.logging.Logger;
import com.im.imjutil.media.image.Image;
import com.im.imjutil.media.image.Transform;
import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Validator;

abstract class AbstractTransform implements Transform {

	protected abstract Image applyTransform(Image image, Parameter params)
			throws Exception;
	
	@Override
	public final Image apply(Image image, Object... params)
			throws ImageEditingException {
		try {
			Parameter parameters = new Parameter(params);
			return applyTransform(image, parameters);
			
		} catch (Exception e) {
			catchException(e);
			throw new ImageEditingException(Convert.toString(
					e.getClass().getSimpleName(), ": ", e.getMessage()),e);
		}
	}
	
	protected void catchException(Exception ex) {
		Logger.error(ex);
	}

	protected boolean fullEquals(String str1, String str2) {
		if (Validator.isValid(str1, str2)) {
			return str1.trim().toLowerCase().equalsIgnoreCase(
					str2.trim().toLowerCase());
		}
		return false;
	}
	
}
