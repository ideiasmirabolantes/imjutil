package com.im.imjutil.media.image.transform;

import java.awt.image.renderable.ParameterBlock;

import javax.media.jai.Interpolation;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;

import com.im.imjutil.media.image.Image;

class CropTransform extends AbstractTransform {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		PlanarImage pi = image.getSource();
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(pi);
		pb.add(params.get(0));
		pb.add(params.get(1));
		pb.add(params.get(2));
		pb.add(params.get(3));
		pb.add(Interpolation.getInstance(Interpolation.INTERP_NEAREST));
		pi = JAI.create("crop", pb);
		
		return ImageUtil.create(pi, image.getFormat(), image.getColor());
	}

}
