package com.im.imjutil.media.image.transform;

import java.awt.color.ColorSpace;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.media.jai.PlanarImage;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.media.image.Image;
import com.im.imjutil.media.image.Image.Color;
import com.im.imjutil.media.image.Image.Format;

public final class ImageUtil {

	private ImageUtil() {
		super();
	}
	
	public static Image create(PlanarImage pi) {
		return new TransformableImage(pi);
	}
	
	public static Image create(PlanarImage pi, Format format, Color color) {
		return new TransformableImage(pi, format, color);
	}
	
	public static Color getColor(PlanarImage image) {
		try {
			int colorType = image.getColorModel().getColorSpace().getType();
			
			Color color;
			
			switch (colorType) {
			case ColorSpace.TYPE_RGB: 
				color = Color.RGB;
				break;
			case ColorSpace.TYPE_GRAY:
				color = Color.GREYSCALE;
				break;
			case ColorSpace.TYPE_CMYK:
				color = Color.CMYK;
				break;
			case ColorSpace.TYPE_CMY: 
				color = Color.CMY;
				break;
			case ColorSpace.TYPE_Lab: 
				color = Color.LAB;
				break;
			default:
				color = Color.RGB;
			}
			return color;
		} catch (Exception e) {
			Logger.error(e, "[ImageUtil] Impossivel determinar a cor da imagem");
			return null;
		}
	}

	public static Format getFormat(InputStream image) {
		return getImageFormat(image);
	}
	
	public static Format getFormat(File image) {
		return getImageFormat(image);
	}
	
	private static Format getImageFormat(Object image) {
		try {
	        ImageInputStream in = ImageIO.createImageInputStream(image);
	        Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
	        if (readers.hasNext()) {
	        	ImageReader reader = readers.next();
	            return Format.valueOf(reader.getFormatName().toUpperCase());
	        } 
	        throw new IOException("Tipo nao encontrado nos cabecalhos da imagem");
		} catch (Exception e) {
			Logger.error(e, "[ImageUtil] Impossivel determinar o formato da imagem");
			return null;
		}
	}
	
}
