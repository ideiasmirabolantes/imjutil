package com.im.imjutil.media.image.transform;

import java.awt.image.renderable.ParameterBlock;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.im.imjutil.util.Albums;
import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;

class Parameter {

	private List<Object> parameters;
	
	public Parameter() {
		this.parameters = new LinkedList<Object>();
	}
	
	public Parameter(Object... parameters) {
		this.parameters = Albums.asList(parameters);
	}
	
	public boolean isValid(int index) {
		return Validator.isValid(this.parameters.get(index));
	}
	
	@SuppressWarnings("unchecked")
	public <P> P get(int index) {
		return (P) this.parameters.get(index);
	}
	
	public void add(Object parameter) {
		this.parameters.add(parameter);
	}
	
	public List<Object> getAll() {
		return parameters;
	}
	
	public void addAll(Collection<?> parameters) {
		this.parameters.addAll(parameters);
	}
	
	public ParameterBlock toParameterBlock() {
		ParameterBlock pb = new ParameterBlock();
		for (Object object : parameters) {
			pb.add(object);
		}
		return pb;
	}
	
	@Override
	public String toString() {
		return Format.toString(this);
	}
	
}
