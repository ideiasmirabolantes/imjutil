package com.im.imjutil.media.image.transform;

import javax.media.jai.PlanarImage;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.media.image.Image;
import com.im.imjutil.media.image.Image.Color;
import com.im.imjutil.media.image.Image.Format;
import com.im.imjutil.validation.Convert;

class ConvertTransform extends AbstractTransform  {

	@Override
	protected Image applyTransform(Image image, Parameter params)
			throws Exception {
		PlanarImage pi = image.getSource();
		Format format = image.getFormat();
		Color color  = image.getColor();
		
		String command = params.get(0);
		
		if (fullEquals("both", command)) {
			format = params.get(1);
			color = params.get(2);
			
		} else if  (fullEquals("format", command)) {
			format = params.get(1);
			
		} else if (fullEquals("color", command)) {
			color = params.get(1);
			
		} else {
			throw new ValidationException(Convert.toString(
					"Comando de conversao invalido: ", command));
		}
		return ImageUtil.create(pi, format, color);
	}

}
