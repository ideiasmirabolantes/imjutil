package com.im.imjutil.media.image;

import java.awt.image.renderable.ParameterBlock;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.media.jai.Interpolation;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;
import javax.media.jai.operator.TransposeDescriptor;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import com.sun.media.jai.codec.SeekableStream;
import com.sun.media.jai.widget.DisplayJAI;
@SuppressWarnings("unused")
class JAITest {

	public static void main(String[] args) throws Exception {
		File f = new File("/home/fzappala/Área de Trabalho/teste/duke.png");
		InputStream is = new FileInputStream(f);
		URL u = new URL("http://smeira.blog.terra.com.br/files/2009/07/image18.png");
		Image i = ImageEditor.open(is);
		
		System.out.println(i.getWidth());
		System.out.println(i.getHeight());
		
		
//		for (String string : source.getPropertyNames()) {
//			System.out.println(string + ":" + source.getProperty(string));
//		}
		
//		ImageInputStream iis = ImageIO.createImageInputStream(f);
//		Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);
//		
//		while (readers.hasNext()) {
//			System.out.println(readers.next().getFormatName());
//		}
			
		i = ImageEditor.crop(i, 0, 0, 320, 230);
		i = ImageEditor.resize(i, 800, 600);
		
//		int[][] histogram = ImageEditor.histogram(i);
//		for (int[] h : histogram) {
//			for (int t : h) {
//				System.out.println(t);
//			}
//		}
		
//		System.out.println("color type: " + source.getColorModel().getColorSpace().getType());
//		System.out.println("RGB type: " + ColorSpace.TYPE_RGB);
//		System.out.println("GRAY type: " + ColorSpace.TYPE_GRAY);
		
		f = new File("/home/fzappala/Área de Trabalho/teste/duke_edit.png");
		OutputStream os = new FileOutputStream(f);
		ImageEditor.save(i, os);
	}
	
	
	public static void main2(String[] args) throws IOException {
		PlanarImage pi;
		ParameterBlock pb = new ParameterBlock();
		
		// read in the original image from an input stream
		InputStream inputStream = new FileInputStream("resources/images/original/duke_queen.png");
		SeekableStream s = SeekableStream.wrapInputStream(inputStream, true);
		pi = JAI.create("stream", s);
//		((OpImage)image.getRendering()).setTileCache(null);

		
		/*Carrega a imagem, não utiliza Java.io.*,
		isso é bom, pois não teremos que trabalhar com bytes,
		a api cuida disso.*/
//		pi = JAI.create("fileload", "resources/images/original/duke_queen_300ppp.png");

		System.out.println("Largura = " + pi.getWidth());
		System.out.println("Altura = " + pi.getHeight());

		/*Calculo para o novo tamanho, pois o JAI trabalha com
		ponto flutuante como valor para o novo size.*/
//		double newSize = ((double) 200) / ((double) pi.getWidth());

		
		
		/*É selecionado o tipo de renderização, no caso, esta fará a
		interpolação da imagem. Que no caso fará um resize com um
		ótimo efeito, como se tivesse feito num editor de
		imagens(Gimp ou Photoshop).*/
		/*Aqui são passadas a imagem que queremos aplicar
		o resize e o novo tamanho para ela.*/
		
//		double newSize = 1.1;
//		ParameterBlock pb = new ParameterBlock();
//		pb.addSource(pi);
//		pb.add(newSize);
//		pb.add(newSize);
//		RenderingHints qualityHints = new RenderingHints(
//				RenderingHints.KEY_INTERPOLATION,
//				RenderingHints.VALUE_RENDER_QUALITY
//		);
//		pi = JAI.create("SubsampleAverage", pb, qualityHints);
		

		//Tutorial PDF
		// now resize the image
		float scale = 1.5f;
//		scale = 200f / pi.getHeight();
//		pb = new ParameterBlock();
//		pb.addSource(pi);
//		pb.add(scale);
//		pb.add(scale);
//		pb.add(0F);
//		pb.add(0F);
		pb = new ParameterBlock();
		pb.add(2*1208f / pi.getHeight()); //0,078125 - 200
		pb.add(2*960f / pi.getWidth()); // 0,078125 - 150
//		pb.add(new InterpolationNearest());
//		pb.add(Interpolation.getInstance(Interpolation.INTERP_NEAREST));
		pb.add(Interpolation.getInstance(Interpolation.INTERP_BICUBIC_2));
		pi = JAI.create("scale", pb);

		
//		pb = new ParameterBlock();
//	    pb.addSource(pi);
//	    pb.add(0f);
//	    pb.add(0f);
//	    pb.add(200f);
//	    pb.add(200f);
//	    // Create the output image by cropping the input image.
//	    pi = JAI.create("crop",pb);

	    
//	    float shearing = (float)Math.tan((Math.toRadians(15)));
//	    pb = new ParameterBlock();
//	    pb.addSource(pi);
//	    pb.add(shearing);
////	    pb.add(ShearDescriptor.SHEAR_HORIZONTAL);
//	    pb.add(ShearDescriptor.SHEAR_VERTICAL);
//	    pb.add(0.0F);
//	    pb.add(0.0F);
//	    pb.add(new InterpolationNearest());
//	    // Creates a new, sheared image and uses it on the DisplayJAI component
//	    pi = JAI.create("shear", pb);

	    
	    
//	    pb = new ParameterBlock();
//	    pb.addSource(pi);
//	    pb.add(-200f);
//	    pb.add(-200f);
//	    // Create the output image by translating itself.
//	    pi = JAI.create("translate",pb);

		
		/*Como já foi utilizado o ParameterBlock,
		tem que apontar para um novo objeto*/
		pb = new ParameterBlock();

		//É informado o local e o nome da nova imagem e o formato.
		pb.addSource(pi);
		pb.add("resources/images/edited/duke_queen_300ppp.png");
		pb.add("png");
//		pb.add("jpeg");
		JAI.create("filestore", pb);
//		JAI.create("filestore",pi,"cropped.tif","TIFF");
		

		//Cria a imagem em disco redimencionada.
		pi = JAI.create("fileload", "resources/images/edited/duke_queen_300ppp.png");
		System.out.println("Largura = " + pi.getWidth());
		System.out.println("Altura = " + pi.getHeight());

		
		// output stream, in a specific encoding
//		JAI.create("encode", pi, outputStream, "PNG", null);

		display(pi);
		

	}
	
	private static void display(PlanarImage image) {
		JFrame frame = new JFrame();
	    frame.setTitle("Image");
	    frame.getContentPane().add(new JScrollPane(new DisplayJAI(image)));
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
	    frame.pack(); // adjust the frame size using preferred dimensions.
	    frame.setVisible(true); // show the frame.
	}
	
	public static PlanarImage FlipVertical( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.FLIP_VERTICAL );

        dst =  JAI.create( "transpose", pb );

        return dst;
    }

    public static PlanarImage FlipHorizontal( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.FLIP_HORIZONTAL );

        dst =  JAI.create( "transpose", pb );

        return dst;

    }

    public static PlanarImage FlipDiagonal( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.FLIP_DIAGONAL );

        dst =  JAI.create( "transpose", pb );

        return dst;

    }

    public static PlanarImage FlipAntidiagonal( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.FLIP_ANTIDIAGONAL );

        dst =  JAI.create( "transpose", pb );

        return dst;

    }

    public static PlanarImage Rot90( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.ROTATE_90 );

        dst =  JAI.create( "transpose", pb );

        return dst;

    }

    public static PlanarImage Rot180( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.ROTATE_180 );

        dst =  JAI.create( "transpose", pb );

        return dst;

    }

    public static PlanarImage Rot270( PlanarImage src ) {

        PlanarImage dst;

        ParameterBlock pb = new ParameterBlock();
        pb.addSource( src );
        pb.add( TransposeDescriptor.ROTATE_270 );

        dst =  JAI.create( "transpose", pb );

        return dst;
    }
    
    /*
     URLs de teste:
     http://jchrisos.wordpress.com/2008/03/20/java-advanced-imaging-tratando-imagens-facilmente-com-java/
     http://java.sun.com/products/java-media/jai/forDevelopers/jai1_0_1guide-unc/JAITOC.fm.html
     http://download.oracle.com/javase/1.4.2/docs/guide/2d/spec/j2d-bookTOC.html
     https://jaistuff.dev.java.net/operators.html
     http://java.sun.com/products/java-media/jai/forDevelopers/jai1_0_1guide-unc/
     http://java.sun.com/developer/onlineTraining/javaai/jai/index.html
     
     http://www.slideshare.net/whitepapers/introduction-to-the-javatm-advanced-imaging-api
     http://code.google.com/appengine/docs/java/images/overview.html
     http://www.exampledepot.com/egs/javax.imageio/DiscType.html
     http://i-proving.ca/space/Technologies/Java+Advanced+Imaging
     http://www.ideyatech.com/2009/05/converting-to-tiff-on-mac-using-java-advanced-imaging/
     http://www.guj.com.br/posts/list/108555.java
     */
    
}
