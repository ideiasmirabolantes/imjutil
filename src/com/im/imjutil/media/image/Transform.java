package com.im.imjutil.media.image;

import com.im.imjutil.exception.ImageEditingException;

/**
 * Interface responsavel por aplicar transformacoes em imagens {@link Image}.
 * 
 * @author Felipe Zappala
 */
public interface Transform {

	/**
	 * Aplica a transformacao na imagem com os parametros opcionais passados.
	 * 
	 * @param image A imagem fonte a ser transformada.
	 * @param params Os parametros necessarios para a transformacao. 
	 * @return A imagem transformada.
	 * @throws ImageEditingException caso ocorra algum erro.
	 */
	public Image apply(Image image, Object... params) 
			throws ImageEditingException;

}
