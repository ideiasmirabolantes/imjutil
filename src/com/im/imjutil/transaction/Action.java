package com.im.imjutil.transaction;

import com.im.imjutil.exception.TransactionException;

/**
 * Classe que define uma acao em uma transacao.
 * A acao pode ser executada ou desfeita.
 * 
 * @author Bruno Alcantara
 */
public interface Action {
	
	/**
	 * Executa a acao.
	 * 
	 * @param args Lista de argumentos.
	 * @throws Exception caso ocorra algum erro.
	 */
	public void execute(Object... args) throws TransactionException;
	
	/**
	 * Desfaz a acao.
	 * 
	 * @param args Lista de argumentos.
	 * @throws Exception caso ocorra algum erro.
	 */
	public void undo(Object... args) throws TransactionException;
	
}
