package com.im.imjutil.cipher;

import com.im.imjutil.exception.CipherException;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 * Classe responsavel por codificar e decodificar o algoritmo de base64.
 * @see {@link Cipher}
 * 
 * @author Felipe Zappala
 */
class Base64Cipher extends Cipher {

	@Override
	public byte[] encrypt(byte[] value) throws CipherException {
		if (value == null || value.length == 0) {
			throw new CipherException("Byte array nulo ou invalido");
		}
		return Base64.encode(value).getBytes();
	}

	@Override
	public byte[] decrypt(byte[] value) throws CipherException {
		if (value == null || value.length == 0) {
			throw new CipherException("Byte array nulo ou invalido");
		}
		return Base64.decode(new String(value));
	}
	
	@Override
	public void setPassword(byte[] password) {
		throw new UnsupportedOperationException("Operacao nao permitida");
	}
	
	@Override
	public byte[] getPassword() {
		throw new UnsupportedOperationException("Operacao nao permitida");
	}
}