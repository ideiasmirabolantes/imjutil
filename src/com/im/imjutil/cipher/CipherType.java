package com.im.imjutil.cipher;

/**
 * Determina os tipos de algoritmos possiveis para a construcao 
 * de um objeto {@link Cipher}.
 * 
 * @author Felipe Zappala
 */
public enum CipherType {

	/**
	 * Define o algoritmo padrao de criptografia. Atualmente: {@code AES}
	 */
	DEFAULT("AES", AESCipher.class),

	/**
	 * Define o algoritmo de cifra AES (Advanced Encryption Standard)
	 */
	AES("AES", AESCipher.class),

	/**
	 * Define o algoritmo de cifra DES (Data Encryption Standard)
	 */
	DES("DES", DESCipher.class),

	/**
	 * Define o algoritmo de cifra Blowfish (Bruce Schneier)
	 */
	BLOWFISH("Blowfish", BlowfishCipher.class),
	
	/**
	 * Define o algoritmo de hash MD5 (Message-Digest algorithm 5)
	 */
	MD5("MD5", HashCipher.class),

	/**
	 * Define o algoritmo de hash SHA-1 (Secure Hash Algorithm 1)
	 */
	SHA1("SHA-1", HashCipher.class),

	/**
	 * Define o algoritmo de hash SHA-256 (Secure Hash Algorithm 256)
	 */
	SHA2("SHA-256", HashCipher.class),

	/**
	 * Define o algoritmo de cifra BASE64 (Content Transfer Enconding 64 chars)
	 */
	BASE64("BASE64", Base64Cipher.class);
	
	
	/**
	 * Construtor padrao.
	 * 
	 * @param cipher O nome {@link String} do algoritmo.
	 * @param clazz A classe de implementacao do {@link Cipher}
	 */
	private CipherType(String cipher, Class<? extends Cipher> clazz) {
		this.cipher = cipher;
		this.clazz = clazz;
	}
	
	/** Define o nome em {@link String} do algoritmo de codificacao. */
	public final String cipher;
	
	/** Define a classe de implementacao do {@link Cipher} */
	public final Class<? extends Cipher> clazz;
	
}
