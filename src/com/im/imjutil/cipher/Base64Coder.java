package com.im.imjutil.cipher;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

/**
 * Classe responsavel por codificar e decodificar o algoritmo de base64
 * 
 * @author Bruno Alcantara
 */
public class Base64Coder {
	
	/**
	 * Codifica o {@link byte[]} em uma {@link String} em base64.
	 */
	public static String encrypt(byte[] value) {
		return Base64.encode(value);		
	}
	
	/**
	 * Decodifica uma {@link String} em base64 em um {@link byte[]}. 
	 */
	public static byte[] decrypt(String value) {
		return Base64.decode(value);		
	}
	
}
