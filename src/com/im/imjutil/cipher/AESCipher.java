package com.im.imjutil.cipher;

/**
 * Classe responsavel por codificar e decodificar o algoritmo AES.
 * Esta utiliza a classe {@link CryptographyCipher} personalizando 
 * a senha padrao e o algoritmo.
 * 
 * @author Felipe Zappala
 */
final class AESCipher extends CryptographyCipher {

	private static final byte[] DEFAULT_PASS = "0123456789abcdef".getBytes();
	
	public AESCipher() {
		super(CipherType.AES);
		password = DEFAULT_PASS;
	}

	@Override
	public void setPassword(byte[] password) {
		throw new UnsupportedOperationException("Operacao nao permitida");
	}
	
	@Override
	public byte[] getPassword() {
		throw new UnsupportedOperationException("Operacao nao permitida");
	}
	
}
