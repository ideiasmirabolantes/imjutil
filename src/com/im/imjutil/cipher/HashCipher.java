package com.im.imjutil.cipher;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.im.imjutil.exception.CipherException;
import com.im.imjutil.exception.InstantiateException;
import com.im.imjutil.validation.Convert;

/**
 * Classe responsavel por codificar e decodificar algoritmos de hash.
 * 
 * @author Felipe Zappala
 */
public class HashCipher extends Cipher {
	
	private CipherType type;
	private MessageDigest messageDigest;
	
	public HashCipher(CipherType type) {
		this.type = type;
		try {
			messageDigest = MessageDigest.getInstance(type.cipher);
		} catch (NoSuchAlgorithmException e) {
			throw new InstantiateException(Convert.toString(
					"Algoritmo nao suportado [", type.cipher, "]"), e);
		}
	}
	
	@Override
	public byte[] encrypt(byte[] value) throws CipherException {
		try {
			messageDigest.reset();
			messageDigest.update(value);
			return messageDigest.digest();
			
		} catch (Exception e) {
			throw new CipherException(Convert.toString(
					"Erro ao codificar, causa [", e.getClass().getSimpleName(),
					" - ", e.getMessage(), "]"), e);
		}
	}

	@Override
	public byte[] decrypt(byte[] value) throws CipherException {
		throw new CipherException(Convert.toString(
				"Operacao nao suportada pelo algoritmo [", type.cipher, "]"));
	}

	@Override
	public void setPassword(byte[] password) {
		throw new UnsupportedOperationException("Operacao nao permitida");
	}
	
	@Override
	public byte[] getPassword() {
		throw new UnsupportedOperationException("Operacao nao permitida");
	}
}
