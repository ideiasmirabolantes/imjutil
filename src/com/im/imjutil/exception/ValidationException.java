package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por validadores do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class ValidationException extends SystemUncheckedException {

	private static final long serialVersionUID = 210594849884028461L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da validacao.
	 * 
	 * @param message Mensagem de erro da validacao.
	 */
	public ValidationException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da validacao 
	 * e passa uma outro excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da validacao.
	 * @param cause Excecao de causa
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
