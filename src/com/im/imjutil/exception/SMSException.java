package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por envio de SMS do sistema da VoxAge.
 * @see {@link SystemUncheckedException}
 * 
 * @author Bruno Alcantara.
 */
public class SMSException extends SystemUncheckedException {

	private static final long serialVersionUID = 6570750470969082582L;

	/**
	 * Constroi uma excecao com uma mensagem de erro do envio de SMS.
	 * 
	 * @param message Mensagem de erro do envio de SMS.
	 */
	public SMSException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do envio de SMS e passa 
	 * uma outra excecao que causou a atual.
	 * 
	 * @param message Mensagem de erro do envio de SMS.
	 * @param cause Excecao de causa
	 */
	public SMSException(String message, Throwable cause) {
		super(message, cause);
	}

}
