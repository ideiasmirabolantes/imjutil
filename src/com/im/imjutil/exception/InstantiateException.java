package com.im.imjutil.exception;

/**
 * Classe de excecao lancada por instanciadores de classes do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class InstantiateException extends SystemUncheckedException {

	private static final long serialVersionUID = -1001018162462990453L;

	/**
	 * Constroi uma excecao com uma mensagem de erro do instanciador.
	 * 
	 * @param message Mensagem de erro do email.
	 */
	public InstantiateException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do instanciador e passa 
	 * uma outra excecao que causou a atual.
	 * 
	 * @param message Mensagem de erro do codificador.
	 * @param cause Excecao de causa
	 */
	public InstantiateException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
