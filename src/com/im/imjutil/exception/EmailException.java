package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por envio de email do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class EmailException extends SystemUncheckedException {

	private static final long serialVersionUID = 5263519110214520173L;
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do email.
	 * 
	 * @param message Mensagem de erro do email.
	 */
	public EmailException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do email e passa 
	 * uma outra excecao que causou a atual.
	 * 
	 * @param message Mensagem de erro do email.
	 * @param cause Excecao de causa
	 */
	public EmailException(String message, Throwable cause) {
		super(message, cause);
	}

}
