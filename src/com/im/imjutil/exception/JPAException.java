package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por persistidores JPA do sistema.
 * @see {@link PersistenceException}
 * 
 * @author Felipe Zappala
 */
public class JPAException extends PersistenceException {

	private static final long serialVersionUID = 3694521349371069015L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da persistencia do JPA.
	 * 
	 * @param message Mensagem de erro da persistencia.
	 */
	public JPAException(String message) {
		super(message);
	}

	/**
	 * Constroi uma excecao com uma mensagem de erro da persistencia JPA
	 * e passa uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da persistencia.
	 * @param cause Excecao de causa
	 */
	public JPAException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
