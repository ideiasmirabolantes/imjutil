package com.im.imjutil.exception;

/**
 * Classe de excecao lancada por validadores da logica de negocio do sistema.
 * @see {@link ValidationException}
 * 
 * @author Felipe Zappala
 */
public class BusinessException extends ValidationException {

	private static final long serialVersionUID = -8796980169414257745L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da validacao.
	 * 
	 * @param message Mensagem de erro da validacao.
	 */
	public BusinessException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da validacao 
	 * e passa uma outro excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da validacao.
	 * @param cause Excecao de causa
	 */
	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}
}
