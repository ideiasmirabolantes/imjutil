package com.im.imjutil.exception;

import com.im.imjutil.validation.Convert;

/**
 * Classe que define uma excecao verificada do sistema.
 * 
 * @author Felipe Zappala
 */
public class SystemCheckedException extends Exception {

	private static final long serialVersionUID = -8299302822471444804L;
	
	/**
	 * Constroi uma excecao verificada.
	 */
	public SystemCheckedException() {
		super();
	}

	/**
	 * Constroi uma excecao nao verificada.
	 * 
	 * @param message Mensagem da excecao.
	 */
	public SystemCheckedException(Object...message) {
		super(Convert.toString(message));
	}
	
	/**
	 * Constroi uma excecao nao verificada.
	 * 
	 * @param message Mensagem da excecao.
	 * @param cause Causa da excecao.
	 */
	public SystemCheckedException(Throwable cause, Object...message) {
		super(Convert.toString(message), cause);
	}
	
}
