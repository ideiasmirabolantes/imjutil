package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por persistidores JAXB do sistema.
 * @see {@link PersistenceException}
 * 
 * @author Felipe Zappala
 */
public class JAXBException extends PersistenceException {

	private static final long serialVersionUID = -4950655472675869749L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da persistencia do JAXB.
	 * 
	 * @param message Mensagem de erro da persistencia.
	 */
	public JAXBException(String message) {
		super(message);
	}

	/**
	 * Constroi uma excecao com uma mensagem de erro da persistencia JAXB
	 * e passa uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da persistencia.
	 * @param cause Excecao de causa
	 */
	public JAXBException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
