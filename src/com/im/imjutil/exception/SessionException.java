package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por sessoes do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class SessionException extends SystemUncheckedException {

	private static final long serialVersionUID = -5938047032225828143L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da sessoes.
	 * 
	 * @param message Mensagem de erro da transacao.
	 */
	public SessionException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da sessoes e passa 
	 * uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da transacao.
	 * @param cause Excecao de causa 
	 */
	public SessionException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
