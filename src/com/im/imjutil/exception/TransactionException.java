package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por transacoes do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class TransactionException extends SystemUncheckedException {

	private static final long serialVersionUID = -6401594177649092717L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da transacao.
	 * 
	 * @param message Mensagem de erro da transacao.
	 */
	public TransactionException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da transacao e passa 
	 * uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da transacao.
	 * @param cause Excecao de causa
	 */
	public TransactionException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
