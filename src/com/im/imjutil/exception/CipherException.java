package com.im.imjutil.exception;

/**
 * Classe de excecao lancada por codificadores e decodificadores do sistema.
 * @see {@link SystemCheckedException}
 * 
 * @author Felipe Zappala
 */
public class CipherException extends SystemCheckedException {

	private static final long serialVersionUID = -3120421642091886542L;

	/**
	 * Constroi uma excecao com uma mensagem de erro do codificador.
	 * 
	 * @param message Mensagem de erro do email.
	 */
	public CipherException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do codificador e passa 
	 * uma outra excecao que causou a atual.
	 * 
	 * @param message Mensagem de erro do codificador.
	 * @param cause Excecao de causa
	 */
	public CipherException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
