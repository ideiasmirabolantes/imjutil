package com.im.imjutil.exception;


/**
 * Classe de excecao lancada pelo sistema de controle de permissao.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class PermissionException extends SystemUncheckedException {

	private static final long serialVersionUID = -7068801528266037204L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da permissao.
	 * 
	 * @param message Mensagem de erro da permissao.
	 */
	public PermissionException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da permissao e passa 
	 * uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da permissao.
	 * @param cause Excecao de causa.
	 */
	public PermissionException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
