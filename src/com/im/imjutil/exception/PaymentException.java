package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por pagamentos do sistema.
 * @see {@link SystemCheckedException}
 * 
 * @author Felipe Zappala
 */
public class PaymentException extends SystemUncheckedException {

	private static final long serialVersionUID = -1061783392464932870L;

	/**
	 * Constroi uma excecao com uma mensagem de erro do pagamento.
	 * 
	 * @param message Mensagem de erro do pagamento.
	 */
	public PaymentException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do pagamento e passa 
	 * uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro do pagamento.
	 * @param cause Excecao de causa
	 */
	public PaymentException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
