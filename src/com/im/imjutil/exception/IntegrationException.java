package com.im.imjutil.exception;

/**
 * Classe de excecao lancada por integradores do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class IntegrationException extends SystemUncheckedException {

	private static final long serialVersionUID = 4126589576861436969L;
	
	/**
	 * Constroi uma excecao com uma mensagem de erro do integrador.
	 * 
	 * @param message Mensagem de erro da integracao.
	 */
	public IntegrationException(String message) {
		super(message);
	}

	/**
	 * Constroi uma excecao com uma mensagem de erro do integrador
	 * e passa uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da integracao.
	 * @param cause Excecao de causa
	 */
	public IntegrationException(String message, Throwable cause) {
		super(message, cause);
	}

}
