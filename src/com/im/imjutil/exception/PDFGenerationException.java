package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por geradores de PDF do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Bruno Alcantara.
 */
public class PDFGenerationException extends SystemUncheckedException {

	private static final long serialVersionUID = 210594849884028461L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da geracao do aqruivo PDF.
	 * 
	 * @param message Mensagem de erro da geracao do arquivo PDF.
	 */
	public PDFGenerationException(String message) {
		super(message);
	}

	/**
	 * Constroi uma excecao com uma mensagem de geracao do arquivo PDF e passa
	 * uma outro excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da geracao do arquivo PDF.
	 * @param cause Excecao de causa
	 */
	public PDFGenerationException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
