package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por editores de imagem do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class ImageEditingException extends SystemUncheckedException {

	private static final long serialVersionUID = 5753615637835176764L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da edicao de imagem.
	 * 
	 * @param message Mensagem de erro da edicao de imagem.
	 */
	public ImageEditingException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da edicao de imagem 
	 * e passa uma outro excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da edicao de imagem.
	 * @param cause Excecao de causa
	 */
	public ImageEditingException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
