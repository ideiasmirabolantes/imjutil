package com.im.imjutil.exception;

/**
 * Classe de excecao lancada por operacoes do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class OperationException extends SystemUncheckedException {

	private static final long serialVersionUID = -3522696311361782679L;

	/**
	 * Constroi uma excecao com uma mensagem de erro da operacao.
	 * 
	 * @param message Mensagem de erro da operacao.
	 */
	public OperationException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da operacao 
	 * e passa uma outro excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da operacao.
	 * @param cause Excecao de causa.
	 */
	public OperationException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
