package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por geradores de consultas do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class QueryException extends SystemUncheckedException {

	private static final long serialVersionUID = -6574586911416562255L;

	/**
	 * Constroi uma excecao com uma mensagem de erro do gerador de consulta.
	 * 
	 * @param message Mensagem de erro da geracao.
	 */
	public QueryException(String message) {
		super(message);
	}

	/**
	 * Constroi uma excecao com uma mensagem de erro do gerador de consulta
	 * e passa uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da geracao.
	 * @param cause Excecao de causa
	 */
	public QueryException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
