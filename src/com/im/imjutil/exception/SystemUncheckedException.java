package com.im.imjutil.exception;


/**
 * Classe que define uma excecao nao verificadas do sistema.
 * 
 * @author Felipe Zappala
 */
public class SystemUncheckedException extends RuntimeException {

	private static final long serialVersionUID = -2302358440291434454L;
	
	/**
	 * Constroi uma excecao nao verificada.
	 */
	public SystemUncheckedException() {
		super();
	}
	
	/**
	 * Constroi uma excecao nao verificada.
	 * 
	 * @param message Mensagem da excecao.
	 */
	public SystemUncheckedException(String message) {
		super(message);
	}

	/**
	 * Constroi uma excecao nao verificada.
	 *
	 * @param message Mensagem da excecao.
	 * @param cause Causa da excecao.
	 */
	public SystemUncheckedException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
