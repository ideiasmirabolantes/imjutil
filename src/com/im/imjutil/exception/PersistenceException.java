package com.im.imjutil.exception;


/**
 * Classe de excecao lancada por persistidores do sistema.
 * @see {@link SystemUncheckedException}
 * 
 * @author Felipe Zappala
 */
public class PersistenceException extends SystemUncheckedException {

	private static final long serialVersionUID = -5152436545854279607L;
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da persistencia.
	 * 
	 * @param message Mensagem de erro da persistencia.
	 */
	public PersistenceException(String message) {
		super(message);
	}
	
	/**
	 * Constroi uma excecao com uma mensagem de erro da persistencia e passa 
	 * uma outra excecao que causou a atual.
	 *
	 * @param message Mensagem de erro da persistencia.
	 * @param cause Excecao de causa
	 */
	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

}
