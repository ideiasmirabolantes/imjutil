package com.im.imjutil.config;

import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.util.Util;
import com.im.imjutil.validation.Convert;

/**
 * Class that implements settings file properties. 
 * The settings file for this class must have the name 
 * '<code>/META-INF/config.properties</code>' and located in the 
 * <code>CLASSPATH</code> 
 * of the application. The file should follow the specification of 
 * <code>java.util.Properties</code>. If the file is not found the class will 
 * throw a <code>java.lang.RuntimeException</code> to signal its lack.

 * @see java.lang.RuntimeException
 * @see java.util.Properties
 * 
 * @author Felipe Zappala
 */
public final class Configurator {

	/** The properties of system. */
	private static Properties config;
	
	private static final String pathFileName;
	
	/**
	 * Load the property file on class load.
	 */
	static {
		pathFileName = "/META-INF/config.properties";
		reload();
	}
	
	/**
	 * Avoiding instantiation.
	 */
	private Configurator() {
		super();
	}
	
	/**
	 * Recarrega as propriedades do arquivo de configuracao.
	 */
	public static void reload() {
		try {
			if (!loadEncrypted())
				loadPlain();
			
		} catch (Exception e) {
			throw new ValidationException(Convert.toString(
					"Error on load the ", pathFileName, " file."), e);
		}
	}
	
	private static void loadPlain() throws Exception {
		InputStream file = Configurator.class.getResourceAsStream(pathFileName);
		config = new Properties();
		config.load(file);
	}
	
	private static boolean loadEncrypted() throws Exception {
		InputStream resource = Configurator.class.getResourceAsStream(pathFileName + ".crypt");
		
		if (resource != null) {
			String content = new String(Convert.toBytes(resource));
			content = Util.decryptPassword(content);
			StringReader stringReader = new StringReader(content);
			
			config = new Properties();
			config.load(stringReader);
			return true;
		}
		return false;
	}
	
	/**
	 * Get the property in <code>config.properties</code> file by name.
	 * 
	 * @param name The name of property.
	 * @return The value of property.
	 */
	public static final String getProperty(String name) {
		String value = config.getProperty(name);
		
		if (value != null) {
			value = value.trim();
		}
		
		return value;
	}
	
	/**
	 * Get the property in <code>config.properties</code> file by name.
	 * 
	 * @param name The name of property.
	 * @return The value of property.
	 */
	public static final String getPrt(String name) { 
		return getProperty(name);
	}
	
	/**
	 * Get the property in <code>config.properties</code> file by name.
	 * 
	 * @param name The name of property.
	 * @return The value of property.
	 */
	public static final String get(String name) { 
		return getProperty(name);
	}
		
	/**
	 * Retorna um Map com chave e valor de todas 
	 * as propriedades contidas no arquivo.
	 * 
	 * @return Map com chave e valor das propriedades.
	 */
	public static final  Map<String, String> getAllProperties() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		
		for (Entry<Object, Object> element : config.entrySet()) {
			Object value = element.getValue();
			
			if (value != null) {
				value = value.toString().trim();
			}
			
			if (value == null) {
				map.put(element.getKey().toString(), null);
			} else {
				map.put(element.getKey().toString(), value.toString());
			}
		}
		
		return map;
	}
	 
	/**
	 * Retorna um Map com chave e valor de todas as propriedades contidas 
	 * no arquivo na mesma ordem que elas estao no mesmo.
	 * 
	 * @return Map ordenado com chave e valor das propriedades.
	 */
	public static Map<String, String> getSequenceAllProperties() {

		URL file = Configurator.class.getResource(pathFileName);
		
		Map<String, String> registers = new LinkedHashMap<String, String>();
		
		List<String> result = com.im.imjutil.file.File.read(file.getPath());
		
		for (String element : result) {
			if (element != null) {
				element = element.trim();
			}
			
			if (element != null && !(element.startsWith("#")) 
					&& (!element.trim().equals(""))) {
				String [] lineContents = element.split("=");
				
				if (lineContents.length == 2) {
					registers.put(lineContents[0], lineContents[1]);
				}
			}
		}
		
		return registers;
	}
	
}

