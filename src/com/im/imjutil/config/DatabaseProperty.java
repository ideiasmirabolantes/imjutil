package com.im.imjutil.config;

import java.io.Serializable;
import java.util.Properties;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Format;

/**
 * Classe que define um modelo de propriedades no banco de dados.
 * Utiliza como gerenciador a classe {@link DatabaseConfig}, que abstrai
 * o mecanismo do banco em uma interface parecida com a da classe 
 * {@link Properties}.
 * 
 * @author Felipe Zappala
 */
@Entity
@Table(name = "t_database_property")
@SequenceGenerator(name = "t_database_property_seq", 
	sequenceName = "t_database_property_id_seq", allocationSize = 1)
@NamedQueries(@NamedQuery(name="DatabaseProperty.findAll", 
	query="SELECT p FROM DatabaseProperty AS p ORDER BY p.map, p.key, p.value")
)
class DatabaseProperty 
		implements Serializable, Comparable<DatabaseProperty> {

	@Id
	@GeneratedValue(
    		strategy = GenerationType.SEQUENCE, 
    		generator = "t_database_property_seq")
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
	private long id;
	
	@Basic(optional=false)
    @Column(name = "map", nullable=false)
	private String map;
	
	@Basic(optional=false)
    @Column(name = "key", nullable=false)
	private String key;
	
	@Basic(optional=false)
    @Column(name = "value", nullable=false)
	private String value;

	private static final long serialVersionUID = -8623911041015216072L;

	/**
	 * Construtor padrao {@link DatabaseProperty}.
	 */
	public DatabaseProperty() {
		this.map = "";
		this.key = "";
		this.value = null;
	}
	
	/**
	 * Construtor personalizado {@link DatabaseProperty}. 
	 */
	public DatabaseProperty(String map, String key, String value) {
		this.set(map, key, value);
	}
	
	public void set(String map, String key, String value) {
		this.setMap(map);
		this.setKey(key);
		this.setValue(value);
	}
	
	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		if (map != null) {
			this.map = map;
		}
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		if (key != null) {
			this.key = key;
		}
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		if (value != null) {
			this.value = value;
		}
	}

	public long getId() {
		return id;
	}

	private String str() {
		return Convert.toString(this.map, this.key, this.value);
	}
	
	public boolean isEmpty() {
		return this.str().isEmpty();
	}
	
	@Override
	public int hashCode() {
		return this.str().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			DatabaseProperty another = (DatabaseProperty) obj;
			return this.str().equals(another.str());
		}
		return false;
	}
	
	@Override
	public int compareTo(DatabaseProperty another) {
		if (another != null) {
			return this.str().compareTo(another.str());
		}
		return -1;
	}
	
	@Override
	public String toString() {
		return Format.toString(this);
	}

}
