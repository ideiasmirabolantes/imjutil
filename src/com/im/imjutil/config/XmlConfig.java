package com.im.imjutil.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * This class implements a XML configuration file parser. It has some validation
 * facilities, thus allowing for XML syntax correctness, and also makes it
 * possible to create validation rules that simplifies the work of reading and
 * validating specific configuration types.
 */
public class XmlConfig implements ErrorHandler {

	private final String document;
    private final String schema;
    private Element root;
    
    public XmlConfig(String document) {
        this(document, document.matches(".*\\.xml$") ? document.replace(".xml",
            ".xsd") : null);
    }

    /**
     * Class constructor.
     * <p>
     * The string parameters must be a file name, full path or URL.
     * </p>
     * 
     * @param document
     *            The document containing the configuration to be loaded and
     *            validated.
     * @param schema
     *            The schema file used to validate the configuration file.
     */
    public XmlConfig(String document, String schema) {
        this.document = document;
        this.schema = schema;
    }

    /**
     * Get the root element of the XML document.
     * 
     * @return the root element of the XML tree.
     */
    public Element getRootElement() {
        return root;
    }

    /**
     * Receive notification of a recoverable error.
     * <p>
     * This corresponds to the definition of "error" in section 1.2 of the W3C
     * XML 1.0 Recommendation. For example, a validating parser would use this
     * callback to report the violation of a validity constraint. The default
     * behaviour is to take no action.
     * </p>
     * <p>
     * The SAX parser must continue to provide normal parsing events after
     * invoking this method: it should still be possible for the application to
     * process the document through to the end. If the application cannot do so,
     * then the parser should report a fatal error even if the XML
     * recommendation does not require it to do so.
     * </p>
     * <p>
     * Filters may use this method to report other, non-XML errors as well.
     * </p>
     * 
     * @param exception
     *            The error information encapsulated in a SAX parse exception.
     * @exception org.xml.sax.SAXException
     *                Any SAX exception, possibly wrapping another exception.
     * @see org.xml.sax.SAXParseException
     */
    @Override
	public void error(SAXParseException exception) throws SAXException {
        throw new SAXException(this.getMessage(exception), exception);
    }

    /**
     * Receive notification of a non-recoverable error.
     * <p>
     * <strong>There is an apparent contradiction between the documentation for
     * this method and the documentation for
     * {@link org.xml.sax.ContentHandler#endDocument}. Until this ambiguity is
     * resolved in a future major release, clients should make no assumptions
     * about whether endDocument() will or will not be invoked when the parser
     * has reported a fatalError() or thrown an exception.</strong>
     * </p>
     * <p>
     * This corresponds to the definition of "fatal error" in section 1.2 of the
     * W3C XML 1.0 Recommendation. For example, a parser would use this callback
     * to report the violation of a well-formedness constraint.
     * </p>
     * <p>
     * The application must assume that the document is unusable after the
     * parser has invoked this method, and should continue (if at all) only for
     * the sake of collecting additional error messages: in fact, SAX parsers
     * are free to stop reporting any other events once this method has been
     * invoked.
     * </p>
     * 
     * @param exception
     *            The error information encapsulated in a SAX parse exception.
     * @exception org.xml.sax.SAXException
     *                Any SAX exception, possibly wrapping another exception.
     * @see org.xml.sax.SAXParseException
     */
    @Override
	public void fatalError(SAXParseException exception) throws SAXException {
        throw new SAXException(this.getMessage(exception), exception);
    }

    /**
     * Retrieves an attribute value by name.
     * 
     * @param name
     *            The name of the attribute to retrieve.
     * @return The attribute value as a string, or the empty string if that
     *         attribute does not have a specified or default value.
     */
    public String getAttribute(String name) {
        return this.root.getAttribute(name);
    }

    /**
     * Retrieves child elements identified by a given name.
     * 
     * @param name
     *            the name of the elements to retrieve
     * @return A list of matching elements
     */
    public List<Element> getElements(String name) {
        return getChildElements(this.root, name);
    }

    /**
     * Parse the content of the configuration file as an XML document.
     * 
     * @return this XmlConfig object.
     * @throws IOException
     *             If any I/O errors occur.
     * @throws SAXException
     *             If any parse errors occur.
     * @see org.xml.sax.DocumentHandler
     */
    public XmlConfig parse() throws Exception {
        InputStream is = getResource(this.document).openStream();

        DocumentBuilderFactory documentBuilderFactory =
            DocumentBuilderFactory.newInstance();

        if (this.schema != null) {
            SchemaFactory schemaFactory =
                SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
            Schema schema = schemaFactory.newSchema(getResource(this.schema));
            documentBuilderFactory.setSchema(schema);
        }

        try {
            DocumentBuilder documentBuilder =
                documentBuilderFactory.newDocumentBuilder();
            documentBuilder.setErrorHandler(this);
            Document document = documentBuilder.parse(is);
            this.root = document.getDocumentElement();
        } catch (ParserConfigurationException e) {
            throw new SAXException(e.getMessage());
        }

        return this;
    }

    /**
     * Receive notification of a warning.
     * <p>
     * SAX parsers will use this method to report conditions that are not errors
     * or fatal errors as defined by the XML recommendation. The default
     * behaviour is to take no action.
     * </p>
     * <p>
     * The SAX parser must continue to provide normal parsing events after
     * invoking this method: it should still be possible for the application to
     * process the document through to the end.
     * </p>
     * <p>
     * Filters may use this method to report other, non-XML warnings as well.
     * </p>
     * 
     * @param exception
     *            The warning information encapsulated in a SAX parse exception.
     * @exception org.xml.sax.SAXException
     *                Any SAX exception, possibly wrapping another exception.
     * @see org.xml.sax.SAXParseException
     */
    @Override
	public void warning(SAXParseException exception) throws SAXException {
        throw new SAXException(getMessage(exception), exception);
    }

    /**
     * Returns a customized message for a given exception.
     * 
     * @param exception
     *            The exception used to build the message.
     * @return A formatted message for the error indicated by the exception.
     */
    private String getMessage(SAXParseException exception) {
        return exception.getMessage() + " (line " + exception.getLineNumber() +
            ')';
    }

    /**
     * Find the configuration file/resource. This method will search in
     * different places The rearch order is as follows:
     * <ol>
     * <p>
     * <li>Search for a existent file path.
     * <p>
     * <li>Search using class loader that loaded this class.
     * <p>
     * <li>Search using the thread context class loader.
     * <p>
     * <li>Try one last time with
     * <code>ClassLoader.getSystemResourceAsStream</code>
     * </ol>
     * 
     * @param name
     *            The resource name
     * @return URL The resource's <code>URL</code>
     */
    public static URL getResource(String name) throws IOException {
        File file = new File(name);
        if (file.exists()) {
            try {
                return file.toURI().toURL();
            } catch (MalformedURLException e) {
                // should never occur
            }
        }
        URL url = XmlConfig.class.getResource(name);
        if (url == null) {
            url =
                Thread.currentThread().getContextClassLoader()
                    .getResource(name);
            if (url == null) {
                url = ClassLoader.getSystemResource(name);
            }
        }
        if (url == null) {
            throw new IOException("Resource \"" + name + "\" file not found");
        }
        return url;
    }

    /**
     * Retrieves child elements of a given node that are identified by name.
     * 
     * @param parent
     *            the parent element
     * @return A list of matching elements
     */
    public static List<Element> getChildElements(Element parent, String name) {
        NodeList nodeList = parent.getElementsByTagName(name);
        ArrayList<Element> elementList =
            new ArrayList<Element>(nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                elementList.add((Element) node);
            }
        }
        return elementList;
    }

    /**
     * Retrieves all child elements of a given node.
     * 
     * @param parent
     *            the parent element
     * @return A list of matching elements
     */
    public static List<Element> getChildElements(Element parent) {
        NodeList nodeList = parent.getChildNodes();
        ArrayList<Element> elementList =
            new ArrayList<Element>(nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                elementList.add((Element) node);
            }
        }
        return elementList;
    }

    /**
     * Get a text element value
     * 
     * @param element
     *            the target element name
     * @return the text value; null if the element does not contain any text
     *         element.
     */
    public static String getValue(Element element) {
        NodeList nodeList = element.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); ++i) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.TEXT_NODE) {
                return node.getNodeValue().trim();
            }
        }
        return null;
    }

}
