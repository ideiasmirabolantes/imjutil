package com.im.imjutil.serialization;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * Classe responsavel por gerar a serializacao texto de um objeto binario,
 * podendo usar os protocolos XML ou JSON.
 * 
 * @author Felipe Zappala
 */
public final class Serializer {

	private Serializer() {
		super();
	}
	
	/**
	 * algoritmo de serializacao
	 */
	@SuppressWarnings("unchecked")
	private static <T> T serializer(Object object, 
			boolean isJSON, boolean isSerialize) {
		XStream serializer;
		if (isJSON) {
			// Serializador JSON
			serializer = new XStream(new JettisonMappedXmlDriver());
		} else {
			// Serializador XML
			serializer = new XStream(new DomDriver());
		}
		// Processa automaticamente as anotacoes presentes nos modelos.
		serializer.autodetectAnnotations(true);

		if (isSerialize) {
			// Serializa o objeto
			return (T) serializer.toXML(object);
		}
		// Desserializa o objeto
		return (T) serializer.fromXML((String) object);
	}
	
	/**
	 * Serializa um objeto Java para string XML.
	 */
	public static String toXML(Object obj) {
		return serializer(obj, false, true);
	}
	
	/**
	 * Desserializa uma string XML para um objeto Java. 
	 */
	public static <T> T fromXML(String obj) {
		return serializer(obj, false, false);
	}
	
	/**
	 * Serializa um objeto Java para uma string JSON.
	 */
	public static String toJSON(Object obj) {
		return serializer(obj, true, true);
	}
	
	/**
	 * Desserializa uma string JSON para um objeto Java.
	 */
	public static <T> T fromJSON(String obj) {
		return serializer(obj, true, false);
	}
	
}
