package com.im.imjutil.email;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;

/**
 * Classe que define um email. 
 * Esta deve conter no mínimo um endereco de destino para ser valida.
 * Esta pode conter arquivos anexos opcionalmente.<br>
 * Para enviar o email use a classe {@link EmailSender}.
 * @see {@link EmailSender}
 * 
 * @author Felipe Zappala
 */
public class Email {

	private Set<String> to;
	private String subject;
	private String message;
	private Set<File> attachments;
	
	/**
	 * Constroi um email padrao em branco.
	 */
	public Email() {
		this.to = new HashSet<String>();
		this.subject = "";
		this.message = "";
		this.attachments = new HashSet<File>();
	}

	/**
	 * Constroi um email em branco passando um ou varios destinatarios.
	 * 
	 * @param to Endereco de email dos destinatarios.
	 */
	public Email(String... to) {
		this();
		this.setTo(to);
	}
	
	/**
	 * Verifica se o email esta vazio, ou seja, sem destinatarios.
	 * 
	 * @return Verdadeiro se o email nao contiver destinatarios.
	 */
	public boolean isEmpty() {
		return this.to.isEmpty();
	}
	
	/**
	 * Verifica se o email possui anexos.
	 * 
	 * @return Verdadeiro se o email possuir anexos.
	 */
	public boolean hasAttachments() {
		return !this.attachments.isEmpty();
	}
	
	/**
	 * Obtem o conjunto imutavel dos enderecos de email dos destinatarios.
	 * @return Um conjunto de emails.
	 */
	public Set<String> getTo() {
		return Collections.unmodifiableSet(this.to);
	}

	/**
	 * Configura um conjunto dos enderecos de email dos destinatarios.
	 * @param to Um conjunto de emails.
	 */
	public void setTo(String[] to) {
		if (!Validator.isEmpty(to)) {
			for (String email : to) {
				this.addTo(email);
			}
		}
	}
	
	/**
	 * Configura um conjunto dos enderecos de email dos destinatarios.
	 * @param to Um conjunto de emails.
	 */
	public void setTo(Collection<String> to) {
		if (!Validator.isEmpty(to)) {
			for (String email : to) {
				this.addTo(email);
			}
		}
	}
	
	/**
	 * Adiciona um endereco de email de destino.
	 * 
	 * @param to Um email de destino.
	 */
	public void addTo(String to) {
		if (Validator.isEmail(to)) {
			this.to.add(to);
		}
	}
	
	/**
	 * Remove um endereco de email de destino.
	 * 
	 * @param to Um email de destino.
	 */
	public void removeTo(String to) {
		if (Validator.isEmail(to)) {
			this.to.remove(to);
		}
	}

	/**
	 * Obtem o assunto do email.
	 * 
	 * @return O assunto do email.
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Configura um assunto do email.
	 * 
	 * @param subject O assunto do email.
	 */
	public void setSubject(String subject) {
		if (Validator.isValid(subject)) {
			this.subject = subject;
		}
	}

	/**
	 * Obtem a mensagem do email.
	 * 
	 * @return A mensagem do email.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Configura uma mensagem de email.
	 * 
	 * @param message A mensagem do email.
	 */
	public void setMessage(String message) {
		if (Validator.isValid(message)) {
			this.message = message;
		}
	}

	/**
	 * Obtem o conjunto imutavel dos arquivos anexos do email. 
	 * 
	 * @return O conjunto de arquivos anexos.
	 */
	public Set<File> getAttachments() {
		return Collections.unmodifiableSet(this.attachments);
	}

	/**
	 * Configura um conjunto imutavel dos arquivos anexos do email.
	 * 
	 * @param attachments O conjunto de arquivos anexos.
	 */
	public void setAttachments(Collection<File> attachments) {
		if (!Validator.isEmpty(attachments)) {
			this.attachments.addAll(attachments);
		}
	}
	
	/**
	 * Adiciona um arquivo como anexo ao email.
	 * 
	 * @param attachment Um arquivo anexo.
	 */
	public void addAttachment(File attachment) {
		if (Validator.isFile(attachment)) {
			this.attachments.add(attachment);
		}
	}
	
	/**
	 * Remove um arquivo como anexo ao email.
	 * 
	 * @param attachment Um arquivo anexo.
	 */
	public void removeAttachment(File attachment) {
		if (Validator.isFile(attachment)) {
			this.attachments.remove(attachment);
		}
	}

	@Override
	public String toString() {
		return Format.toString(this, "message");
	}
	
}
