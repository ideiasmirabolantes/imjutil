package com.im.imjutil.ftp;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Classe responsavel por prover um cliente de FTP, 
 * para acesso completo ao mesmo.
 * 
 * @author Felipe Zappala
 */
public interface FTPClient {
	
	/**
	 * Conecta o cliente a um servidor. 
	 * 
	 * @param domain Dominio ou IP do servidor.
	 * @param port Porta do servidor
	 * @param login Login de acesso.
	 * @param password Senha de acesso.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void connect(String domain, int port, String login, String password)
			throws IOException;
	
	/**
	 * Conecta o cliente a um servidor.
	 * 
	 * @param domain Dominio ou IP do servidor.
	 * @param login Login de acesso.
	 * @param password Senha de acesso.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void connect(String domain, String login, String password) throws IOException;
	
	/**
	 * Conecta o cliente a um servidor.
	 * 
	 * @param domain Dominio ou IP do servidor.
	 * @param port Porta do servidor
	 * @throws IOException caso ocorra algum erro.
	 */
	public void connect(String domain, int port) throws IOException;
	
	/**
	 * Conecta o cliente a um servidor.
	 * 
	 * @param domain Dominio ou IP do servidor.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void connect(String domain) throws IOException;
	
	/**
	 * Disconecta o cliente de um servidor.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void disconnect() throws IOException;
	
	/**
	 * Baixa um arquivo no diretorio corrente pelo nome.
	 * 
	 * @param name Nome do arquivo.
	 * @return O ponteiro para o arquivo.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void download(String name, File file) throws IOException;
	
	/**
	 * Baixa um arquivo no diretorio corrente pelo nome.
	 * 
	 * @param name Nome do arquivo.
	 * @return O stream de saida do arquivo.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void download(String name, OutputStream file) throws IOException; 
	
	/**
	 * Sobe um arquivo para o diretorio corrente.
	 * 
	 * @param file O ponteiro para o arquivo.
	 * @throws IOException caso ocorra algum erro. 
	 */
	public void upload(File file) throws IOException;
	
	/**
	 * Sobe um arquivo para o diretorio corrente.
	 * 
	 * @param name Nome do arquivo.
	 * @param file O stream de entrada do arquivo.
	 * @throws IOException caso ocorra algum erro.
	 */
	public void upload(String name, InputStream file) throws IOException;
	
	/**
	 * Lista os diretorios e arquivos do diretorio corrente.
	 * 
	 * @return A lista dos arquivos e diretorios do diretorio corrente.
	 * @throws IOException caso ocorra algum erro.
	 */
	public List<String> list() throws IOException;
	
	/**
	 * Acessa um diretorio pelo nome a partir do 
	 * diretorio corrente ou raiz.
	 * 
	 * @param name Nome do diretorio.
	 * @throws IOException caso ocorra algum erro.
	 */
	public boolean directory(String name) throws IOException;
	
	/**
	 * Renomeia o arquivo no diretorio corrente.
	 * 
	 * @param from O nome atual do arquivo.
	 * @param to O novo nome do arquivo.
	 * @return Verdadeiro se a operacao for realizada com sucesso.
	 * @throws IOException caso ocorra algum erro.
	 */
	public boolean rename(String from, String to) throws IOException;
	
}
