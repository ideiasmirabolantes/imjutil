package com.im.imjutil.ftp;

import static org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPReply;

import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Validator;


/**
 * Classe responsavel pela implemetacao da interface {@link FTPClient}
 * 
 * @author Ygor Fonseca
 */
public class SimpleFTPClient implements FTPClient {
	
	/** Objeto padrao de conexao FTP*/ 
	private org.apache.commons.net.ftp.FTPClient FTP;
	
	/** porta default de conexao com o servidor FTP */
	private final static int defaultPort = 21;
	
	/** login default de conexao com o servidor FTP */
	private final static String defaultLogin = "anonymous";
	
	/** senha default de conexao com o servidor FTP */
	private final static String defaultPassword = "anonymous";
	
	/**
	 * Construtor da Classe
	 */
	public SimpleFTPClient() {
		FTP = new org.apache.commons.net.ftp.FTPClient();
	}

	@Override
	public void connect(String domain, int port, String login, String password)
			throws IOException {
		try {
			// Conecta no servidor atraves do servidor "domain" na porta "port"
			FTP.connect(domain, port);

			// verifica se conectou com sucesso
			if (FTPReply.isPositiveCompletion(FTP.getReplyCode())) {
				FTP.login(login, password);
			} else {
				// erro ao se conectar
				FTP.disconnect();
				throw new IOException("Conexao Recusada.");
			}
		} catch (Exception e) {
			throw new IOException(Convert.toString(
					"Ocorreu um Erro: ", e.getMessage()), e);
		}
	}
	
	@Override
	public void connect(String domain, String login, String password) 
			throws IOException {
		connect(domain, defaultPort, login, password);
	}

	@Override
	public void connect(String domain, int port) throws IOException {
		connect(domain, defaultPort, defaultLogin, defaultPassword);
	}
	
	@Override
	public void connect(String domain) throws IOException {
		connect(domain, defaultPort, defaultLogin, defaultPassword);
	}

	@Override
	public boolean directory(String name) throws IOException {
		return FTP.changeWorkingDirectory(name);
	}

	@Override
	public void disconnect() throws IOException {
		FTP.disconnect();
	}

	@Override
	public void download(String name, File file) throws IOException {
		OutputStream os = new BufferedOutputStream(new FileOutputStream(file)) ;
		download(name, os);
		os.close();
	}

	@Override
	public void download(String name, OutputStream file) throws IOException {
		FTP.setFileType(BINARY_FILE_TYPE);  
		FTP.retrieveFile(name,file);
	}

	@Override
	public List<String> list() throws IOException {
		List<String> files = new ArrayList<String>();
		for (String filename : FTP.listNames()) {
			if(Validator.isValid(filename)){
				files.add(filename);
			}
		}
		return files;
	}

	@Override
	public void upload(File file) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(file)); 
		upload(file.getName(), is);
		is.close();	
	}
	
	@Override
	public void upload(String name, InputStream file) throws IOException {
		FTP.setFileType(BINARY_FILE_TYPE);
		FTP.storeFile(name, file);
	}

	@Override
	public boolean rename(String from, String to) throws IOException {
		return FTP.rename(from, to);
	}

}
