package com.im.imjutil.permission;

import com.im.imjutil.exception.PermissionException;

/**
 * Interface que define uma permissao de acesso do sistema.
 * <br> 
 * O metodo {@link #check(Object...)} verifica a permissao de acesso 
 * a um determinado contexto, caso for negado o acesso este lanca uma 
 * excecao do tipo {@link PermissionException} com o motivo da proibicao. 
 * Caso passar com sucesso, nada acontece. 
 * 
 * @author Felipe Zappala
 */
public interface Permission {

	/**
	 * Verifica a permissao de acesso a um determinado contexto, 
	 * lancando excecao caso nao exista a permissao para o contexto em questao.
	 *  
	 * @param args Argumentos para auxiliar a validacao. 
	 * @throws PermissionException caso nao contenha permissao.
	 */
	public void check(Object... args) throws PermissionException;
	
}
