package com.im.imjutil.session;

import java.util.Date;

/**
 * Interface que define uma sessao no sistema
 * <br>
 * Esta possui metodos para um controle de sessao, com um tempo de vida 
 * pre-determinado e permitindo a manipulacao de dados na mesma. 
 * 
 * @see {@link SessionManager}, {@link SimpleSession}
 *  
 * @author Felipe Zappala
 */
public interface Session {

	/**
	 * Configura o ID da sessao.
	 * 
	 * @param id O ID da sessao.
	 */
	public void setId(String id);
	
	/**
	 * Obtem o ID da sessao.
	 * 
	 * @return O ID da sessao.
	 */
	public String getId();
	
	/**
	 * Configura a data de expiracao da sessao.
	 * 
	 * @param date A data limite para expiracao.
	 */
	public void setExpirationDate(Date date);
	
	/**
	 * Obtem a data de expiracao da sessao.
	 * 
	 * @return A data limite para expiracao.
	 */
	public Date getExpirationDate();
	
	/**
	 * Verifica se a sessao e valida, ou seja, se o seu tempo 
	 * de expiracao ainda nao foi alcancado.
	 * 
	 * @return Verdadeiro caso o tempo de expiracao nao foi ultrapassado.
	 */
	public boolean isValid();
	
	/**
	 * Invalida a sessao, destuindo os seus dados e 
	 * estoura o seu tempo limite de vida.
	 */
	public void invalidate();
	
	/**
	 * Adiciona um dado na sessao. 
	 * 
	 * @param key O nome do dado adicionado.
	 * @param value O valor do dado adicionado.
	 */
	public void addData(String key, Object value);
	
	/**
	 * Remove um dado da sessao.
	 * 
	 * @param key O nome do dado a ser removido.
	 */
	public void removeData(String key);
	
	/**
	 * Obtem um dado da sessao.
	 * <p>
	 * ATENCAO: Este metodo aplica o cast automaticamente para o objeto de 
	 * retorno, contudo se o objeto criado nao for do mesmo tipo do esperado 
	 * no retorno, sera gerada uma excessao do tipo {@link ClassCastException}.
	 * </p>
	 * @param <T> O tido do dado retornado.
	 * @param key O nome do dado a ser obtido.
	 * @return O dado caso encontrado, caso contrario, {@code null}.
	 */
	public <T> T getData(String key);
	
}
