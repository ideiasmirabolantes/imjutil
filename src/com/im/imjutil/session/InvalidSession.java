package com.im.imjutil.session;

import java.util.Date;

class InvalidSession implements Session {

	private String id; 
	
	public InvalidSession(String id) {
		this.setId(id);
	}
	
	public InvalidSession() {
		this(null);
	}
	
	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public void setExpirationDate(Date date) {
		// Nada faz
	}

	@Override
	public Date getExpirationDate() {
		return new Date(0);
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public void invalidate() {
		// Nada faz
	}

	@Override
	public void addData(String key, Object value) {
		// Nada faz
	}

	@Override
	public void removeData(String key) {
		// Nada faz
	}

	@Override
	public <T> T getData(String key) {
		return null;
	}

}
