package com.im.imjutil.session;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;

/**
 * Classe que define uma implementacao simples para uma sessao 
 * especificada pela interface {@link Session}.
 * <br>
 * Esta pode armazenar dados de sessao em uma tabela de hash.
 * 
 * @author Felipe Zappala
 */
public class SimpleSession implements Session, Serializable {
	
	/** serialVersionUID */
	private static final long serialVersionUID = -9140341361875877561L;

	/** Mapa de dados da sessao */
	private Map<String, Object> data;
	
	/** Data de expiracao da sessao */
	private Date expiration;
	
	/** Identificados da sessao */
	private String id; 
	
	/**
	 * Construtor padrao para uma instancia {@link SimpleSession}.
	 */
	public SimpleSession() {
		this.data = new HashMap<String, Object>();
		this.expiration = new Date(0);
		this.id = "";
	}
	
	@Override
	public void addData(String key, Object value) {
		validateParam(key);
		validateParam(value);
		this.data.put(key, value);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T getData(String key) {
		validateParam(key);
		return (T) this.data.get(key);
	}

	@Override
	public Date getExpirationDate() {
		return this.expiration;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public void invalidate() {
		expiration = new Date(0);
	}

	@Override
	public boolean isValid() {
		return expiration.after(new Date());
	}

	@Override
	public void removeData(String key) {
		validateParam(key);
		data.remove(key);
	}

	@Override
	public void setExpirationDate(Date date) {
		validateParam(date);
		Date current = new Date();
		if (current.after(date)) {
			throw new ValidationException(Convert.toString("Data invalida [", 
					Format.toTimestamp(date), "] anterior a data corrente [", 
					Format.toTimestamp(current), "]"));
		}
		this.expiration = date;
	}

	@Override
	public void setId(String id) {
		validateParam(id);
		this.id = id;
	}
	
	/**
	 * Verifica se os parametros passados sao validos, ou seja, 
	 * nao nulos ou vazios. Caso forem invalidos, sera lancado uma excecao 
	 * nao verificado de validacao.
	 *  
	 * @param obj Os parametros a serem validados.
	 * @throws ValidationException A excessao lancada.
	 */
	protected static void validateParam(Object obj) 
			throws ValidationException {
		if (!Validator.isValid(obj)) {
			throw new ValidationException(
					"Parametro passado e nulo ou vazio");
		}
	} 

	@Override
	public String toString() {
		return Format.toString(this);
	}
	
}
