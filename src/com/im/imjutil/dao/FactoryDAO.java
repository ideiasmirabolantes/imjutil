package com.im.imjutil.dao;

import java.util.Map;

import javax.persistence.EntityManager;

import com.im.imjutil.dao.jpa.JPADAO;
import com.im.imjutil.validation.Validator;

/**
 * Classe fabrica responsavel por construir uma instancia concreta 
 * da interface {@link DAO} baseado no tipo {@link TypeDAO} passado
 * 
 * @author Felipe Zappala
 */
public final class FactoryDAO {
	
	/** Proibindo a instanciacao. */
	private FactoryDAO() {
		super();
	}

	/**
	 * Cria um {@link DAO} de acordo com o tipo {@link TypeDAO} passado
	 * 
	 * @param type O tipo do DAO
	 * @param params Array de parametros do tipo {@link Object}
	 * @return Um instancia concreta de {@link DAO}
	 */
	@SuppressWarnings("unchecked")
	public static <T> DAO<T> createDAO(TypeDAO type, Class<T> clazz, 
			Object...params) {
		switch (type) {
			case JPA:
				if (!Validator.isEmpty(params)) {
					if (params.length == 1) {
						return new JPADAO<T>(clazz, (EntityManager) params[0]);
						
					}
					return new JPADAO<T>(clazz, (EntityManager) params[0],
							(Map<String, String>) params[1]);
				}
				return new JPADAO<T>(clazz);
		
			case XML:
				throw new UnsupportedOperationException("Not implemented!");
				
			case JDBC:
				throw new UnsupportedOperationException("Not implemented!");
				
			default:
				throw new UnsupportedOperationException("Not implemented!");
		}
	}
	
}
