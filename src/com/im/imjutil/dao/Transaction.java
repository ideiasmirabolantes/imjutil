package com.im.imjutil.dao;

import com.im.imjutil.exception.PersistenceException;

/**
 * Interface que define o controle manual das transacoes 
 * a serem executadas no {@link DAO} de uma entidade.
 * Caso este nao for invocado, o DAO deve controlar as 
 * transacoes automaticamente e individualmente por operacao.
 * 
 * @author Felipe Zappala
 */
public interface Transaction {
	
	/**
	 * Inicia uma transacao no {@link DAO}.
	 * 
	 * @throws PersistenceException Caso ocorra algum erro.
	 */
	public void begin() throws PersistenceException;
	
	/**
	 * Finaliza uma transacao no {@link DAO}.
	 * 
	 * @throws PersistenceException Caso ocorra algum erro.
	 */
	public void commit() throws PersistenceException;
	
	/**
	 * Desfaz uma transacao no {@link DAO}.
	 * 
	 * @throws PersistenceException Caso ocorra algum erro.
	 */
	public void rollback() throws PersistenceException;
	
	/**
	 * Verifica se uma transacao no {@link DAO} ainda se encontra ativa.
	 * 
	 * @return Retorna verdadeiro quando ativo.
	 * @throws PersistenceException Caso ocorra algum erro.
	 */
	public boolean isActive() throws PersistenceException;
	
}
