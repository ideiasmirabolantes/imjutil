package com.im.imjutil.dao.jpa;

import javax.persistence.EntityTransaction;

import com.im.imjutil.dao.Transaction;
import com.im.imjutil.exception.PersistenceException;

class JPATransactionAdapter implements Transaction {

	private EntityTransaction transaction;
	
	public JPATransactionAdapter(EntityTransaction transaction) 
			throws PersistenceException {
		if (transaction == null)
			throw new PersistenceException("EntityTransaction is null");
		
		this.transaction = transaction; 
	}
	
	@Override
	public void begin() throws PersistenceException {
		try {
			transaction.begin();
		} catch (Exception e) {
			throw new PersistenceException(e.getMessage(), e);
		}
	}

	@Override
	public void commit() throws PersistenceException {
		try {
			transaction.commit();
		} catch (Exception e) {
			throw new PersistenceException(e.getMessage(), e);
		}
	}

	@Override
	public void rollback() throws PersistenceException {
		try {
			transaction.rollback();
		} catch (Exception e) {
			throw new PersistenceException(e.getMessage(), e);
		}
	}

	@Override
	public boolean isActive() throws PersistenceException {
		try {
			return transaction.isActive();
		} catch (Exception e) {
			throw new PersistenceException(e.getMessage(), e);
		}
	}

	public EntityTransaction getTransaction() {
		return transaction;
	}

	public void setTransaction(EntityTransaction transaction) {
		this.transaction = transaction;
	}

}
