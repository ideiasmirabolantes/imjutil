package com.im.imjutil.dao;

/**
 * Determina os possiveis tipo de fabrica de DAO existentes.
 * 
 * @author Felipe Zappala.
 */
public enum TypeDAO {
	
	/** Determina a fabrica de DAO como uma fabrica JDBC. */
	JDBC,
	
	/** Determina a fabrica de DAO como uma fabrica JPA. */
	JPA,
	
	/** Determina a fabrica de DAO como uma fabrica XML. */
	XML;
	
}
