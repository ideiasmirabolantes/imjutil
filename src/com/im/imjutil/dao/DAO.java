package com.im.imjutil.dao;

import java.util.List;

import com.im.imjutil.dao.jpa.JPADAO;
import com.im.imjutil.exception.PersistenceException;
import com.im.imjutil.query.Query;
import com.im.imjutil.util.Filter;

/**
 * Interface que define um DAO do sistema. O DAO dispoe de metodos basicos 
 * para persistencia de um modelo do sistema. 
 * @see {@link JPADAO}
 * 
 * @param <T> Tipo do modelo do sistema.
 * 
 * @author Felipe Zappala
 */
public interface DAO<T> {

	/**
	 * ID do tipo padrao do sistema.
	 */
	public static final long DEFAULT_ID = 1;
	
	/**
	 * Obtem uma lista de todos os tipos T.
	 * 
	 * @return Uma lista de todos os tipos T.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public List<T> getAll() throws PersistenceException;
	
	/**
	 * Obtem um tipo T do sistema.
	 * 
	 * @param O ID do tipo T.
	 * @return Um tipo T do sistema.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public T get(Object id) throws PersistenceException;
	
	/**
	 * Obtem uma lista de tipo T com base no filtro passado.
	 * 
	 * @param filter O filtro a ser aplicado na busca.
	 * @return Uma lista de todos os tipo T encontrados.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public List<T> findAll(Filter filter) throws PersistenceException;
	
	/**
	 * Obtem um tipo T com base no filtro passado.
	 * 
	 * @param filter O filtro a ser aplicado na busca.
	 * @return O tipo T encontrado nulo.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public T find(Filter filter) throws PersistenceException;
	
	/**
	 * Adiciona ou atualiza um tipo T do sistema.
	 * 
	 * @param type Um tipo T do sistema.
	 * @return Uma lista de todos os tipo T encontrados.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public T add(T type) throws PersistenceException;
	
	/**
	 * Adiciona ou atualiza uma lista de tipo T do sistema.
	 * 
	 * @param types Lista de tipo T do sistema.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public void addAll(List<T> types) throws PersistenceException;
	
	/**
	 * Remove um tipo T do sistema.
	 * 
	 * @param type Um tipo T do sistema.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public void remove(T type) throws PersistenceException;
	
	/**
	 * Remove uma lista de tipo T do sistema.
	 * 
	 * @param types Lista de tipo T do sistema.
	 * @throws PersistenceException Caso ocorra algum erro ao persistir.
	 */
	public void removeAll(List<T> types) throws PersistenceException;

	/**
	 * Executa uma consulta personalizada e obtem o seu unico resultado.
	 * 
	 * @param <R> Tipo do retorno da execucao da consulta.
	 * @param query A consulta personalizada.
	 * @param filters Os filtros opcionais da consulta.
	 * @return O resultado da execucao. 
	 * @throws PersistenceException Caso ocorra algum erro ao consultar.
	 */
	public <R> R execute(Query query, Filter... filters) 
			throws PersistenceException;
	
	/**
	 * Executa uma consulta personalizada e recebe todos os seus resultados.
	 * 
	 * @param <R> Tipo do retorno da execucao da consulta.
	 * @param query A consulta personalizada.
	 * @param filters Os filtros opcionais da consulta.
	 * @return A lista dos resultados da execucao. 
	 * @throws PersistenceException Caso ocorra algum erro ao consultar.
	 */
	public <R> List<R> executeAll(Query query, Filter... filters) 
			throws PersistenceException;
	
	/**
	 * Retorna a transacao para controle manual
	 * @return a transacao corrente a ser retornada.
	 */
	public Transaction transaction();
	
}
