package com.im.imjutil.maps;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Armazena os dados de um local alimentado com dados do google maps
 * 
 * @author Bruno Alcantara
 */
public class Local {

	private String address;
	private String thoroughfare;
	private String district;
	private String country;
	private String state;
	private String city;
	private String zipCode;
	private double latitude;
	private double longitude;
	
	public Local() {
		this.address = "";
		this.thoroughfare = "";
		this.district = "";
		this.country = "";
		this.state = "";
		this.city = "";
		this.zipCode = "";
	}

	public Local(JSONObject json) {
		feedFromJSON(json);
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if (address != null) {
			this.address = address;
		}
	}

	public String getThoroughfare() {
		return thoroughfare;
	}

	public void setThoroughfare(String thoroughfare) {
		if (thoroughfare != null) {
			this.thoroughfare = thoroughfare;
		}
	}
	
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		if (district != null) {
			this.district = district;
		}
	}
	
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		if (country != null) {
			this.country = country;
		}
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		if (state != null) {
			this.state = state;
		}
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		if (city != null) {
			this.city = city;
		}
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		if (zipCode != null) {
			this.zipCode = zipCode;
		}
	}
	
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public void feedFromJSON(JSONObject json) {
		JSONArray placemark = (JSONArray) json.get("Placemark");
		JSONObject data = (JSONObject) placemark.get(0);

		this.address = (String) data.get("address");

		JSONObject addressDetailsJson = (JSONObject) data.get("AddressDetails");
		JSONObject countryJson = (JSONObject) addressDetailsJson.get("Country");
		this.country = (String) countryJson.get("CountryName");
		
		JSONObject administrativeAreaJson = (JSONObject) countryJson.get("AdministrativeArea");
		this.state = (String) administrativeAreaJson.get("AdministrativeAreaName");
		
		JSONObject localityJson = (JSONObject) administrativeAreaJson.get("Locality");
		this.city = (String) localityJson.get("LocalityName");

		JSONObject dependentLocalityJson = (JSONObject) localityJson.get("DependentLocality");
		
		if (dependentLocalityJson != null) {
			this.district = (String) dependentLocalityJson.get("DependentLocalityName");
			
			JSONObject postalCodeJson = (JSONObject) dependentLocalityJson.get("PostalCode");
			this.zipCode = (String) postalCodeJson.get("PostalCodeNumber");
			
			JSONObject ThoroughfareJson = (JSONObject) dependentLocalityJson.get("Thoroughfare");
			this.thoroughfare = (String) ThoroughfareJson.get("ThoroughfareName");
		} else {
			JSONObject postalCodeJson = (JSONObject) localityJson.get("PostalCode");
			this.zipCode = (String) postalCodeJson.get("PostalCodeNumber");
			
			JSONObject ThoroughfareJson = (JSONObject) localityJson.get("Thoroughfare");
			this.thoroughfare = (String) ThoroughfareJson.get("ThoroughfareName");
		}
		
		JSONObject poitnJson = (JSONObject) data.get("Point");
		JSONArray coordinates = (JSONArray) poitnJson.get("coordinates");
		this.latitude = (Double) coordinates.get(1);
		this.longitude = (Double) coordinates.get(0);
	}

}
