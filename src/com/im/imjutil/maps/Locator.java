package com.im.imjutil.maps;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.im.imjutil.config.Configurator;
import com.im.imjutil.util.Util;
import com.im.imjutil.validation.Convert;

/**
 * Classe utilitaria para o google maps
 * 
 * @author Bruno Alcantara
 */
public class Locator {

	private static String GOOGLE_MAPS_BASE_URL;
	private static Map<String, String> BRAZIL_STATES_CODES;
	
	static {
		GOOGLE_MAPS_BASE_URL = "http://maps.google.com/maps/geo?";
		BRAZIL_STATES_CODES = new HashMap<String, String>();
		feedBrazilStateCodes();
	}
	
	
	
	/**
	 * Este metodo esta sendo implementado para corrigir o erro do google maps
	 * que retorna o nome de alguns estados por extenso ao inves da sigla do
	 * estado no Brasil
	 */
	private static void feedBrazilStateCodes() {
		BRAZIL_STATES_CODES.put("Acre", "AC");
		BRAZIL_STATES_CODES.put("Alagoas", "AL");
		BRAZIL_STATES_CODES.put("Amap�", "AP");
		BRAZIL_STATES_CODES.put("Amazonas", "AM");
		BRAZIL_STATES_CODES.put("Bahia", "BA");
		BRAZIL_STATES_CODES.put("Cear�", "CE");
		BRAZIL_STATES_CODES.put("Distrito Federal", "DF");
		BRAZIL_STATES_CODES.put("Esp�rito Santo", "ES");
		BRAZIL_STATES_CODES.put("Goi�s", "GO");
		BRAZIL_STATES_CODES.put("Maranh�o", "MA");
		BRAZIL_STATES_CODES.put("Mato Grosso", "MT");
		BRAZIL_STATES_CODES.put("Mato Grosso do Sul", "MS");
		BRAZIL_STATES_CODES.put("Minas Gerais", "MG");
		BRAZIL_STATES_CODES.put("Par�", "PA");
		BRAZIL_STATES_CODES.put("Para�ba", "PB");
		BRAZIL_STATES_CODES.put("Paran�", "PR");
		BRAZIL_STATES_CODES.put("Pernambuco", "PE");
		BRAZIL_STATES_CODES.put("Piau�", "PI");
		BRAZIL_STATES_CODES.put("Rio de Janeiro", "RJ");
		BRAZIL_STATES_CODES.put("Rio Grande do Norte", "RN");
		BRAZIL_STATES_CODES.put("Rio Grande do Sul", "RS");
		BRAZIL_STATES_CODES.put("Rond�nia", "RO");
		BRAZIL_STATES_CODES.put("Roraima", "RR");
		BRAZIL_STATES_CODES.put("Santa Catarina", "SC");
		BRAZIL_STATES_CODES.put("S�o Paulo", "SP");
		BRAZIL_STATES_CODES.put("Sergipe", "SE");
		BRAZIL_STATES_CODES.put("Tocantins", "TO");
	}
	
	public static String getStateCode(String stateName) {
		return BRAZIL_STATES_CODES.get(stateName);
	}
	
	public static Local findLocal(double latitude, double longitude) {
		Local local = null;
		
		String apiKey = Configurator.get("google.maps.apikey");
		
		String url = Convert.toString(GOOGLE_MAPS_BASE_URL, "ll=", latitude,
				",", longitude, "&output=json&sensor=false&key=", apiKey);
		
		try {
			String json = Util.HTTPConnection(url);
			JSONObject result = (JSONObject) JSONValue.parse(json);
			JSONObject status = (JSONObject) result.get("Status");
			long statusCode = (Long) status.get("code");
			
			if (statusCode == 200) {
				local = new Local(result);
			}
		} catch (Exception e) {
			// Nada faz...
		}
		
		return local;
	}
	
}
