package com.im.imjutil.report.csv;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.im.imjutil.logging.Logger;

/**
 * Classe responsavel pela construcao de arquivos CSV, tanto para 
 * escrita quanto para leitura. 
 * <br>
 * Use os metodos {@link #writeCSV()} para escrever os arquivos CSV.
 * Use os metodos {@link #readCSV()} para ler os arquivos CSV.
 * <p>
 * O arquivo de saida esta no padrao: <br>
 * - Elementos envolvidos por aspas duplas (").<br>
 * - Elementos separados por virgula (,).<br>
 * - Elementos em linhas separadas por caracter de nova linha (\n).<br>
 * </p>
 * 
 * @author Felipe Zappala
 */
public final class CSVMaker {
	
	/** Separador que envolve o elemento. */
	private static final String QUOTE;
	/** Separador entre elementos. */
	private static final String COMMA;
	/** Separador de linhas dos elementos. */
	private static final String LINE;
	static {
		QUOTE = "\"";
		COMMA = ","; 
		LINE  = "\n";
	}
	
	/** Proibe a instanciacao */
	private CSVMaker() {
		super();
	}
	
	/**
	 * Cria um relatorio CSV a partir de uma lista de {@link CSVElement}
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @return A String contendo o relatorio CSV. 
	 */
	public static String writeCSV(List<CSVElement> elements, char caracter) {
		StringBuilder sb = new StringBuilder();
		char SEMICOLON = caracter;
		
		if (elements != null && !elements.isEmpty()) {
			for (CSVElement e : elements) {
				for(String s : e) {
					sb.append(s).append(SEMICOLON);
				}
				sb.setLength(sb.length() -1);
				sb.append(LINE);
			}
		}
		return sb.toString();
	}
	
	/**
	 * Cria um relatorio CSV a partir de uma lista de {@link CSVElement}
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @return A String contendo o relatorio CSV. 
	 */
	public static String writeCSV(List<CSVElement> elements) {
		StringBuilder sb = new StringBuilder();
		
		if (elements != null && !elements.isEmpty()) {
			for (CSVElement e : elements) {
				for(String s : e) {
					sb.append(QUOTE).append(s).append(QUOTE).append(COMMA);
				}
				sb.setLength(sb.length() -1);
				sb.append(LINE);
			}
		}
		return sb.toString();
	}

	/**
	 * Cria um arquivo CSV a partir de uma lista de {@link CSVElement} 
	 * e grava o retorno no {@link OutputStream} passado.
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @param out Stream de saida a gravar os dados.
	 * @throws IOException Caso ocorra algum erro na gravacao.
	 */
	public static void writeCSV(List<CSVElement> elements, OutputStream out) 
			throws IOException {
		if (out != null && elements != null && !elements.isEmpty()) {
			out.write(writeCSV(elements).getBytes());
		}
	}
	
	/**
	 * Cria um arquivo CSV a partir de uma lista de {@link CSVElement} 
	 * e grava o retorno no {@link File} passado.
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @param file Arquivo de saida a gravar os dados.
	 * @throws IOException Caso ocorra algum erro na gravacao.
	 */
	public static void writeCSV(List<CSVElement> elements, File file) 
			throws IOException {
		if (file != null && elements != null && !elements.isEmpty()) {
			BufferedOutputStream out = new BufferedOutputStream(
					new FileOutputStream(file));
			out.write(writeCSV(elements).getBytes());
		}
	}
	
	/**
	 * Cria uma lista de {@link CSVElement} a partir de um arquivo CSV.
	 * Separador padao e a virgula (,).
	 * 
	 * @param file Nome {@link String} do arquivo CSV.
	 * @return Lista de elementos do csv.
	 */
	public static List<CSVElement> readCSV(String file) {
		return readCSV(file, ',');
	}
	
	/**
	 * Cria uma lista de {@link CSVElement} a partir de um arquivo CSV.
	 * Separador padao e a virgula (,).
	 * 
	 * @param file Ponteiro {@link File} do arquivo CSV.
	 * @return Lista de elementos do csv.
	 */
	public static List<CSVElement> readCSV(File file) {
		return readCSV(file, ',');
	}
	
	/**
	 * Cria uma lista de {@link CSVElement} a partir de um arquivo CSV.
	 * Separador padao e a virgula (,).
	 * 
	 * @param file Fluxo {@link InputStream} do arquivo CSV.
	 * @return Lista de elementos do csv.
	 */
	public static List<CSVElement> readCSV(InputStream file) {
		return readCSV(file, ',');
	}
	
	/**
	 * Cria uma lista de {@link CSVElement} a partir de um arquivo CSV.
	 * 
	 * @param file Nome {@link String} do arquivo CSV.
	 * @param separator Separador utilizado no arquivo.
	 * @return Lista de elementos do csv.
	 */
	public static List<CSVElement> readCSV(String file, char separator) {
		try {
			return readCSV(new File(file), separator);
			
		} catch (Exception e) {
			Logger.error("[CSVMaker] readCSV: " + file, e);
			
			return new ArrayList<CSVElement>(0);
		}
	}
	
	/**
	 * Cria uma lista de {@link CSVElement} a partir de um arquivo CSV.
	 * 
	 * @param file Ponteiro {@link File} do arquivo CSV.
	 * @param separator Separador utilizado no arquivo.
	 * @return Lista de elementos do csv.
	 */
	public static List<CSVElement> readCSV(File file, char separator) {
		try {
			InputStream in = new BufferedInputStream(new FileInputStream(file));
			return readCSV(in, separator);
			
		} catch (Exception e) {
			Logger.error("[CSVMaker] readCSV: " + file, e);
			
			return new ArrayList<CSVElement>(0);
		}
	}
	
	/**
	 * Cria uma lista de {@link CSVElement} a partir de um arquivo CSV.
	 * 
	 * @param file Fluxo {@link InputStream} do arquivo CSV.
	 * @param separator Separador utilizado no arquivo.
	 * @return Lista de elementos do csv.
	 */
	public static List<CSVElement> readCSV(InputStream file, char separator) {
		try {
			List<CSVElement> elements = new ArrayList<CSVElement>();
			
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(file)); 

			CSVElement element; 
			String line;
			
			while (reader.ready()) {
				line = reader.readLine();
				element = new CSVElement();
				
				for (String column : line.split(String.valueOf(separator))) {
					element.add(column.replaceAll("\"", ""));
				}
				if (!element.isEmpty()) {
					elements.add(element);
				}
			}
			reader.close();
			
			return elements;
			
		} catch (Exception e) {
			Logger.error("[CSVMaker] readCSV: Erro ao ler arquivo.", e);
			
			return new ArrayList<CSVElement>(0);
		}
	}

	
	//
	// Metodos para retrocompatibilidade
	//
	
	/**
	 * Cria um relatorio CSV a partir de uma lista de {@link CSVElement}
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @return A String contendo o relatorio CSV.
	 * @deprecated Usar o metodo {@link #writeCSV()}.
	 */
	@SuppressWarnings("dep-ann")
	public static String createCSV(List<CSVElement> elements) {
		return writeCSV(elements);
	}
	
	/**
	 * Cria um arquivo CSV a partir de uma lista de {@link CSVElement} 
	 * e grava o retorno no {@link File} passado.
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @param file Arquivo de saida a gravar os dados.
	 * @throws IOException Caso ocorra algum erro na gravacao.
	 * @deprecated Usar o metodo {@link #writeCSV()}.
	 */
	@SuppressWarnings("dep-ann")
	public static void createCSV(List<CSVElement> elements, File file) 
			throws IOException {
		writeCSV(elements, file);
	}
	
	/**
	 * Cria um arquivo CSV a partir de uma lista de {@link CSVElement} 
	 * e grava o retorno no {@link OutputStream} passado.
	 * 
	 * @param elements Lista de elementos do CSV.
	 * @param out Stream de saida a gravar os dados.
	 * @throws IOException Caso ocorra algum erro na gravacao.
	 * @deprecated Usar o metodo {@link #writeCSV()}.
	 */
	@SuppressWarnings("dep-ann")
	public static void createCSV(List<CSVElement> elements, OutputStream out) 
			throws IOException {
		writeCSV(elements, out);
	}
}
