package com.im.imjutil.report.csv;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Format;
import com.im.imjutil.validation.Validator;

/**
 * Classe que define um elemento no arquivo CSV.<br>
 * Um elemento determina uma linha do CSV e cada objeto adicionado
 * determina uma celula da linha.<br>
 * Para a construcao do arquivo use o objeto {@link CSVMaker} ou {@link CSVFile} 
 * 
 * @author Felipe Zappala
 */
public class CSVElement implements Iterable<String>, Comparable<CSVElement> {
	
	/** Lista de elementos do CSV*/
	private List<String> elements;

	/**
	 * Constroi um elemento do CSV
	 */
	public CSVElement() {
		init();
	}
	
	/**
	 * Constroi um elemento do CSV passando uma celula inicial.
	 * 
	 * @param element Um objeto para a primeira celula do elemento.
	 */
	public CSVElement(Object element) {
		init();
		this.setElement(element);
	}
	
	/**
	 * Inicializa o elemento.
	 */
	private void init() {
		this.elements = new ArrayList<String>();
	}

	/**
	 * Adiciona objetos as celulas do elemento.
	 * 
	 * @param element Um objeto convertivel para texto. 
	 * @return A referencia para o mesmo objeto.
	 */
	public CSVElement add(Object element) {
		set(element);
		return this;
	}
	
	public CSVElement addAll(Collection<? extends String> elements) {
		if (!Validator.isEmpty(elements)) {
			this.elements.addAll(elements);
		}
		return this;
	}
	
	/**
	 * Adiciona objetos as celulas do elemento.
	 * 
	 * @param element Um objeto convertivel para texto.
	 */
	public void setElement(Object element) {
		set(element);
	}
	
	/**
	 * Adiciona objetos as celulas do elemento.
	 * 
	 * @param element Um objeto convertivel para texto.
	 */
	public void setElement(int index, Object element) {
		set(index, element);
	}
	
	/**
	 * Adiciona objetos as celulas do elemento.
	 * 
	 * @param element Um objeto convertivel para texto.
	 */
	public void set(Object element) {
		if (element != null) {
			this.elements.add(Convert.toString(element));
		} else {
			this.elements.add("");
		}
	}
	
	/**
	 * Adiciona objetos as celulas do elemento.
	 * 
	 * @param element Um objeto convertivel para texto.
	 */
	public void set(int index, Object element) {
		if (index >= 0) {
			if (element != null) {
				this.elements.set(index, Convert.toString(element));
			} else {
				this.elements.set(index, "");
			}
		}
	}
	
	/**
	 * Adiciona objetos as celulas do elemento.
	 * 
	 * @param element Um objeto convertivel para texto.
	 */
	public void setCelule(int index, Object element) {
		set(index, element);
	}

	/**
	 * Obtem um objeto do elemento pelo indice.
	 * 
	 * @param index Indice do elemento.
	 * @return O texto da celula
	 */
	public String getElement(int index) {
		return get(index);
	}
	
	public String getCelule(int index) {
		return get(index);
	}
	
	/**
	 * Obtem um objeto da celula pelo indice.
	 * 
	 * @param index Indice da celula.
	 * @return O texto da celula
	 */
	public String get(int index) {
		if (index >= 0 && index < this.elements.size()) {
			return this.elements.get(index);			
		}
		return "";
	}
	
	/**
	 * Obtem um iterador para as celulas do elemento.
	 */
	@Override
	public Iterator<String> iterator() {
		return this.elements.iterator();
	}

	/**
	 * Numero de linha do csv.
	 * 
	 * @return Retorna o tamanho da lista de itens.
	 */
	public int size() {
		return elements.size();
	}
	
	/**
	 * Verifica se o elemento e vazio, ou seja, nao possui colunas.
	 *  
	 * @return Verdadeiro quando for vazio.
	 */
	public boolean isEmpty() {
		boolean isEmpty = true;
		for (String column : this.elements) {
			isEmpty &= column.isEmpty();
		}
		return isEmpty;
	}
	
	@Override
	public int hashCode() {
		return this.elements.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CSVElement) {
			CSVElement another = (CSVElement) obj;
			return this.elements.equals(another.elements);
		}
		return false;
	}

	@Override
	public int compareTo(CSVElement o) {
		if (o != null) {
			return this.elements.toString().compareTo(o.elements.toString());
		}
		return 1;
	}
	
	@Override
	public String toString() {
		return Format.toString(this);
	}
}
