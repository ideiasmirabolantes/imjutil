package com.im.imjutil.report.csv;

import com.im.imjutil.validation.Convert;

/**
 * Classe que define o padrao de formatacao do CSV.<br>
 * Esta define qual sera o tipo de o container de celula, 
 * o separador de celula e o separador de linha.
 *  
 * @author Felipe Zappala
 */
public class CSVPattern {
	
	/** Define o padrao de formatacao do CSV como o padrao da especificacao. */
	public static final CSVPattern DEFAULT;
	/** Define o padrao de formatacao do CSV como o padrao do Excel. */
	public static final CSVPattern EXCEL;
	/** Define o padrao de formatacao do CSV como sem nenhum separador. */
	public static final CSVPattern NONE;
	
	static {
		DEFAULT = create("\"", ",", "\n");
		EXCEL = create("", ";", "\n");
		NONE = create("", "", "\n");
	}
	
	
	/** Define o separador que envolve a celula do elemento CSV. */
	public final String QUOTE;
	/** Define o separador entre as celulas do elemento CSV. */
	public final String COMMA;
	/** Define o separador de linhas do elemento CSV. */
	public final String LINE;
	
	/**
	 * Constroi um novo padrao de formatacao da celula do elemento CSV.
	 * 
	 * @param quote O separador que envolve a celula. 
	 * @param comma O separador entre as celulas. 
	 * @param line O separador de linhas.
	 */
	public CSVPattern(String quote, String comma, String line) {
		this.QUOTE = quote;
		this.COMMA = comma;
		this.LINE = line;
	}
	
	/**
	 * Cria um novo padrao de formatacao da celula do elemento CSV.
	 * 
	 * @param quote O separador que envolve a celula. 
	 * @param comma O separador entre as celulas. 
	 * @param line O separador de linhas.
	 * @return O padrao {@link CSVPattern} criado.
	 */
	public static CSVPattern create(String quote, String comma, String line) {
		return new CSVPattern(quote, comma, line); 
	}
	
	/**
	 * Aplica o padrao na celula passada.
	 * 
	 * @param pattern O padrao a ser aplicado.
	 * @param celule A celula do elemento CSV.
	 * @return A celula do elemento CSV com o padrao aplicado.
	 */
	public static String apply(CSVPattern pattern, String celule) {
		return Convert.toString(
				pattern.QUOTE, celule, pattern.QUOTE, pattern.COMMA
		);
	}
	
	/**
	 * Aplica o padrao na celula passada. 
	 * 
	 * @param celule A celula do elemento CSV.
	 * @return A celula do elemento CSV com o padrao aplicado.
	 */
	public String apply(String celule) {
		return apply(this, celule);
	}
	
}
