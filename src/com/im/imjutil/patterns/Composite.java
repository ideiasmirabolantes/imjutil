package com.im.imjutil.patterns;

import java.util.Iterator;

/**
 * Interface que representa um objeto que e constituido pela 
 * composicao de objetos similares a ele. O objeto composto possui 
 * um conjunto de outros objetos que estao na mesma hierarquia de 
 * classes a que ele pertence. Normalmente utilizado para representar 
 * listas recorrentes ou recursivas de elementos. Ele permite que os 
 * elementos contidos em um objeto composto sejam tratados como se 
 * fossem um único objeto. 
 * Definicao: Wikipedia. 
 * <p>
 * <b>Padrao de Projeto:</b> Composite
 * </p>
 * @author Felipe Zappala
 */
public interface Composite<T extends Composite<?>> extends Iterable<T> {

	/**
	 * Adiciona uma composicao a composicao atual.
	 * 
	 * @param composite Uma composicao do tipo {@link Composite}.
	 */
	public void add(T composite);
	
	/**
	 * Remove uma composicao a composicao atual.
	 * 
	 * @param composite Uma composicao do tipo {@link Composite}.
	 */
	public void remove(T composite);

	/**
	 * Verifica se a composicao atual e realmente composta, ou seja, 
	 * possui outras composicoes {@link Composite} anexadas a atual.
	 * 
	 * @return Verdade se a composicao atual esta composta de outras.
	 */
	public boolean isComposite();
	
	/**
	 * Obtem o iterador da composicao atual, o qual permite navegar 
	 * pelos compostos da composicao.
	 * 
	 * @return O iterador da composicao.
	 */
	@Override
	public Iterator<T> iterator();
	
}
