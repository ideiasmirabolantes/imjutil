package com.im.imjutil.web.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.im.imjutil.exception.PermissionException;
import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.logging.Logger;
import com.im.imjutil.util.Util;
import com.im.imjutil.validation.Validator;

/**
 * Classe filtro de requisicao responsavel por validar o acesso 
 * a uma URL para a regra atualmente em sessao. Caso a regra nao
 * for encontrada acesso ou nao confirmar a posse da URL o acesso
 * sera negado. 
 * 
 * @author Felipe Zappala
 */
public class AccessControl implements javax.servlet.Filter {

	private String MSG_TEXT;
	private String URL_FAIL;
	private String SESSION_KEY;
	private String MSG_KEY;
	private List<String> URL_BYPASS;
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		SESSION_KEY = config.getInitParameter("sessionKey"); 
		MSG_KEY = config.getInitParameter("requestKey"); 
		MSG_TEXT = config.getInitParameter("errorMessage");
		URL_FAIL = config.getInitParameter("urlRedirect");  
		URL_BYPASS = Util.parseCSV(config.getInitParameter("urlsBypass"));
		
		if (!Validator.isValid(SESSION_KEY, MSG_KEY, MSG_TEXT, URL_FAIL)) {
			throw new ValidationException("Parametros nao configurados");
		}
	}
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		String url = request.getServletPath();
		Rule rule = null;
		try {
			rule = (Rule) request.getSession().getAttribute(SESSION_KEY);
			
			check(rule, url);	
			chain.doFilter(req, resp);	
		} catch (Exception e) {
			Logger.error(e, "AccessControl  rule:[",rule,"] access:[",url,"]");
			request.setAttribute(MSG_KEY, MSG_TEXT);
			request.getRequestDispatcher(URL_FAIL).forward(req, resp);
		}
	}

	/**
	 * Verifica a permissao de acesso de um usuario a uma url 
	 */
	private void check(Rule rule, String urlAccess) {
		// Testa se a URL nao esta sobre o controle de acesso
		for (String bypass : URL_BYPASS) {
			if (bypass.equals(urlAccess)) {
				return;
			}
		}
		
		// Testa se a regra nao esta em sessao
		if (rule == null) {
			throw new PermissionException(MSG_TEXT);
		}
		
		// Testa se a regra possui acesso a url requisitada
		if (!rule.containsRule(urlAccess)) {
			throw new PermissionException(MSG_TEXT);
		}
		
	}

	@Override
	public void destroy() {
		// Nada fazer
	}	
	
}
