package com.im.imjutil.web.filter;

/**
 * Interface que define a regra de acesso a um ou varios recursos.
 *  
 * @author Felipe Zappala
 */
public interface Rule {

	/**
	 * Verifica se a regra possui acesso ao recurso passado. 
	 */
	public boolean containsRule(String resource);
	
	/**
	 * Adiciona um recurso na regra. 
	 */
	public void addRule(String resource);
	
	/**
	 * Remove um recurso da regra. 
	 */
	public void removeRule(String resource);
	
}
