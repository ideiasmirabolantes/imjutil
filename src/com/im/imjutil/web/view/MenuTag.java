package com.im.imjutil.web.view;

import java.text.MessageFormat;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.im.imjutil.validation.Convert;

/**
 * Classe Tag responsavel pela montagem do componente visual de menu.
 * 
 * @author Felipe Zappala
 */
public class MenuTag extends TagSupport {

	private static final long serialVersionUID = 449617296126269598L;
	
	
	private static final String htmlElementCB;
	private static final String htmlElementAH;
	private static final String htmlElementLI;
	private static final String htmlElementUL;
	static {
		htmlElementCB = "<label title=\"{2}\">"
					  + "<input type=\"checkbox\" name=\"checkbox\" value=\"{1}\" />{0}</label>";
		htmlElementAH = "<a href=\"{3}{1}\" title=\"{2}\">{0}</a>";
		htmlElementLI = "<li>{0}</li>";
		htmlElementUL = "<ul class=\"{1}\">{0}</ul>";
	}
	
	private List<? extends MenuItem> menuList;
	private String cssClassMenu = "";
	private String cssClassSubMenu = "";
	private String menuType;
	private String Context;
	
	public String getContext() {
		return Context;
	}

	public void setContext(String context) {
		Context = context;
	}

	public List<? extends MenuItem> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<? extends MenuItem> menuList) {
		this.menuList = menuList;
	}

	public String getCssClassMenu() {
		return cssClassMenu;
	}

	public void setCssClassMenu(String cssClassMenu) {
		this.cssClassMenu = cssClassMenu;
	}

	public String getCssClassSubMenu() {
		return cssClassSubMenu;
	}

	public void setCssClassSubMenu(String cssClassSubMenu) {
		this.cssClassSubMenu = cssClassSubMenu;
	}

	public String getMenuType() {
		return menuType;
	}

	public void setMenuType(String menuType) {
		this.menuType = menuType;
	}

	@Override
	public int doStartTag() throws JspException {
		try {
			Context = pageContext.getServletContext().getContextPath();
			pageContext.getOut().write(print(menuList));
			return super.doStartTag();
			
		} catch (Exception e) {
			throw new JspException(Convert.toString(
					"MenuTag error: ", e.getMessage()) , e);
		}
	}
	
	protected String print(List<? extends MenuItem> menus) {
		StringBuilder out = new StringBuilder();
		for (MenuItem menu : menus) {
			if (!menu.isHidden()) {
				out.append(printSub(menu));
			}
		}
		return write(htmlElementUL, out.toString(), cssClassMenu);
	}
	
	private String printSub(MenuItem menu) {
		StringBuilder out = new StringBuilder();
		if (menu.isComposite() && !menu.isHidden()) {
			StringBuilder sout = new StringBuilder();
			for (MenuItem m : menu) {
				sout.append(printSub(m));
			}
			out.append(printItem(printLink(menu), printList(sout.toString())));
		} else {
			if (!menu.isHidden()) {
				out.append(printItem(menu));
			}
		}
		return out.toString();
	}

	private String printLink(MenuItem mi) {
		String htmlElement = htmlElementAH;
		
		if ("checkbox".equalsIgnoreCase(menuType)) {
			htmlElement = htmlElementCB;
		}

		if (mi.getURL().startsWith("javascript") 
				|| mi.getURL().endsWith("/")
				|| Context.equals("/")) {
			return write(htmlElement,mi.getName(),mi.getURL(),mi.getText(),"");
		}
		return write(htmlElement,mi.getName(),mi.getURL(),mi.getText(),Context);
	}
	
	private String printItem(Object... item) {
		return write(htmlElementLI, Convert.toString(item));
	}
	
	private String printItem(MenuItem menu) {
		return write(htmlElementLI, printLink(menu));
	}
	
	private String printList(String list) {
		return write(htmlElementUL, list, cssClassSubMenu);
	}
	
	private static String write(String element, Object... arguments) {
		return MessageFormat.format(element, arguments);
	}
	
}
