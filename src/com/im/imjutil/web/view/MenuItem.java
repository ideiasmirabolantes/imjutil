package com.im.imjutil.web.view;

import com.im.imjutil.patterns.Composite;

/**
 * Interface que define os dados de objeto menu.
 * 
 * @author Felipe Zappala
 */
public interface MenuItem extends Composite<MenuItem> {

	/**
	 * Obtem o nome do link do menu.
	 */
	public String getName();

	/**
	 * Configura o nome do link do menu.
	 */
	public void setName(String name);
	
	/**
	 * Obtem a URL do link do menu. 
	 */
	public String getURL();

	/**
	 * Configura a URL do link do menu. 
	 */
	public void setURL(String url);
	
	/**
	 * Obtem o texto de informacao do menu. 
	 */
	public String getText();

	/**
	 * Configura o texto de informacao do menu. 
	 */
	public void setText(String text);
	
	/**
	 * Verifica se o menu esta oculto. 
	 */
	public boolean isHidden();
}
