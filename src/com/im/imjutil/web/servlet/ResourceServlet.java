package com.im.imjutil.web.servlet;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.im.imjutil.config.Configurator;
import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.logging.Logger;
import com.im.imjutil.validation.Convert;
import com.im.imjutil.validation.Validator;

/**
 * Servlet responsavel por obter recursos via URL no diretorio
 * {@code /WEB-INF/resources/}
 * 
 * @author Felipe Zappala 
 */
public class ResourceServlet extends HttpServlet {

	/** Serial Version UID */
	private static final long serialVersionUID = -6698545952061001312L;
	
	/** Mapa de extensoes de arquivos e mime types */
	private static final Map<String, String> extensionMime;
	static {
		Properties properties = new Properties();
		String mtype = "mimetypes.properties";
		try {
			properties.load(ResourceServlet.class.getResourceAsStream(mtype));
		} catch (IOException e) {
			Logger.error(e, "[ResourceServlet] Erro ao abrir ", mtype);
			throw new ValidationException("Erro ao abrir mimetypes", e);
		}
		if (properties.isEmpty()) {
			throw new ValidationException("Erro mimetypes vazio");
		}
		
		Set<Entry<Object, Object>>  mimes = properties.entrySet();
		extensionMime = new HashMap<String, String>(mimes.size());
		
		for (Entry<Object, Object> entry : properties.entrySet()) {
			extensionMime.put((String)entry.getKey(), (String)entry.getValue());
		}
	}
	
	/** Caminho do diretorio de recursos do sistema. */
	private String RESOURCE_PATH;

	/** Define o uso de um caminho completo no SO ou um relativo a aplicacao */
	private boolean USE_ABSOLUTE_PATH;
	
	/** Define a forma em que o conteudo sera disposto, como anexo ou na tela */
	private String DISPOSITION_TYPE;
	
	@Override
	public void init(javax.servlet.ServletConfig config) 
			throws ServletException {
		// Determina o uso de um caminho absoluto ou relativo a aplicacao
		String use = Configurator.get("ResourceServlet.useAbsolutePath");
		if (!Validator.isValid(use)) {
			use = config.getInitParameter("useAbsolutePath");			
		}
		USE_ABSOLUTE_PATH = Convert.toBoolean(use);
		
		// Determina o caminho dos recursos
		String path = Configurator.get("ResourceServlet.resourcePath");
		if (!Validator.isValid(path)) {
			path = config.getInitParameter("resourcePath");			
		}
		
		if (Validator.isValid(path)) {
			RESOURCE_PATH = path;
		} else {
			RESOURCE_PATH = Convert.toString(
					"WEB-INF", File.separatorChar, "resources");	
		}

		// Determina a forma como o conteudo sera disposto, anexo ou tela.
		String disType = Configurator.get("ResourceServlet.contentDispositionType");
		if (!Validator.isValid(disType)) {
			disType = config.getInitParameter("contentDispositionType");			
		}
		
		if (Validator.isValid(disType)) {
			DISPOSITION_TYPE = disType;
		} else {
			DISPOSITION_TYPE = "inline";	
		}
		
	}
	
	/**
	 * Executa a tarefa de obter um recurso pela url via servlet.
	 * 
	 * @param req O objeto de requisicao.
	 * @param resp O objeto de resposta.
	 */
	protected void doWork(HttpServletRequest req, HttpServletResponse resp) {
		String file = null;
		try {
			// Obtem da URL invocada o recurso desejado
			String relativeURI = req.getRequestURI().replaceFirst(
					Convert.toString(req.getContextPath(), "/resources"), "");
			
			// Prepara o recurso para o filesystem do S.O. corrente
			file = relativeURI.replaceAll("/", File.separator);
			
			// Remove os cookies passados na URLs
			file = file.replaceFirst("\\;.*$", ""); 
			
			// Obtem o recurso do disco e retorna no response com o seu mimetype 
			this.getResource(req, resp, file);
			
		} catch (Exception e) {
			Logger.error(e, "[ResourceServlet] Erro ao obter o recurso: ",file);
		}
	}

	/**
	 * Obtem um recurso do diretorio <code>/WEB-INF/resources/<code>
	 * e grava-o na resposta.
	 * 
	 * @param req O objeto de requisicao.
	 * @param resp O objeto de resposta.
	 * @throws IOException Caso ocorra um erro de leitura.
	 */
	private void getResource(HttpServletRequest req, HttpServletResponse resp, 
			String filePath) throws IOException {
		
		if (Validator.isValid(filePath)) {
			String appContextPath = "";
			if (!USE_ABSOLUTE_PATH) {
				appContextPath = getAppContextRealPath(req);
			} 
			File file = new File(Convert.toString(
					appContextPath, RESOURCE_PATH, filePath));
			
			resp.setContentType(getMimeType(file));
	        resp.setHeader("Content-Disposition", Convert.toString(
	        		DISPOSITION_TYPE, "; filename=\"", file.getName(), "\";"));
	        resp.setContentLength((int)file.length());
	        
			BufferedInputStream in = new BufferedInputStream(
					new FileInputStream(file));
			
			BufferedOutputStream out = new BufferedOutputStream(
					resp.getOutputStream());
			
	        int length;
	        byte[] buffer = new byte[4096];
	        
	        while ((length = in.read(buffer)) > 0) {
	            out.write(buffer, 0, length);
	        }
			in.close();
			out.flush();
			out.close();
			
//	        Logger.debug("[ResourceServlet] get file: " + file);
		} 		
	}
	
	/**
	 * Obtem o Mime Type a partir da extensao do arquivo.
	 * O mime type padrao e: <code>application/octet-stream</code>
	 * 
	 * @param file O arquivo a obter o mime type.
	 * @return O mime type do arquivo.
	 */
	protected String getMimeType(File file) {
		// Define a extensao do arquivo
		Pattern regexp = Pattern.compile("^.+\\.(\\w{2,4})$");
		Matcher matcher = regexp.matcher(file.getName());
		String ext = null;
		if (matcher.matches()) { 
			ext = matcher.group(1);
		}
		
		// Aplica o MIME TYPE correspondente
		String contentType = "application/octet-stream";
		if (ext != null && extensionMime.containsKey(ext)) {
			contentType = extensionMime.get(ext);
		}
		
		return contentType;
	}

	/**
	 * Obtem o path real da aplicacao no disco da maquina.
	 *  
	 * @param req O request corrente
	 * @return O path da aplicacao
	 */
	protected static String getAppContextRealPath(HttpServletRequest req) {
			return req.getSession().getServletContext().getRealPath("/");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doWork(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		this.doWork(req, resp);
	}

}

