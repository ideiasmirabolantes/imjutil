package com.im.imjutil.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.im.imjutil.logging.Logger;

/**
 * Servlet de execucao de {@link Runnables} online
 * 
 * @author Felipe Zappala
 */
public class RunnerServlet extends HttpServlet {

	private static final long serialVersionUID = 5321194979239062305L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String cName = req.getParameter("class");
		String mName = req.getParameter("method");
		
		Logger.info("[RunnerServlet] Rodando classe: ",cName," metodo: ",mName);
		
		try {
			Class<?> clazz = Class.forName(cName);
			if ("main".equals(mName)) {
				Method m = clazz.getDeclaredMethod("main", String[].class);
				m.setAccessible(true);
				m.invoke(clazz, (Object) new String[]{});
			} else {
				Method m = clazz.getDeclaredMethod(mName);
				m.setAccessible(true);
				Constructor<?> c = clazz.getDeclaredConstructor();
				c.setAccessible(true);
				m.invoke(c.newInstance());
			}
			
			log(resp.getWriter(), "Classe executada com sucesso:", 
					"Classe: ", cName, "Metodo: ", mName);
			
		} catch (Exception e) {
			Logger.error(e, "[RunnerServlet] Erro ao executar classe - ", 
					"Classe: ", cName, "Metodo: ", mName);
			
			log(resp.getWriter(), "Classe executada com ERRO:", 
					"Classe: ", cName, "Metodo: ", mName);
			
			e.printStackTrace(resp.getWriter());
		}
	}
	
	/**
	 * Imprime o log da execucao 
	 */
	private void log(PrintWriter writer, String msg, Object... log) {
		writer.println("<h1>");
		writer.println(msg);
		writer.println("</h1><br />");
		writer.println("<h3>");
		for (Object o : log) {
			writer.println(o);
			writer.println("<br />");
		}
		writer.println("</h3>");
	}

}
