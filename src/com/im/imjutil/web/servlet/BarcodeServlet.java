package com.im.imjutil.web.servlet;

import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.krysalis.barcode4j.impl.int2of5.Interleaved2Of5Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;

import com.im.imjutil.validation.Convert;

/**
 *	Servlet responsavel por gerar codigos de barra
 * 
 * @author cfreitas
 *
 */
public class BarcodeServlet extends HttpServlet {

	private static final long serialVersionUID = 4644102563534862500L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String codigoBarra = request.getParameter("codigobarra");
		
		String orientation = request.getParameter("orientation");
		
		String dpi =  request.getParameter("dpi");
		
		String WideFactor =  request.getParameter("WideFactor");
		
		String BarHeigh =  request.getParameter("BarHeigh");
		
		String FontSize =  request.getParameter("FontSize");
		
		Interleaved2Of5Bean bean = new Interleaved2Of5Bean();
		
        //Configure the barcode generator
        bean.setModuleWidth(UnitConv.in2mm(1.5f / Convert.toInteger(dpi))); 
        
        bean.setWideFactor(Convert.toDouble(WideFactor));
        bean.doQuietZone(false);
        bean.setBarHeight(Convert.toDouble(BarHeigh));
        bean.setFontSize(Convert.toDouble(FontSize));
        
        response.setContentType("image/jpeg");
        response.setDateHeader("Expires", 0);
        DataOutputStream out = new DataOutputStream(response.getOutputStream());
        try {
            //Set up the canvas provider for monochrome JPEG output 
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/jpeg",Convert.toInteger(dpi), BufferedImage.TYPE_BYTE_BINARY, false, Convert.toInteger(orientation));            
            //Generate the barcode
            bean.generateBarcode(canvas, codigoBarra);            
            //Signal end of generation
            canvas.finish();
        } finally {
            out.close();
        }
	}
}
