package com.im.imjutil.web.spring;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.im.imjutil.exception.ValidationException;
import com.im.imjutil.logging.Logger;

/**
 * Classe controle spring responsavel por encaminhar um mapeamento do
 * xml do spring para um recurso qualquer que possa ser acessado diretamente
 * pela url do aplicativo. O mapeamento do recurso no xml do spring 
 * depende do padrao de captura de url definido no web.xml para o servlet do 
 * spring receber a requisicao e passar a este controle. 
 * 
 * @author Felipe Zappala
 */
public class ForwardController implements Controller {

	/** Recurso injeto pelo spring */
	private String resource;
	
	@Override
	public ModelAndView handleRequest(HttpServletRequest req,
			HttpServletResponse resp) throws Exception {
		if (resource == null) {
			throw new ValidationException(
					"Recurso de destino nao definido, nulo ou vazio");
		}
		Logger.debug("[ForwardController] Forward from [", 
				req.getRequestURI(), "] to [", resource, "]");
		
		req.getRequestDispatcher(resource).forward(req, resp);
		return null;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		if (resource != null) {
			this.resource = resource;
		}
	}
	
}
