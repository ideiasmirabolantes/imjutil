package com.im.imjutil.factory;

import java.lang.reflect.Constructor;

import com.im.imjutil.validation.Convert;

/**
 * Classe utilizada para armazenar a chave e a classe especifica utilizada no
 * registro da factory {@link Factory}
 * 
 * @author Bruno Alcantara
 * 
 * @param <K> Tipo da chave utilizada no registro
 * @param <V> Tipo da classe utilizada no registro
 */
class Entry<K, V> {
	
	/** Chave do registro */
    private K key;

    /** Classe do registro */
    private Class<V> clazz;

    protected Entry(K key, Class<V> clazz) {
        this.key = key;
        this.clazz = clazz;
    }

    public K key() {
        return this.key;
    }

    /**
     * Retorna a instancia da classe armazenada no atributo clazz
     * 
     * @param constructorParameters Parametros do metodo construtor da classe
     * @return Retorna a instancia do tipo generico V
     * @throws Exception lanca uma excecao para o escopo de chamada do metodo
     */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public V getInstance(Object... constructorParameters) throws Exception {
		Constructor[] constructors = clazz.getConstructors();

        for (Constructor<V> c : constructors) {
            V instance = c.newInstance(constructorParameters);

            if (instance != null) {
                return instance;
            }
        }

        String msg = "Construtor nao encontrado para argumentos (";

        for (int i = 0; i < constructorParameters.length; i++) {
            msg = Convert.toString(msg,
                (((i > 0) && (i < constructorParameters.length)) ? ", " : ""),
                    constructorParameters[i].getClass().getName());
        }

        msg = Convert.toString(msg, ")");
        
        throw new RuntimeException(msg);
    }
    
    /**
     * Retorna a instancia de uma classe Singleton armazenada no atributo clazz
     * 
     * @param constructorParameters Parametros do metodo construtor da classe
     * @return Retorna a instancia do tipo generico V
     * @throws Exception lanca uma excecao para o escopo de chamada do metodo
     */
    @SuppressWarnings("unchecked")
	public V getInstanceSingleton() throws Exception {
    	return (V) clazz.getMethod("instance").invoke(clazz);
    }
    
}
