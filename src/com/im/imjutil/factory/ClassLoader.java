package com.im.imjutil.factory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;

import com.im.imjutil.logging.Logger;
import com.im.imjutil.util.Util;

/**
 * Classe implementa um class loader para isolar o escopo de um pacote de
 * classes, ou de determinada classe de outro class loader, ou do proprios class
 * loaders BootStrap, ExtClassLoader e AppClassLoader.
 * 
 * OBS: Esta classe extende de {@link URLClassLoader} pela sua facilidade de uso
 * para carregar pacotes completos (jars) com classes java, e por ja implementar
 * o metodo loadClass utilizado para carregar uma determinada classe para uso.
 * 
 * @see URLClassLoader
 * 
 * @author Bruno Alcantara
 */
public class ClassLoader extends URLClassLoader {

	private class Bootstrap extends URLClassLoader {

		public Bootstrap() {
			super(new URL[0], null);
		}
		
		@Override
		public void addURL(URL url) {
			super.addURL(url);
		}
		
	}
	
	/** Controla o uso do classLoade pai. */
	private boolean ignoreParent = false;
	 
	/** Instancia do classLoader pai utilizada para usar o BootStrap*/
	private Bootstrap bootstrap;
	
	/** Armazena o objeto do classLoader pai. */
	private java.lang.ClassLoader parent;
	
	/** Identificador unico do ClassLoader. */
	private long id;
	
	/**
	 * Construtor padrao inicializa a classe super {@link URLClassLoader}.
	 */
	public ClassLoader() {
		this(Thread.currentThread().getContextClassLoader(), false);
	}

	/**
	 * Construtor inicializa a classe super {@link URLClassLoader}, ou ignora
	 * essa inicializacao caso receba o valor FALSO (false) pelo parametro do
	 * construtor.
	 * 
	 * @param ignoreParent Controla o uso de um classLoader pai.
	 */
	public ClassLoader(boolean ignoreParent) {
		this(Thread.currentThread().getContextClassLoader(), ignoreParent);
	}
	
	/**
	 * Construtor inicializa a classe super {@link URLClassLoader} recebendo o
	 * classLoader pai como parametro e o controlador do uso do classLoader pai.
	 * 
	 * @param parent ClassLoader pai.
	 * @param ignoreParent Controla o uso de um classLoader pai.
	 */
	public ClassLoader(java.lang.ClassLoader parent, boolean ignoreParent) {
		super(new URL[0], parent);
		id = Util.getCode();
		this.bootstrap = new Bootstrap();
		this.ignoreParent = ignoreParent;
		this.parent = parent;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		if (id > 0) {
			this.id = id;
		}
	}
    
	/**
	 * MACUMBA |o|
	 */
	@Override
	public URL getResource(String name) {
		if (ignoreParent) {
			return this.bootstrap.getResource(name);
		}
		
		return super.getResource(name);
	}
	
	@Override
	public Enumeration<URL> getResources(String name) throws IOException {
		return super.getResources(name);
	}
	 
	@Override
	public InputStream getResourceAsStream(String name) {
		return super.getResourceAsStream(name);
	}
	
	/**
	 * Adiciona um pacote de classes | jar | jars para o class loader corrente.
	 * 
	 * @param file Arquivo para ser adicionado.
	 * 
	 * @throws MalformedURLException 
	 */
    public void addFile(File file) throws MalformedURLException {
		Logger.debug("ClassLoader addFile - Path: ", file.getAbsolutePath());
		
        super.addURL(file.toURI().toURL());
        
        // Adiciona o arquivo no bootrap
        this.bootstrap.addURL(file.toURI().toURL());
    }

	/**
	 * Adiciona uma lista de pacotes de classes | jar | jars para o class loader
	 * corrente.
	 * 
	 * @param files Lista de arquivos dos pacotes de classes | jar | jars para
	 *            serem adicionados.
	 *            
	 * @throws MalformedURLException 
	 */
    public void addFiles(File...files) throws MalformedURLException {
		Logger.debug("ClassLoader addFiles - Files: ", files);
		
		for (File file : files) {
			addFile(file);
		}
    }

	/**
	 * Adiciona uma lista de arquivos de um diretorio passsado como argumento 
	 * para o metodo.para o class loader corrente.
	 * 
	 * @param directory Diretorio raiz de arquivos dos pacotes de classes | jar 
	 * 					| jars para serem adicionados.
	 * 
	 * @throws MalformedURLException
	 */
	public void addFilesFromDirectory(String directory)
			throws MalformedURLException {
		
		Logger.debug("ClassLoader addFilesFromDirectory - directory: ",
				directory);
		
		File filesDirectory = new File(directory);
		File[] files = filesDirectory.listFiles();

		for (int i = 0; i < files.length; i++) {
			if (files[i].getName().endsWith(".jar") ||
					files[i].getName().endsWith(".class"))	 {
				addFile(files[i]);
			}
		}
    }
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		// Nome do classLoader corrente.
		sb.append(this.getClass().getSimpleName()).append("_").append(id);
		sb.append("\r\n");

		if (this.parent != null) {
			sb.append("----------> Parent Classloader:\r\n");
			sb.append(this.parent.toString());
			sb.append("\r\n");
		}

		return (sb.toString());
	}
	
}
