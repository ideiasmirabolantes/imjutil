package com.im.imjutil.factory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Classe implementa o padrao de projetos Abstract Factory.
 * 
 * Abstract Factory e um padrao de projeto de software (design pattern em
 * ingles). Este padrao permite a criacao de familias de objetos relacionados ou
 * dependentes, atraves de uma unica interface e sem que a classe concreta seja
 * especificada.
 * 
 * <a href="http://pt.wikipedia.org/wiki/Abstract_Factory">mais informacoes</a>
 * 
 * @author Bruno Alcantara.
 */
public class Factory {

	/** Armazena os registros da classes da fabrica. */
    private Map<Object, Object> registry;

    /** ClassLoader da factory. */
    private ClassLoader classLoader;
    
    /** Construtor padrao. */
    public Factory() {
    	registry = new HashMap<Object, Object>();
	}
    
    /** 
     * Construtor recebe o classLoader da factory como argumento. 
     * 
     * @param classLoader ClassLoader da factory do tipo {@link ClassLoader}.
     * */
    public Factory(ClassLoader classLoader) {
    	registry = new HashMap<Object, Object>();
    	this.classLoader = classLoader;
	}

	/**
	 * Retorna o {@link Set} de {@link java.util.Map.Entry} dos registros da
	 * factory para serem iterados em um loop.
	 * 
	 * @return Um {@link Set} com o mapa de registros da factory
	 *         Set<java.util.Map.Entry<Object, Object>>.
	 */
    public Set<java.util.Map.Entry<Object, Object>> getRegistries() {
		return registry.entrySet();
	}

	/**
     * Faz o resgistro de uma classe na Factory
     * 
     * @param <K> Chave identificadora com o nome da classe na Factory
	 * @param <V> Valor do registro da classe na Factory
	 * @param key Chave identificadora da classe na Factory
     * @param clazz Objeto da classe a ser registrada na Factory
     */
    public <K, V> void register(K key, Class<V> clazz) {
        Entry<K, V> entry = new Entry<K, V>(key, clazz);
        registry.put(key, entry);
    }

    public ClassLoader getClassLoader() {
		return classLoader;
	}

	/**
	 * Instancia uma classe armazenada na Factory
	 * 
	 * @param <K> Chave identificadora com o nome da classe na Factory
	 * @param <V> Valor do registro da classe na Factory
	 * @param key Chave identificadora da classe na Factory
	 * 
	 * @return Retorna a instancia de uma classe armazenada na Factory
	 * 
	 * @throws RuntimeException
	 *             lanca uma excecao para o escopo de chamada do metodo
	 */
    @SuppressWarnings("unchecked")
    public <K, V> V instantiate(K key, Object... parameters)
            throws RuntimeException {

        Entry<K, V> entry = (Entry<K, V>) registry.get(key);

        if (entry == null) {
            return null;
        }

        try {
            return entry.getInstance(parameters);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

	/**
	 * Instancia uma classe Singleton armazenada na Factory
	 * 
	 * @param <K> Chave identificadora com o nome da classe na Factory
	 * @param <V> Valor do registro da classe na Factory
	 * @param key Chave identificadora da classe na Factory
	 * 
	 * @return Retorna a instancia de uma classe Singleton armazenada na Factory
	 * 
	 * @throws RuntimeException
	 *             lanca uma excecao para o escopo de chamada do metodo
	 */
    @SuppressWarnings("unchecked")
    public <K, V> V instantiateSingleton(K key)
            throws RuntimeException {

        Entry<K, V> entry = (Entry<K, V>) registry.get(key);

        if (entry == null) {
            return null;
        }

        try {
            return entry.getInstanceSingleton();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
}
