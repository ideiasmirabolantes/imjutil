-- Tabela da classe DatabaseProperty usada pela classe DatabaseConfig --
CREATE TABLE t_database_property (
  id bigserial,
  map TEXT NOT NULL,
  key TEXT NOT NULL,
  value TEXT NULL,
  PRIMARY KEY(id),
  UNIQUE(map,key)
);