################################################################################
Esta lib tem as seguintes dependencias:
################################################################################

	- activation.jar                
	- barcode4j.jar                 
	- commons-fileupload-1.2.1.jar  
	- commons-io-1.4.jar            
	- commons-logging.jar           
	- commons-net-ftp-2.0.jar       
	- jai_codec.jar                 
	- jai_core.jar                  
	- javax.persistence_2.0.0.jar   
	- json_simple-1.1-all.jar
	- jsp-api.jar
	- log4j-1.2.15.jar
	- mail.jar
	- postgresql-8.4-701.jdbc4.jar
	- quartz-1.6.5.jar
	- servlet-api.jar
	- spring.jar
	- spring-webmvc.jar	
	
################################################################################	
Configurar o Java heap space: 
################################################################################

-Xms512m -Xmx1024m
